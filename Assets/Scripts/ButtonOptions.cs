using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ButtonOptions : MonoBehaviour {

    public ModeScore Modescore;

    //public void PlayGame() {
        //SceneManager.LoadScene(2);
    //}
  
    public void PlayGame () {
        SceneManager.LoadScene(2);
    }
    public void Controls()
    {
        SceneManager.LoadScene(6);
    }
    public void MainMenu() {
        SceneManager.LoadScene(1);
        Time.timeScale = 1f;
    }
    public void Credits()
    {
        SceneManager.LoadScene(5);
    }

    // Below is track selection 

    public void Track01 () {
        SceneManager.LoadScene(3);
    }
    public void Track02() {
        SceneManager.LoadScene(4);
    }

    //Quit Button 

    public void doExitGame() {
        Application.Quit();
    }


}
