using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarChoice : MonoBehaviour
{
    public GameObject RedBody;
    public GameObject BlueBody;
    public GameObject BlackBody;
    public int CarImport;
    void Start()
    {
        CarImport = GlobalCar.CarType;
        if (CarImport == 1)
        {
            RedBody.SetActive(true);
        }
        CarImport = GlobalCar.CarType;
        if (CarImport == 2)
        {
            BlueBody.SetActive(true);
        }
        if (CarImport == 3)
        {
            BlackBody.SetActive(true);
        }
    }
    }
    //1=RED 2=BLUE 3=BLACK
