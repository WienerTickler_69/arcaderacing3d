using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityStandardAssets.Vehicles.Car;


public class CarControlActive : MonoBehaviour
{

    public GameObject CarController;
    public GameObject DreamCar01;

    // Use this for initialization

    void Start()
    {
        CarController.GetComponent <CarController>().enabled = true;
        DreamCar01.GetComponent<CarAIControl> ().enabled = true;

    }

}
