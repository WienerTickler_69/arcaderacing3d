using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishRotate : MonoBehaviour {
    public GameObject UnpausedUI;
    public GameObject FinishUI;

    void Update() {
        transform.Rotate(0, 1, 0, Space.World);
        UnpausedUI.SetActive(false);
        FinishUI.SetActive(true);
    }
}
