using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuActive : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public GameObject PauseUI;
    public GameObject UnPausedUI;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            { 
                Resume();
            } else
            {
                Pause();
            }

        }
        void Resume ()
        {
            PauseUI.SetActive(false);
            UnPausedUI.SetActive(true);
            Time.timeScale = 1f;
            GameIsPaused = false;
        }
        void Pause ()
        {
            PauseUI.SetActive(true);
            UnPausedUI.SetActive(false);
            Time.timeScale = 0f;
            GameIsPaused = true;
        }

    }
}
