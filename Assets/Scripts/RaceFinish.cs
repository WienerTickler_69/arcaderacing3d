using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class RaceFinish : MonoBehaviour
{

    public GameObject MyCar;
    public GameObject FinishCam;
    public GameObject ViewModes;
    //public Gameobject LevelMusic;
    public GameObject CompleteTrig;

    void OnTriggerEnter()
    {
        CompleteTrig.SetActive(false); 
        MyCar.SetActive(true);
        FinishCam.SetActive(true);
        //LevelMusic.SetActive (false);
        ViewModes.SetActive(false);
        GlobalCash.TotalCash += 100;
        PlayerPrefs.SetInt("SavedCash", GlobalCash.TotalCash);
    }
}