using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Unlockables : MonoBehaviour
{
    public GameObject BlackButton;
    public int CashValue;
    
    
    void Update()
    {
        CashValue = GlobalCash.TotalCash;
        if (CashValue >= 16700)
        {
            BlackButton.GetComponent<Button>().interactable = true;
        }
    }

    public void BlackUnlock()
    {
        BlackButton.SetActive(false);
        CashValue -= 16700;
        GlobalCash.TotalCash -= 16700;
        PlayerPrefs.SetInt("SavedCash", GlobalCash.TotalCash);
        PlayerPrefs.SetInt("BlackBought", 16700);
    }
}
