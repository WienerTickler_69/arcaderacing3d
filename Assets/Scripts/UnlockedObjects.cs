using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockedObjects : MonoBehaviour
{
    public int BlackSelect;
    public GameObject FakeBlack;
    
    void Start() {
        BlackSelect = PlayerPrefs.GetInt("BlackBought");
        if (BlackSelect == 16700) 
        {
            FakeBlack.SetActive(false);
        }
    }
}
