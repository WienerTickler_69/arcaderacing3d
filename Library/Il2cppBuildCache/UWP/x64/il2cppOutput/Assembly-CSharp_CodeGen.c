﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 BuggyControl/WheelComponent BuggyControl::SetWheelComponent(UnityEngine.Transform,UnityEngine.Transform,System.Single,System.Boolean,System.Single)
extern void BuggyControl_SetWheelComponent_mDA0A83284216CCE4617B2D91DD51C06E7EAB3601 (void);
// 0x00000002 System.Void BuggyControl::Awake()
extern void BuggyControl_Awake_mAE7DAACA6D6C79DC05805C4988BD175436BE3D26 (void);
// 0x00000003 System.Void BuggyControl::ShiftUp()
extern void BuggyControl_ShiftUp_m240F4182487427C6C3C33747E92C0E44D1C198B9 (void);
// 0x00000004 System.Void BuggyControl::ShiftDown()
extern void BuggyControl_ShiftDown_m7DC7E2F78E665309B651A1A0F9E870BAA6FFB45A (void);
// 0x00000005 System.Void BuggyControl::OnCollisionEnter(UnityEngine.Collision)
extern void BuggyControl_OnCollisionEnter_mCD4AF2091BD7A4D1172B080E05EF2C9E986FB61C (void);
// 0x00000006 System.Void BuggyControl::OnCollisionStay(UnityEngine.Collision)
extern void BuggyControl_OnCollisionStay_m58AA1AF5AB6A982E7C76D5BC68D9AC35F885012B (void);
// 0x00000007 System.Void BuggyControl::Update()
extern void BuggyControl_Update_m8CBD000D95FD28BC3FD1E23B05DE0F2D3792604F (void);
// 0x00000008 System.Void BuggyControl::FixedUpdate()
extern void BuggyControl_FixedUpdate_m16770D6FA4F9D034EEED8D06105B61B911177282 (void);
// 0x00000009 System.Void BuggyControl::OnDrawGizmos()
extern void BuggyControl_OnDrawGizmos_m46348F0CBA6C49A1D19815E54DB8463CB4E458E6 (void);
// 0x0000000A System.Void BuggyControl::.ctor()
extern void BuggyControl__ctor_mABF5843B49AC233AE41CD869BAE07EEBF9E08B23 (void);
// 0x0000000B System.Void BuggyControl/CarWheels::.ctor()
extern void CarWheels__ctor_mF04AA3C3EFEE9244B28873F8F1FB7562B5F559BC (void);
// 0x0000000C System.Void BuggyControl/ConnectWheel::.ctor()
extern void ConnectWheel__ctor_m0F2DA1599E913AF9FBBD61802CC985DD528C93E3 (void);
// 0x0000000D System.Void BuggyControl/WheelSetting::.ctor()
extern void WheelSetting__ctor_mD57418DAD333AD86355F1477AC7066DCA9782141 (void);
// 0x0000000E System.Void BuggyControl/CarLights::.ctor()
extern void CarLights__ctor_mDEC1742CF5441FF292B68204BCA657841E89EACB (void);
// 0x0000000F System.Void BuggyControl/CarSounds::.ctor()
extern void CarSounds__ctor_m7430D2202D45FAC242C60334AF4C6FFA224F51CC (void);
// 0x00000010 System.Void BuggyControl/CarParticles::.ctor()
extern void CarParticles__ctor_mABD18B780B67A45D2BE45BA08571AED8502817A2 (void);
// 0x00000011 System.Void BuggyControl/CarSetting::.ctor()
extern void CarSetting__ctor_mD5F53F3C1E2B46EE83FF9BE914151592A9C1E4A5 (void);
// 0x00000012 System.Void BuggyControl/HitGround::.ctor()
extern void HitGround__ctor_mC47369434578A488EC2E930EDC6C1A095140E154 (void);
// 0x00000013 System.Void BuggyControl/WheelComponent::.ctor()
extern void WheelComponent__ctor_m6D3A95D9A508D5DE54A7F3731AE063A381CBE83B (void);
// 0x00000014 System.Void CarSwitch::CurrentCarActive(System.Int32)
extern void CarSwitch_CurrentCarActive_m40525385AACE666CDDD0C18AE74D6D91AC170664 (void);
// 0x00000015 System.Void CarSwitch::.ctor()
extern void CarSwitch__ctor_m4973A47FF6FA0C613D9CBD43123E6A46CDDDFEC4 (void);
// 0x00000016 System.Void CenterAxle::Update()
extern void CenterAxle_Update_mD07DE3AD001E22874E46BE16EFF7752B94434A96 (void);
// 0x00000017 System.Void CenterAxle::.ctor()
extern void CenterAxle__ctor_m41158E9FD04B238E60CDB36266B55466F1A666DB (void);
// 0x00000018 System.Void GizmoObject::OnDrawGizmos()
extern void GizmoObject_OnDrawGizmos_mBC844FEDCC85587B7B06FCB59EEE44FC864F1963 (void);
// 0x00000019 System.Void GizmoObject::.ctor()
extern void GizmoObject__ctor_m07BAEF9E583D5F0E577BF217DF81B0582EFD12F0 (void);
// 0x0000001A System.Void jiggleBone::Awake()
extern void jiggleBone_Awake_m662C11C8DA10DEBF6F1C7C67B9FB0249AF7E33DD (void);
// 0x0000001B System.Void jiggleBone::LateUpdate()
extern void jiggleBone_LateUpdate_mF887898486CDB2369AF9B299258107C46B477074 (void);
// 0x0000001C System.Void jiggleBone::.ctor()
extern void jiggleBone__ctor_m59C260C3E2CC17526EAAF6D712EBB7556486256D (void);
// 0x0000001D System.Void PlayerAnimation::Awake()
extern void PlayerAnimation_Awake_m1E43458A064235886005E0336844DC1685D7DC56 (void);
// 0x0000001E System.Void PlayerAnimation::Update()
extern void PlayerAnimation_Update_m24161E32CFBC1DB4A00E63C4B8AE2A2026189A03 (void);
// 0x0000001F System.Void PlayerAnimation::OnAnimatorIK()
extern void PlayerAnimation_OnAnimatorIK_mCBB2EA7800B9E68238A9A9BA5DAF85A5BAEE984F (void);
// 0x00000020 System.Void PlayerAnimation::.ctor()
extern void PlayerAnimation__ctor_m6712BFFFBAA031003247E1E6F34C1D9C197CF268 (void);
// 0x00000021 System.Void Spring::Update()
extern void Spring_Update_mD01DE0062BB2AE428472EC2EC9A12603257DFC1F (void);
// 0x00000022 System.Void Spring::.ctor()
extern void Spring__ctor_m7A1FF518446BAECE38938EA7B4D164A1361A58BB (void);
// 0x00000023 System.Void Spring/LookObjectsClass::.ctor()
extern void LookObjectsClass__ctor_mA091F52250F01D9C10AEF29031AD25D429691B69 (void);
// 0x00000024 System.Void VehicleCamera::CameraSwitch()
extern void VehicleCamera_CameraSwitch_m9ADE1B6BCCEDA23930E633A2C63FDCDDDA3EC4CB (void);
// 0x00000025 System.Void VehicleCamera::CarAccelForward(System.Single)
extern void VehicleCamera_CarAccelForward_m8665B42CDC415490A5DC774D284DE577CF85EED2 (void);
// 0x00000026 System.Void VehicleCamera::CarAccelBack(System.Single)
extern void VehicleCamera_CarAccelBack_m894981900A1F5FA4C4DA306B87A43DEEA2D0D95A (void);
// 0x00000027 System.Void VehicleCamera::CarSteer(System.Single)
extern void VehicleCamera_CarSteer_m0F81EAE6C010C7C720CEDC069F4D1F1FA24A54D4 (void);
// 0x00000028 System.Void VehicleCamera::CarHandBrake(System.Boolean)
extern void VehicleCamera_CarHandBrake_mC907AD072AECD8CADBE663D672AE6ACEEB1421E0 (void);
// 0x00000029 System.Void VehicleCamera::CarShift(System.Boolean)
extern void VehicleCamera_CarShift_m081ECED11B5E25F405D6BFD6AD46DE2D93B74B8E (void);
// 0x0000002A System.Void VehicleCamera::RestCar()
extern void VehicleCamera_RestCar_m47561C9E6E6E4D9359FC097207B507F3DB0AC12B (void);
// 0x0000002B System.Void VehicleCamera::ShowCarUI()
extern void VehicleCamera_ShowCarUI_m4D3B944EDB92C232A96F73821C0AA22A7E05BB20 (void);
// 0x0000002C System.Void VehicleCamera::Start()
extern void VehicleCamera_Start_m332BCBC9078F1C6CA381E4B6030918A0CD23D7D5 (void);
// 0x0000002D System.Void VehicleCamera::Update()
extern void VehicleCamera_Update_m4627BECEAB50B368CDB946182B08AE37C19AF65B (void);
// 0x0000002E System.Single VehicleCamera::AdjustLineOfSight(UnityEngine.Vector3,UnityEngine.Vector3)
extern void VehicleCamera_AdjustLineOfSight_m2815276FF3579FF8C096CE68F3C6D0F82AC8F24E (void);
// 0x0000002F System.Void VehicleCamera::.ctor()
extern void VehicleCamera__ctor_m75F85D6FBEEBD517A9F2C35ABF77B4ABBE2E9BDE (void);
// 0x00000030 System.Void VehicleCamera/CarUIClass::.ctor()
extern void CarUIClass__ctor_mCF84557822C2B5C36498EF1112214183054C095F (void);
// 0x00000031 System.Void VehicleDamage::Start()
extern void VehicleDamage_Start_m885F4656F023FEC9E0DB26162F9AEB75D34AFD1D (void);
// 0x00000032 System.Void VehicleDamage::OnCollisionEnter(UnityEngine.Collision)
extern void VehicleDamage_OnCollisionEnter_m453083B78AE2E273651A199CC9D85F8141664207 (void);
// 0x00000033 System.Void VehicleDamage::OnMeshForce(UnityEngine.Vector4)
extern void VehicleDamage_OnMeshForce_mC2FCBCBDEABC263FCA94CF61FF5C1E0A78A5EE5F (void);
// 0x00000034 System.Void VehicleDamage::OnMeshForce(UnityEngine.Vector3,System.Single)
extern void VehicleDamage_OnMeshForce_m445A59A2DBC0D45EEFE91F572A2D6CC5CFCD4602 (void);
// 0x00000035 System.Void VehicleDamage::.ctor()
extern void VehicleDamage__ctor_m73BE37829922CB43402FCC56C05F3744C87EF188 (void);
// 0x00000036 System.Void Skidmarks::Start()
extern void Skidmarks_Start_m630AD543C6831297B563C7E0FCCF6E593B1E66E4 (void);
// 0x00000037 System.Void Skidmarks::Awake()
extern void Skidmarks_Awake_mE824327E196F09A11614FF3BF8DF459D27101A52 (void);
// 0x00000038 System.Int32 Skidmarks::AddSkidMark(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Skidmarks_AddSkidMark_mA1475B4E1C10D429155C166434BF9D33B1B8C0A1 (void);
// 0x00000039 System.Void Skidmarks::LateUpdate()
extern void Skidmarks_LateUpdate_m243E9DE089D3D2FFEF63A8BDFD41CA21330B16FC (void);
// 0x0000003A System.Void Skidmarks::.ctor()
extern void Skidmarks__ctor_mCF8400BC9314E7EAD88F56AC072EB921C0DAB2DA (void);
// 0x0000003B System.Void Skidmarks/markSection::.ctor()
extern void markSection__ctor_m19F0CFB9502AC2E4060E3B4197173858845D9353 (void);
// 0x0000003C System.Void WheelSkidmarks::Start()
extern void WheelSkidmarks_Start_mB965198AF4498114EFBEC136C7831205B2A81073 (void);
// 0x0000003D System.Void WheelSkidmarks::FixedUpdate()
extern void WheelSkidmarks_FixedUpdate_mCCF74A5BF34FFF9DC41AA2D95CB2929BEAADF065 (void);
// 0x0000003E System.Void WheelSkidmarks::.ctor()
extern void WheelSkidmarks__ctor_m2E1705DF9124D9E9FDB49F2AB016F431A95FCDBB (void);
// 0x0000003F System.Void AlphaButtonClickMask::Start()
extern void AlphaButtonClickMask_Start_m188C2B5F6FE9CF85F7442D2E0DBADC5D8D302D1D (void);
// 0x00000040 System.Boolean AlphaButtonClickMask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern void AlphaButtonClickMask_IsRaycastLocationValid_mE73AF8581DAC8324A8F30F357B4E6BDB2CC4AF7E (void);
// 0x00000041 System.Void AlphaButtonClickMask::.ctor()
extern void AlphaButtonClickMask__ctor_m5C7ADFC7BAC4BEB4564F0F89F107563C394969B7 (void);
// 0x00000042 System.Void EventSystemChecker::Awake()
extern void EventSystemChecker_Awake_m8B954CCFC70EDE79CD9418D2D38DA27F91DEBAA1 (void);
// 0x00000043 System.Void EventSystemChecker::.ctor()
extern void EventSystemChecker__ctor_mE880C47D1714A5E4C2BF1F48ED7254DF1C758363 (void);
// 0x00000044 System.Void ForcedReset::Update()
extern void ForcedReset_Update_m976395EFFB7A0EC376736E1DBC6F80F50ECE4DD5 (void);
// 0x00000045 System.Void ForcedReset::.ctor()
extern void ForcedReset__ctor_mD8A0AEF74A204883C4A757AF8C2BA90B1AE737F9 (void);
// 0x00000046 System.Void MenuSceneLoader::Awake()
extern void MenuSceneLoader_Awake_m18B4E09AAFDC9578469D066C50A8AC0054AA34AC (void);
// 0x00000047 System.Void MenuSceneLoader::.ctor()
extern void MenuSceneLoader__ctor_m27FD45CA6C6C8B579D2FA4EEDABBA35F2C7EF6BC (void);
// 0x00000048 System.Void PauseMenu::Awake()
extern void PauseMenu_Awake_mB04BCBDA84AEC654D1976EC3DF61A0CF68D2C86C (void);
// 0x00000049 System.Void PauseMenu::MenuOn()
extern void PauseMenu_MenuOn_m8973DC956DD5235381EE4ACB4A182AA5BF0EF2EA (void);
// 0x0000004A System.Void PauseMenu::MenuOff()
extern void PauseMenu_MenuOff_mCD1AF41F916B0C02D2FD6F82377DBEAFF7C30720 (void);
// 0x0000004B System.Void PauseMenu::OnMenuStatusChange()
extern void PauseMenu_OnMenuStatusChange_mD1E8BD9B9CCB274A83FFF0FE2E06E6ABD0361B4A (void);
// 0x0000004C System.Void PauseMenu::.ctor()
extern void PauseMenu__ctor_mA1A281F3359C234E5CF24FFEAC20C12C48D69018 (void);
// 0x0000004D System.Void SceneAndURLLoader::Awake()
extern void SceneAndURLLoader_Awake_m8F276157A2A5FA943EF7918D6CCDB81273317E23 (void);
// 0x0000004E System.Void SceneAndURLLoader::SceneLoad(System.String)
extern void SceneAndURLLoader_SceneLoad_m2B09BD48F419F49A6BD461DBC7B2290EC8632B06 (void);
// 0x0000004F System.Void SceneAndURLLoader::LoadURL(System.String)
extern void SceneAndURLLoader_LoadURL_m47E3E286E80F2D5B3E6A164C32F7E1B473532AE2 (void);
// 0x00000050 System.Void SceneAndURLLoader::.ctor()
extern void SceneAndURLLoader__ctor_m6DEE574FADF9E3E894594690CB2755F69D5D4BE5 (void);
// 0x00000051 System.Void CameraSwitch::OnEnable()
extern void CameraSwitch_OnEnable_mD422991EDD9D880928644EE1BC4E557EE644679C (void);
// 0x00000052 System.Void CameraSwitch::NextCamera()
extern void CameraSwitch_NextCamera_m38AB4521C129032FA1DA5154E09D36D0FE2DB257 (void);
// 0x00000053 System.Void CameraSwitch::.ctor()
extern void CameraSwitch__ctor_m550FCA9B0C24BBBC8BDBCAAAAA7BBF26312399FE (void);
// 0x00000054 System.Void LevelReset::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void LevelReset_OnPointerClick_m9625C8235343CB1DE6B089AD6F2D5ACF738072A4 (void);
// 0x00000055 System.Void LevelReset::.ctor()
extern void LevelReset__ctor_mFD2BB9AF1A9D7E795DDAAE2F33C1F0EB1FB31F07 (void);
// 0x00000056 System.Void ButtonOptions::PlayGame()
extern void ButtonOptions_PlayGame_m9E2117F45465D1044B34C49B84D9DE5074453F37 (void);
// 0x00000057 System.Void ButtonOptions::MainMenu()
extern void ButtonOptions_MainMenu_m1E7DC14B4795D0F1EA23C223DA9A0F9CB3DDC1D2 (void);
// 0x00000058 System.Void ButtonOptions::Credits()
extern void ButtonOptions_Credits_m570AA0A350E3D8A0F97436B472DAE6DB8344BA7D (void);
// 0x00000059 System.Void ButtonOptions::Track01()
extern void ButtonOptions_Track01_mC6CA9D2DDF57C79D3119D31F21EB78EDFD12C0F1 (void);
// 0x0000005A System.Void ButtonOptions::Track02()
extern void ButtonOptions_Track02_m54284E0535331EF4C2396EC1E7360136604044A9 (void);
// 0x0000005B System.Void ButtonOptions::doExitGame()
extern void ButtonOptions_doExitGame_m0AE138AAF496350CBB82A76C556AD44E3F6768E9 (void);
// 0x0000005C System.Void ButtonOptions::.ctor()
extern void ButtonOptions__ctor_m6A4E167BAF103B463212364819DB42488E1302CE (void);
// 0x0000005D System.Void CameraChange::Update()
extern void CameraChange_Update_m567F67F1C326A51C016C6BD49D152D6A4BEE67A1 (void);
// 0x0000005E System.Collections.IEnumerator CameraChange::ModeChange()
extern void CameraChange_ModeChange_m5D7CB5AA2649A4513C1C4C53F200DA833147DE68 (void);
// 0x0000005F System.Void CameraChange::.ctor()
extern void CameraChange__ctor_m5A248C7658A54C780F720D1C983B98CEB7993672 (void);
// 0x00000060 System.Void CameraChange/<ModeChange>d__5::.ctor(System.Int32)
extern void U3CModeChangeU3Ed__5__ctor_m6E4DFE6D6853FF215E5C6950314239FBDE5A9065 (void);
// 0x00000061 System.Void CameraChange/<ModeChange>d__5::System.IDisposable.Dispose()
extern void U3CModeChangeU3Ed__5_System_IDisposable_Dispose_mAD92FCC379FAA0BF7F2708FCE020CBD7E3A6A8F6 (void);
// 0x00000062 System.Boolean CameraChange/<ModeChange>d__5::MoveNext()
extern void U3CModeChangeU3Ed__5_MoveNext_m26FDA14921720021888B348B318D82231A0D3BB1 (void);
// 0x00000063 System.Object CameraChange/<ModeChange>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CModeChangeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m394AC68A54E9D1CCE5BA816174FDA7410AD52313 (void);
// 0x00000064 System.Void CameraChange/<ModeChange>d__5::System.Collections.IEnumerator.Reset()
extern void U3CModeChangeU3Ed__5_System_Collections_IEnumerator_Reset_m4ECE5228864B4992CC7AEFAA5CA56A89B6C1459B (void);
// 0x00000065 System.Object CameraChange/<ModeChange>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CModeChangeU3Ed__5_System_Collections_IEnumerator_get_Current_m8B5E062560B10F8ED51785E84B3E2A6617216ACE (void);
// 0x00000066 System.Void CarChoice::Start()
extern void CarChoice_Start_m8035683201A655B956CC208F058EDFF4D7A0B761 (void);
// 0x00000067 System.Void CarChoice::.ctor()
extern void CarChoice__ctor_mE3966E245F06271FC40C705608FC20509C448DE9 (void);
// 0x00000068 System.Void CarControlActive::Start()
extern void CarControlActive_Start_mF58667AC5DCD6308C17681F2446D1DC8ABBDA367 (void);
// 0x00000069 System.Void CarControlActive::.ctor()
extern void CarControlActive__ctor_mB1E1B81049371F652098602724C161B9BDE70DA0 (void);
// 0x0000006A System.Void Countdown::Start()
extern void Countdown_Start_m4EDEE1C587E3BA10C738C384E38BF5E85A4264E9 (void);
// 0x0000006B System.Collections.IEnumerator Countdown::CountStart()
extern void Countdown_CountStart_mA539D770497099A23409A280BD60ED80A0A0CFE9 (void);
// 0x0000006C System.Void Countdown::.ctor()
extern void Countdown__ctor_m046FB7F09BC45D1C33491F89DD01A93BBD31880E (void);
// 0x0000006D System.Void Countdown/<CountStart>d__4::.ctor(System.Int32)
extern void U3CCountStartU3Ed__4__ctor_mBC03B9E6034DF120A0D2089986FD13B398B4D66B (void);
// 0x0000006E System.Void Countdown/<CountStart>d__4::System.IDisposable.Dispose()
extern void U3CCountStartU3Ed__4_System_IDisposable_Dispose_mA70409A559F80987047BC73CF9553FFC296241D5 (void);
// 0x0000006F System.Boolean Countdown/<CountStart>d__4::MoveNext()
extern void U3CCountStartU3Ed__4_MoveNext_m1E901961316164DB4C1470BA2116E670C0664442 (void);
// 0x00000070 System.Object Countdown/<CountStart>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCountStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m421DF04ADD03CBDEE842CCE13AE941A4D4863394 (void);
// 0x00000071 System.Void Countdown/<CountStart>d__4::System.Collections.IEnumerator.Reset()
extern void U3CCountStartU3Ed__4_System_Collections_IEnumerator_Reset_m2ABF112DD1B76A21E8AE4F5F3AC281E80A7018C2 (void);
// 0x00000072 System.Object Countdown/<CountStart>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CCountStartU3Ed__4_System_Collections_IEnumerator_get_Current_m6DA6DC7604AE6FE8D637303A5EF00F99A0263E91 (void);
// 0x00000073 System.Void DreamCar01Track::Update()
extern void DreamCar01Track_Update_mBF4AD15CE71EC75EA57B02C6337F6DFBC94EE6D7 (void);
// 0x00000074 System.Collections.IEnumerator DreamCar01Track::OnTriggerEnter(UnityEngine.Collider)
extern void DreamCar01Track_OnTriggerEnter_mB246B8F2618C49FEC8DE1E13B956AB06F1877DCC (void);
// 0x00000075 System.Void DreamCar01Track::.ctor()
extern void DreamCar01Track__ctor_m06581B38AC1C1A0B15E0C591CF2134E8FD10D4BD (void);
// 0x00000076 System.Void DreamCar01Track/<OnTriggerEnter>d__50::.ctor(System.Int32)
extern void U3COnTriggerEnterU3Ed__50__ctor_mE0FA7F7442227C1E446BB3EEF073CC9515FF105F (void);
// 0x00000077 System.Void DreamCar01Track/<OnTriggerEnter>d__50::System.IDisposable.Dispose()
extern void U3COnTriggerEnterU3Ed__50_System_IDisposable_Dispose_m6A998999BEA1A73AE267C12FEE2C3376DFCD216B (void);
// 0x00000078 System.Boolean DreamCar01Track/<OnTriggerEnter>d__50::MoveNext()
extern void U3COnTriggerEnterU3Ed__50_MoveNext_mA0F38BEB2EAA6A1031CA7D81BE0B53D73DA01B73 (void);
// 0x00000079 System.Object DreamCar01Track/<OnTriggerEnter>d__50::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnTriggerEnterU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m113773DFC1D04FD4C23DB4ACBBDAB3E52D8C7F30 (void);
// 0x0000007A System.Void DreamCar01Track/<OnTriggerEnter>d__50::System.Collections.IEnumerator.Reset()
extern void U3COnTriggerEnterU3Ed__50_System_Collections_IEnumerator_Reset_m0C5FC16BB46437E9E92FB39F12E3F3E6F947439E (void);
// 0x0000007B System.Object DreamCar01Track/<OnTriggerEnter>d__50::System.Collections.IEnumerator.get_Current()
extern void U3COnTriggerEnterU3Ed__50_System_Collections_IEnumerator_get_Current_mD9B90C4BD35D717910E6CEF60B8ACBCF05B9FC36 (void);
// 0x0000007C System.Void DreamCar01Track1::Update()
extern void DreamCar01Track1_Update_m7522D26B7D56DE06D1EB3CE4CA473C5FE815BCF4 (void);
// 0x0000007D System.Collections.IEnumerator DreamCar01Track1::OnTriggerEnter(UnityEngine.Collider)
extern void DreamCar01Track1_OnTriggerEnter_m01F3423EB16DD02CED40F66C74B46758FEFED345 (void);
// 0x0000007E System.Void DreamCar01Track1::.ctor()
extern void DreamCar01Track1__ctor_mCC690053D7EAB18218F875A21060ACE6BC940303 (void);
// 0x0000007F System.Void DreamCar01Track1/<OnTriggerEnter>d__23::.ctor(System.Int32)
extern void U3COnTriggerEnterU3Ed__23__ctor_m804AF5DFF01B58EA0EFB4514515F15749E5C0C5A (void);
// 0x00000080 System.Void DreamCar01Track1/<OnTriggerEnter>d__23::System.IDisposable.Dispose()
extern void U3COnTriggerEnterU3Ed__23_System_IDisposable_Dispose_mAC2B1401586A46A40ADF2FE216D21512EFBBA0B4 (void);
// 0x00000081 System.Boolean DreamCar01Track1/<OnTriggerEnter>d__23::MoveNext()
extern void U3COnTriggerEnterU3Ed__23_MoveNext_mC66CCC043F066FD71023B0E4D2D3775DFC702E62 (void);
// 0x00000082 System.Object DreamCar01Track1/<OnTriggerEnter>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnTriggerEnterU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB147CCADDC2C1613A7428B27D8369D3D49EECCE7 (void);
// 0x00000083 System.Void DreamCar01Track1/<OnTriggerEnter>d__23::System.Collections.IEnumerator.Reset()
extern void U3COnTriggerEnterU3Ed__23_System_Collections_IEnumerator_Reset_m640AB20824CF86EFEA4D886AB2B65B89F7753015 (void);
// 0x00000084 System.Object DreamCar01Track1/<OnTriggerEnter>d__23::System.Collections.IEnumerator.get_Current()
extern void U3COnTriggerEnterU3Ed__23_System_Collections_IEnumerator_get_Current_mD5B525E1142F6B2BC3C1C2F75F013EFBCE4707D5 (void);
// 0x00000085 System.Void FinishRotate::Update()
extern void FinishRotate_Update_m86F2BA51E6B60C5B088534869692C1297A5A8ED7 (void);
// 0x00000086 System.Void FinishRotate::.ctor()
extern void FinishRotate__ctor_m8A5CB130C0B9BDF0303DF35D781470C4C2D1952A (void);
// 0x00000087 System.Void GlobalCar::RedCar()
extern void GlobalCar_RedCar_mF10F4D8931DC80180624BD3078CAB726B49F7994 (void);
// 0x00000088 System.Void GlobalCar::BlueCar()
extern void GlobalCar_BlueCar_m0602B4A1D0BDC85DDCF45467893E3196FD9FC0C3 (void);
// 0x00000089 System.Void GlobalCar::BlackCar()
extern void GlobalCar_BlackCar_mAF3E04BDFC60BFCFD5EDFE880F91F08F44FDC5ED (void);
// 0x0000008A System.Void GlobalCar::.ctor()
extern void GlobalCar__ctor_m664EA38FE0E52A0004BFD547136C3A595D2F86EA (void);
// 0x0000008B System.Void GlobalCash::Start()
extern void GlobalCash_Start_m223FE4563DF64084730E608E1DBF48FBCCD95A3A (void);
// 0x0000008C System.Void GlobalCash::Update()
extern void GlobalCash_Update_mDFC1B00A38360D004EEAB8E7F8E5F89C586DCFA1 (void);
// 0x0000008D System.Void GlobalCash::.ctor()
extern void GlobalCash__ctor_mF756857254CA62235F81B0C8283E2D264DE9A1E1 (void);
// 0x0000008E System.Void HalfPointTrigger::OnTriggerEnter()
extern void HalfPointTrigger_OnTriggerEnter_mEE7997172F17F5BE5F3F2A33680159F40A5DADCA (void);
// 0x0000008F System.Void HalfPointTrigger::.ctor()
extern void HalfPointTrigger__ctor_m289628F9F3B74A159F89173BE51667FD831F82AD (void);
// 0x00000090 System.Void LapComplete::Update()
extern void LapComplete_Update_mD0BA5723842D689D1EA28E78B7A0EEC2BB287B4D (void);
// 0x00000091 System.Void LapComplete::OnTriggerEnter()
extern void LapComplete_OnTriggerEnter_m1245488A53240B7DCB77721394ECEDAEAD202122 (void);
// 0x00000092 System.Void LapComplete::.ctor()
extern void LapComplete__ctor_mA9DFE466EAE019368BCBB632AD40A3280AD728A5 (void);
// 0x00000093 System.Void LapTimeManager::Update()
extern void LapTimeManager_Update_mADEBD26A4E32D6031F798159F50D046A088B9282 (void);
// 0x00000094 System.Void LapTimeManager::.ctor()
extern void LapTimeManager__ctor_mA103382E9B5294A10871BA31021ADE11AAC4FBA6 (void);
// 0x00000095 System.Void MenuAppear::StartMenu()
extern void MenuAppear_StartMenu_mBE11ED4AFF8BB87A6F2C37AAFBC15FD684C6F3C1 (void);
// 0x00000096 System.Void MenuAppear::.ctor()
extern void MenuAppear__ctor_m18BF204B3C2D0D457DEF532D34EACC572C51D45D (void);
// 0x00000097 System.Void ModeScore::Start()
extern void ModeScore_Start_m79E26F0F857997770A73F3B64923B22640FF78FD (void);
// 0x00000098 System.Void ModeScore::Update()
extern void ModeScore_Update_mBD0657CD1243F2707BA4E4528AAE8D12C16F6D36 (void);
// 0x00000099 System.Void ModeScore::.ctor()
extern void ModeScore__ctor_m3CE433FC1A86A5FA7EC8FF01264938D61A65600E (void);
// 0x0000009A System.Void ModeSelect::ScoreMode()
extern void ModeSelect_ScoreMode_m4A1DD9A33830BC7E1DD6CB5E44E63E790B018C61 (void);
// 0x0000009B System.Void ModeSelect::TheRaceMode()
extern void ModeSelect_TheRaceMode_m5E74F2931306244FC6936AB67EAB688599591BF3 (void);
// 0x0000009C System.Void ModeSelect::.ctor()
extern void ModeSelect__ctor_m33C637987E5393112D599F9D614D1CB331B07359 (void);
// 0x0000009D System.Void PauseMenuActive::Update()
extern void PauseMenuActive_Update_m042E804B41D02BE7A73497660C170207CD3FB348 (void);
// 0x0000009E System.Void PauseMenuActive::.ctor()
extern void PauseMenuActive__ctor_m3E75C1597BCE42306C5C4A27C93DF4C126446CF9 (void);
// 0x0000009F System.Void PauseMenuActive::.cctor()
extern void PauseMenuActive__cctor_m7260119B8F740CB82BA903DB8EAA1764F0004CFC (void);
// 0x000000A0 System.Void PauseMenuActive::<Update>g__Resume|3_0()
extern void PauseMenuActive_U3CUpdateU3Eg__ResumeU7C3_0_mB7B4AEEE0C5A1331E8EBE282CDEEE3432FE59A97 (void);
// 0x000000A1 System.Void PauseMenuActive::<Update>g__Pause|3_1()
extern void PauseMenuActive_U3CUpdateU3Eg__PauseU7C3_1_m61EC8A76C741685B429F07D32CA63AF865401E90 (void);
// 0x000000A2 System.Void PosDown::OnTriggerExit(UnityEngine.Collider)
extern void PosDown_OnTriggerExit_m56AA94835B65DA88BDF2105650F12C95B4CBCBF6 (void);
// 0x000000A3 System.Void PosDown::.ctor()
extern void PosDown__ctor_mA58B701B2A150E4B0EF2E3E666298B2C9BC591A9 (void);
// 0x000000A4 System.Void PosUp::OnTriggerExit(UnityEngine.Collider)
extern void PosUp_OnTriggerExit_m8259C49429DE65DAE2793A0E571D58BBCE2DE0AA (void);
// 0x000000A5 System.Void PosUp::.ctor()
extern void PosUp__ctor_mFB0251D735C6E83F7AAED20E1C088A3DADADF3B8 (void);
// 0x000000A6 System.Void RaceFinish::OnTriggerEnter()
extern void RaceFinish_OnTriggerEnter_m4272AEFA0B264542D7B7D43743A7D4E19326C907 (void);
// 0x000000A7 System.Void RaceFinish::.ctor()
extern void RaceFinish__ctor_m7071F04C622AB8342E870EEC0CA48DC15A1CC4C8 (void);
// 0x000000A8 System.Void BlueScore::OnTriggerEnter()
extern void BlueScore_OnTriggerEnter_mB1082E4076C4ADA0E177D172FC6619D95BDBB39D (void);
// 0x000000A9 System.Void BlueScore::.ctor()
extern void BlueScore__ctor_mF13E7C205668C56441018ACF3B4C677D13D0B532 (void);
// 0x000000AA System.Void RedScore::OnTriggerEnter()
extern void RedScore_OnTriggerEnter_mFD2D4F52DB2195CA7FDC6D37716262D99A3DF423 (void);
// 0x000000AB System.Void RedScore::.ctor()
extern void RedScore__ctor_m73554B9DA1FA6ED37E35DF1BCB3AB3DCF42BBC90 (void);
// 0x000000AC System.Void YellowScore::OnTriggerEnter()
extern void YellowScore_OnTriggerEnter_m87DC1C27BBEFA577022F271A9ED753391BF41B36 (void);
// 0x000000AD System.Void YellowScore::.ctor()
extern void YellowScore__ctor_m175883E3BAB70427A2DD0A6728582EE88DE62A42 (void);
// 0x000000AE System.Void Soundmanager::Start()
extern void Soundmanager_Start_m975C31F36795B907784A37DB9BCF92310274B474 (void);
// 0x000000AF System.Void Soundmanager::ChangeVolume()
extern void Soundmanager_ChangeVolume_mAA3DFF1DC7601A9FBCB0795AD6F4E31F443B1F56 (void);
// 0x000000B0 System.Void Soundmanager::load()
extern void Soundmanager_load_m59449B9A84CEC2288275A035E610929DEFF14A1B (void);
// 0x000000B1 System.Void Soundmanager::save()
extern void Soundmanager_save_m2700B41CBB75A7F5A748FC19FCA7C1126159A615 (void);
// 0x000000B2 System.Void Soundmanager::.ctor()
extern void Soundmanager__ctor_m2D0E3F827FD370006285DC36B29EA59520B4097F (void);
// 0x000000B3 System.Void SplashToMenu::Start()
extern void SplashToMenu_Start_m2D4A5CC4F2C41963AF787D9A90B244A1BE6250CF (void);
// 0x000000B4 System.Collections.IEnumerator SplashToMenu::ToMenu()
extern void SplashToMenu_ToMenu_m4340FFAE73ED422E679C79F9857297CB8BD805B0 (void);
// 0x000000B5 System.Void SplashToMenu::.ctor()
extern void SplashToMenu__ctor_mD224BF4E3A7789F4CD188BE40FD3FB692FE8E55D (void);
// 0x000000B6 System.Void SplashToMenu/<ToMenu>d__1::.ctor(System.Int32)
extern void U3CToMenuU3Ed__1__ctor_mF784F00797E38272788DB97F4C0DE2D85FDDB9C4 (void);
// 0x000000B7 System.Void SplashToMenu/<ToMenu>d__1::System.IDisposable.Dispose()
extern void U3CToMenuU3Ed__1_System_IDisposable_Dispose_m36920930640368C665D1DB58C61D18DD460A6759 (void);
// 0x000000B8 System.Boolean SplashToMenu/<ToMenu>d__1::MoveNext()
extern void U3CToMenuU3Ed__1_MoveNext_m4D147FAE80B0DD2AED1D8E6831851B612FF3299F (void);
// 0x000000B9 System.Object SplashToMenu/<ToMenu>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CToMenuU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01474B7BBB17E4E9EB5E8F7AFCAF9AEC603E3B61 (void);
// 0x000000BA System.Void SplashToMenu/<ToMenu>d__1::System.Collections.IEnumerator.Reset()
extern void U3CToMenuU3Ed__1_System_Collections_IEnumerator_Reset_m4E63D4569018C6668C59A91F7521E4770D078437 (void);
// 0x000000BB System.Object SplashToMenu/<ToMenu>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CToMenuU3Ed__1_System_Collections_IEnumerator_get_Current_m9A0139268073C9EEF302E9E29AD8624051550F30 (void);
// 0x000000BC System.Void StableCam::Update()
extern void StableCam_Update_mDC52917C7B3E8FC43E52A361D18811B67262A85B (void);
// 0x000000BD System.Void StableCam::.ctor()
extern void StableCam__ctor_m6F76A729FB16BF7C8F047C3B7F16AEC3FBA01E33 (void);
// 0x000000BE System.Void Unlockables::Update()
extern void Unlockables_Update_m1C3696EB6FC0648B8EA409FEDAB7B98AB1779840 (void);
// 0x000000BF System.Void Unlockables::BlackUnlock()
extern void Unlockables_BlackUnlock_m39A0A801AC6058FB423701C9996B5C8725EB8D2B (void);
// 0x000000C0 System.Void Unlockables::.ctor()
extern void Unlockables__ctor_m143B55A1F8CB79951EC2D7EBA10F9A99BB4DCC74 (void);
// 0x000000C1 System.Void UnlockedObjects::Start()
extern void UnlockedObjects_Start_m7F1F585DB61381BEADACB5AAEA94C4CAC6DFC8B4 (void);
// 0x000000C2 System.Void UnlockedObjects::.ctor()
extern void UnlockedObjects__ctor_mD74938E5D4CA5E3578C3232C27887301A2F7F228 (void);
// 0x000000C3 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Awake()
extern void ParticleSceneControls_Awake_m3268C5B5B81D4EE59C85C27AED5CA3956C284DA4 (void);
// 0x000000C4 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::OnDisable()
extern void ParticleSceneControls_OnDisable_mEB5EAF7BC1928EBEFD83E651459327948255D811 (void);
// 0x000000C5 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Previous()
extern void ParticleSceneControls_Previous_mC2FE564898A6019238C5611E63B5EC3581CEAA63 (void);
// 0x000000C6 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Next()
extern void ParticleSceneControls_Next_m4AB6C18CBE0148C4D354FDCAD5EE45D472577B8E (void);
// 0x000000C7 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Update()
extern void ParticleSceneControls_Update_m7B5894AE70D3733C901BFFF529195AC1467D632B (void);
// 0x000000C8 System.Boolean UnityStandardAssets.SceneUtils.ParticleSceneControls::CheckForGuiCollision()
extern void ParticleSceneControls_CheckForGuiCollision_mB8EDC5E34CE1B59986991852009A011D9514A8FB (void);
// 0x000000C9 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Select(System.Int32)
extern void ParticleSceneControls_Select_m123BA8121C7C5C454EF649A13C724F2622B4517A (void);
// 0x000000CA System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.ctor()
extern void ParticleSceneControls__ctor_mFE76B488C636F213B77193EDFF38F514A3186631 (void);
// 0x000000CB System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.cctor()
extern void ParticleSceneControls__cctor_mBA868BA9BC2400AD1AB420B3D0AB42D863358338 (void);
// 0x000000CC System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::.ctor()
extern void DemoParticleSystem__ctor_mA046B73C934441755CA5D57991D7C453645AD8CE (void);
// 0x000000CD System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList::.ctor()
extern void DemoParticleSystemList__ctor_mAFBB16272EB2E695826211DD030553401DF6E83B (void);
// 0x000000CE System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::Update()
extern void PlaceTargetWithMouse_Update_m28E68FE68FA0185C0D20A6001F4C262868348BE3 (void);
// 0x000000CF System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::.ctor()
extern void PlaceTargetWithMouse__ctor_m3582FA7DC3CF8198244F9EB014FE12AC97985FE7 (void);
// 0x000000D0 System.Void UnityStandardAssets.SceneUtils.SlowMoButton::Start()
extern void SlowMoButton_Start_m8F334286C4BEE772EAA4842F431E01BB4929A04C (void);
// 0x000000D1 System.Void UnityStandardAssets.SceneUtils.SlowMoButton::OnDestroy()
extern void SlowMoButton_OnDestroy_mE8D51330ED641B12795E984B6F0ECCB00536DBDB (void);
// 0x000000D2 System.Void UnityStandardAssets.SceneUtils.SlowMoButton::ChangeSpeed()
extern void SlowMoButton_ChangeSpeed_m0C19576C039AA7BBBC478C87A6C964376BA58EAB (void);
// 0x000000D3 System.Void UnityStandardAssets.SceneUtils.SlowMoButton::.ctor()
extern void SlowMoButton__ctor_m993A85A43C7462445119B63636FD65A6F8C0F6BF (void);
// 0x000000D4 System.Void UnityStandardAssets.Utility.ActivateTrigger::DoActivateTrigger()
extern void ActivateTrigger_DoActivateTrigger_m67A916D32496AEE42B99E067D4B53CD08D7F053F (void);
// 0x000000D5 System.Void UnityStandardAssets.Utility.ActivateTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void ActivateTrigger_OnTriggerEnter_m1B6300E8F49B18FA7B558DC0E3406BD12F65E1D8 (void);
// 0x000000D6 System.Void UnityStandardAssets.Utility.ActivateTrigger::.ctor()
extern void ActivateTrigger__ctor_m70F6E6A83BDB8D18767FB6E122A5019CE24ADC73 (void);
// 0x000000D7 System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch::OnEnable()
extern void AutoMobileShaderSwitch_OnEnable_m64D0D68E657087D191F85C7FA99F3B879ECAC68B (void);
// 0x000000D8 System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch::.ctor()
extern void AutoMobileShaderSwitch__ctor_m480014AF7A4225C84BE0C5BE3BC17ED6220C59F8 (void);
// 0x000000D9 System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::.ctor()
extern void ReplacementDefinition__ctor_m4997D7740DF9303A83CAACA4BA3A12D7C333E3FC (void);
// 0x000000DA System.Void UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList::.ctor()
extern void ReplacementList__ctor_m8F28958E9FB9417946A0360B0AC3493DD0B79920 (void);
// 0x000000DB System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::Start()
extern void AutoMoveAndRotate_Start_m891D026688CE02C1C5FA72BD5AB9C656BB2A75A0 (void);
// 0x000000DC System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::Update()
extern void AutoMoveAndRotate_Update_m3D975BDCB0C3E8648C4BFF4BC1C051031732E607 (void);
// 0x000000DD System.Void UnityStandardAssets.Utility.AutoMoveAndRotate::.ctor()
extern void AutoMoveAndRotate__ctor_m8F0CD8B9B52B5FCBDBABF694D75FBEFFBFEA5600 (void);
// 0x000000DE System.Void UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::.ctor()
extern void Vector3andSpace__ctor_m0BC6C4D6ADDEA323969A3F007106123F3A7B3962 (void);
// 0x000000DF System.Void UnityStandardAssets.Utility.CameraRefocus::.ctor(UnityEngine.Camera,UnityEngine.Transform,UnityEngine.Vector3)
extern void CameraRefocus__ctor_m8A7B2D4B5D3EDD59E8BFC75BA42925A808F25027 (void);
// 0x000000E0 System.Void UnityStandardAssets.Utility.CameraRefocus::ChangeCamera(UnityEngine.Camera)
extern void CameraRefocus_ChangeCamera_m688C8AC7BF3512B6E0CC0A31E2824010C45E687D (void);
// 0x000000E1 System.Void UnityStandardAssets.Utility.CameraRefocus::ChangeParent(UnityEngine.Transform)
extern void CameraRefocus_ChangeParent_m60A783666226E66E99CBB8674D621CF0AEA05136 (void);
// 0x000000E2 System.Void UnityStandardAssets.Utility.CameraRefocus::GetFocusPoint()
extern void CameraRefocus_GetFocusPoint_m81CFE3F6A7D6B26E2AF0E37192953DC2EF6EA039 (void);
// 0x000000E3 System.Void UnityStandardAssets.Utility.CameraRefocus::SetFocusPoint()
extern void CameraRefocus_SetFocusPoint_mB02787B322CA5F26E2223C804457F9CC2798825F (void);
// 0x000000E4 System.Void UnityStandardAssets.Utility.CurveControlledBob::Setup(UnityEngine.Camera,System.Single)
extern void CurveControlledBob_Setup_mBBE7165C9869012E40D574FE4CFC080B77E36E4E (void);
// 0x000000E5 UnityEngine.Vector3 UnityStandardAssets.Utility.CurveControlledBob::DoHeadBob(System.Single)
extern void CurveControlledBob_DoHeadBob_mB0983151149C57CFAEAF837EC93D6DE9BA07F09E (void);
// 0x000000E6 System.Void UnityStandardAssets.Utility.CurveControlledBob::.ctor()
extern void CurveControlledBob__ctor_m0EAE6C6C687D9144555AEFC5E5667CDCE1DAFE9C (void);
// 0x000000E7 System.Void UnityStandardAssets.Utility.DragRigidbody::Update()
extern void DragRigidbody_Update_mF23D1DB84BC75A1F4BA7C1301F2E06AA6220CF71 (void);
// 0x000000E8 System.Collections.IEnumerator UnityStandardAssets.Utility.DragRigidbody::DragObject(System.Single)
extern void DragRigidbody_DragObject_m8FE055A7BF316E6B6081ED6A000F4B33F6331383 (void);
// 0x000000E9 UnityEngine.Camera UnityStandardAssets.Utility.DragRigidbody::FindCamera()
extern void DragRigidbody_FindCamera_mA0D3F9E16A4AF7F01F5C847118F95116FD1E3ABE (void);
// 0x000000EA System.Void UnityStandardAssets.Utility.DragRigidbody::.ctor()
extern void DragRigidbody__ctor_m2FF487F84D55FD4B112EDEC08495D85B707A2576 (void);
// 0x000000EB System.Void UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::.ctor(System.Int32)
extern void U3CDragObjectU3Ed__8__ctor_mE581CFDFA1541B920A9073BD407B17197AA22C4F (void);
// 0x000000EC System.Void UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::System.IDisposable.Dispose()
extern void U3CDragObjectU3Ed__8_System_IDisposable_Dispose_mF26EE7E8993F6C8E42F3D472E25BA9AC3DDCB11A (void);
// 0x000000ED System.Boolean UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::MoveNext()
extern void U3CDragObjectU3Ed__8_MoveNext_mEF8CAA3CA274FA1284C3ED193F398E4C175A4AAA (void);
// 0x000000EE System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDragObjectU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1A8BEAEE65071C04D4D5109F01FC435E5E32598 (void);
// 0x000000EF System.Void UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::System.Collections.IEnumerator.Reset()
extern void U3CDragObjectU3Ed__8_System_Collections_IEnumerator_Reset_m3AD029462BAB8E669EDB80A7D1D156327A6034B7 (void);
// 0x000000F0 System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CDragObjectU3Ed__8_System_Collections_IEnumerator_get_Current_m2FEB828E66BAB45FD0068CB1F443B43F10AE9457 (void);
// 0x000000F1 System.Void UnityStandardAssets.Utility.DynamicShadowSettings::Start()
extern void DynamicShadowSettings_Start_mABF407E5176329CA9C819760364B55D8ECFFD54C (void);
// 0x000000F2 System.Void UnityStandardAssets.Utility.DynamicShadowSettings::Update()
extern void DynamicShadowSettings_Update_mF3128A86A6C42CF4B8AB6F2A3E7FCB2F81011A21 (void);
// 0x000000F3 System.Void UnityStandardAssets.Utility.DynamicShadowSettings::.ctor()
extern void DynamicShadowSettings__ctor_mA1F95D2BC18371EA5A5A00BC6B0B2EEA44648D3B (void);
// 0x000000F4 System.Void UnityStandardAssets.Utility.FollowTarget::LateUpdate()
extern void FollowTarget_LateUpdate_m5A379F65CE462686067A208184C14618FBD47858 (void);
// 0x000000F5 System.Void UnityStandardAssets.Utility.FollowTarget::.ctor()
extern void FollowTarget__ctor_m7695BB9D29256FF8E091780CD9FF2ACD5CD0BD4D (void);
// 0x000000F6 System.Void UnityStandardAssets.Utility.FOVKick::Setup(UnityEngine.Camera)
extern void FOVKick_Setup_m81A58E005DA8B84808DD83EFFD50ADD4E7439DCB (void);
// 0x000000F7 System.Void UnityStandardAssets.Utility.FOVKick::CheckStatus(UnityEngine.Camera)
extern void FOVKick_CheckStatus_m79FB404B3FA31459D29B576F15C186AB0000DE70 (void);
// 0x000000F8 System.Void UnityStandardAssets.Utility.FOVKick::ChangeCamera(UnityEngine.Camera)
extern void FOVKick_ChangeCamera_m9BE2A7DCE3BEF7CBEB8CE5506C882331B38F256F (void);
// 0x000000F9 System.Collections.IEnumerator UnityStandardAssets.Utility.FOVKick::FOVKickUp()
extern void FOVKick_FOVKickUp_m35F515049C6AE4E4A29C5A83CD6DE9BCC822ACFF (void);
// 0x000000FA System.Collections.IEnumerator UnityStandardAssets.Utility.FOVKick::FOVKickDown()
extern void FOVKick_FOVKickDown_mB964A3DD05D1E0337F3462DFF99864E2DD68B55D (void);
// 0x000000FB System.Void UnityStandardAssets.Utility.FOVKick::.ctor()
extern void FOVKick__ctor_m4F96E75BF9E5BBB1DD95122C4A0E1E6BAF4815C1 (void);
// 0x000000FC System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::.ctor(System.Int32)
extern void U3CFOVKickUpU3Ed__9__ctor_m1EF21E9B9DC9018D3B824D6DE00EB1E023012575 (void);
// 0x000000FD System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::System.IDisposable.Dispose()
extern void U3CFOVKickUpU3Ed__9_System_IDisposable_Dispose_m48ABBE7212815A0CCF476B5B04AF9CD539247549 (void);
// 0x000000FE System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::MoveNext()
extern void U3CFOVKickUpU3Ed__9_MoveNext_mAA4C668C48D9643C4631119FCBA37FBEB6921AC9 (void);
// 0x000000FF System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFOVKickUpU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99E4AABEA91319D0ED1D13D64B47B67105627012 (void);
// 0x00000100 System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::System.Collections.IEnumerator.Reset()
extern void U3CFOVKickUpU3Ed__9_System_Collections_IEnumerator_Reset_mE35D0195678B040DBCF2A0AFD6B4A37FFB327AE9 (void);
// 0x00000101 System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CFOVKickUpU3Ed__9_System_Collections_IEnumerator_get_Current_mFA0758DEBD49BA5EB77D7056D77BDA66E1194770 (void);
// 0x00000102 System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::.ctor(System.Int32)
extern void U3CFOVKickDownU3Ed__10__ctor_mD224B124C39C58E12A97C903679950D1DA896021 (void);
// 0x00000103 System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::System.IDisposable.Dispose()
extern void U3CFOVKickDownU3Ed__10_System_IDisposable_Dispose_m2E5B0C234FF88C0C4BAB260D0C220D0E4E416E8E (void);
// 0x00000104 System.Boolean UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::MoveNext()
extern void U3CFOVKickDownU3Ed__10_MoveNext_mBD3B663AFCDA2F1D51080784393CA95339E902AE (void);
// 0x00000105 System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFOVKickDownU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0AC4D4E3816A3A21DE1158B6CF3888DF6F8C605 (void);
// 0x00000106 System.Void UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::System.Collections.IEnumerator.Reset()
extern void U3CFOVKickDownU3Ed__10_System_Collections_IEnumerator_Reset_m585DC08D5BFA86A02F337DD9BA5352C16340E5CA (void);
// 0x00000107 System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CFOVKickDownU3Ed__10_System_Collections_IEnumerator_get_Current_mFAC92D730D3E14E822DAFB863DDBEA52DB5F36C0 (void);
// 0x00000108 System.Void UnityStandardAssets.Utility.FPSCounter::Start()
extern void FPSCounter_Start_m6FD80CF9018A6D0D78201E830965399E32F694C5 (void);
// 0x00000109 System.Void UnityStandardAssets.Utility.FPSCounter::Update()
extern void FPSCounter_Update_m384CE1DC871CDE082E892B0FD1DEED2FBA73310A (void);
// 0x0000010A System.Void UnityStandardAssets.Utility.FPSCounter::.ctor()
extern void FPSCounter__ctor_mA31248749E691ED7B04A6818FF2177EB00EBA9A5 (void);
// 0x0000010B System.Single UnityStandardAssets.Utility.LerpControlledBob::Offset()
extern void LerpControlledBob_Offset_mB3F28F3F7F003505776686DF7416FF0283EB6A82 (void);
// 0x0000010C System.Collections.IEnumerator UnityStandardAssets.Utility.LerpControlledBob::DoBobCycle()
extern void LerpControlledBob_DoBobCycle_mC05BB8C73BDBE83D173E5495474AA83D5696705E (void);
// 0x0000010D System.Void UnityStandardAssets.Utility.LerpControlledBob::.ctor()
extern void LerpControlledBob__ctor_m5298F226DA9B6BD3747475A2B90FF8AC5057ACE6 (void);
// 0x0000010E System.Void UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::.ctor(System.Int32)
extern void U3CDoBobCycleU3Ed__4__ctor_m00E3AA853E0C70833839BE6CF4B684C4CD33ED1A (void);
// 0x0000010F System.Void UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::System.IDisposable.Dispose()
extern void U3CDoBobCycleU3Ed__4_System_IDisposable_Dispose_m041C249DCAD39D852D5D4C53AE74743B147C56C0 (void);
// 0x00000110 System.Boolean UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::MoveNext()
extern void U3CDoBobCycleU3Ed__4_MoveNext_m8A21DA48C9B7B222CE8571687E5555EFB7A95C25 (void);
// 0x00000111 System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoBobCycleU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D01238FD70F51A1194ECCDADB610C047D3A5792 (void);
// 0x00000112 System.Void UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::System.Collections.IEnumerator.Reset()
extern void U3CDoBobCycleU3Ed__4_System_Collections_IEnumerator_Reset_m113C61E01B625F04FCDCB54A3F408E3BBDA8010C (void);
// 0x00000113 System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CDoBobCycleU3Ed__4_System_Collections_IEnumerator_get_Current_m16E4E9B6268494FD6D8AB1355AB783480603ACEC (void);
// 0x00000114 System.Void UnityStandardAssets.Utility.ObjectResetter::Start()
extern void ObjectResetter_Start_m11798912A3244DAB89010F14797215FF4C545FF2 (void);
// 0x00000115 System.Void UnityStandardAssets.Utility.ObjectResetter::DelayedReset(System.Single)
extern void ObjectResetter_DelayedReset_m5EC530F19321F0B1345039F13B2C2656E1C3D55F (void);
// 0x00000116 System.Collections.IEnumerator UnityStandardAssets.Utility.ObjectResetter::ResetCoroutine(System.Single)
extern void ObjectResetter_ResetCoroutine_m69B6702ED74A205BE4C3BCD6CFFC103D95C21AB1 (void);
// 0x00000117 System.Void UnityStandardAssets.Utility.ObjectResetter::.ctor()
extern void ObjectResetter__ctor_m3B23D97D1FB60A8B37A38639C6A91BB7B55AA66C (void);
// 0x00000118 System.Void UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::.ctor(System.Int32)
extern void U3CResetCoroutineU3Ed__6__ctor_mEA1307100A79B77AF1A76D2FEB010AC0F398D1D8 (void);
// 0x00000119 System.Void UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::System.IDisposable.Dispose()
extern void U3CResetCoroutineU3Ed__6_System_IDisposable_Dispose_m3ED64FE685800880DBFC70D40D2E100D99300FFA (void);
// 0x0000011A System.Boolean UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::MoveNext()
extern void U3CResetCoroutineU3Ed__6_MoveNext_m5843875165FBE55EC671EB15BAD223C27EDE181C (void);
// 0x0000011B System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetCoroutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5975A6D04132F784A7EA073D5C53A9718D70F21A (void);
// 0x0000011C System.Void UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::System.Collections.IEnumerator.Reset()
extern void U3CResetCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_m149B3BE7E2CC36FE99172D68509239A467DC5D62 (void);
// 0x0000011D System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CResetCoroutineU3Ed__6_System_Collections_IEnumerator_get_Current_mDC979ABAC9337C102E679D1336D55DCE1CF725F6 (void);
// 0x0000011E System.Collections.IEnumerator UnityStandardAssets.Utility.ParticleSystemDestroyer::Start()
extern void ParticleSystemDestroyer_Start_m22D9C83B5A722EA2E1C253E2FF099438138BF6FD (void);
// 0x0000011F System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer::Stop()
extern void ParticleSystemDestroyer_Stop_m4C91BE25E4FE2956C252BA8F5B403A35283BF657 (void);
// 0x00000120 System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer::.ctor()
extern void ParticleSystemDestroyer__ctor_m93DD6572873B5FA0275846AEFD644028F02164CB (void);
// 0x00000121 System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m75C35393971810AEBA21CC290AAA4CC49495E255 (void);
// 0x00000122 System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m30657DE1BBA6F01F5956D1E956F93FC6FB95E903 (void);
// 0x00000123 System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m8F48483D8702D87182DDF114056160A982938E6F (void);
// 0x00000124 System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m518B488D36CA8433355D9D9E5C7FF56414206FF3 (void);
// 0x00000125 System.Void UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m809AC76CDDA11F1B03907A7BF03939F0645577C0 (void);
// 0x00000126 System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m710D2045FB130B25C4B8911F951D2FB0BA9050D8 (void);
// 0x00000127 System.Void UnityStandardAssets.Utility.PlatformSpecificContent::OnEnable()
extern void PlatformSpecificContent_OnEnable_m3F1EDBFC1F3DBA76DEA47E4F8020D01BB7E81A87 (void);
// 0x00000128 System.Void UnityStandardAssets.Utility.PlatformSpecificContent::CheckEnableContent()
extern void PlatformSpecificContent_CheckEnableContent_mD9F7B70D48E634DC2C18C23E263F6BADDF5FD5EA (void);
// 0x00000129 System.Void UnityStandardAssets.Utility.PlatformSpecificContent::EnableContent(System.Boolean)
extern void PlatformSpecificContent_EnableContent_m0803E25C4DF725D74A58755F9A1AA2F1BC7A9F0A (void);
// 0x0000012A System.Void UnityStandardAssets.Utility.PlatformSpecificContent::.ctor()
extern void PlatformSpecificContent__ctor_m0655BFAD4DD4D971BE30511AFCCA29589DBE90E8 (void);
// 0x0000012B System.Void UnityStandardAssets.Utility.SimpleMouseRotator::Start()
extern void SimpleMouseRotator_Start_mC61B3E02E806C9E966F37029A33553D7643E5B15 (void);
// 0x0000012C System.Void UnityStandardAssets.Utility.SimpleMouseRotator::Update()
extern void SimpleMouseRotator_Update_m524B5CCCCFCBFCCB88FFA000C12D3B8DB1FA352A (void);
// 0x0000012D System.Void UnityStandardAssets.Utility.SimpleMouseRotator::.ctor()
extern void SimpleMouseRotator__ctor_m3CAF12200E3884E468A94ECFAAFBD517191D9516 (void);
// 0x0000012E System.Void UnityStandardAssets.Utility.SmoothFollow::Start()
extern void SmoothFollow_Start_m914CFA8E252AE8832270B584F77EEB18B5DF2612 (void);
// 0x0000012F System.Void UnityStandardAssets.Utility.SmoothFollow::LateUpdate()
extern void SmoothFollow_LateUpdate_m3C60EB2C41AAE96878EE5E5DD0FE31BF5BCFB031 (void);
// 0x00000130 System.Void UnityStandardAssets.Utility.SmoothFollow::.ctor()
extern void SmoothFollow__ctor_mEE4FE3D909F116297EEAC100215CD8A514E043CA (void);
// 0x00000131 System.Void UnityStandardAssets.Utility.TimedObjectActivator::Awake()
extern void TimedObjectActivator_Awake_m35EEF5BA247D13FC5B1AB2C31681BB504EAF5B64 (void);
// 0x00000132 System.Collections.IEnumerator UnityStandardAssets.Utility.TimedObjectActivator::Activate(UnityStandardAssets.Utility.TimedObjectActivator/Entry)
extern void TimedObjectActivator_Activate_m8D54B8B84EACEBCB2C71E10C7214212B741A04B3 (void);
// 0x00000133 System.Collections.IEnumerator UnityStandardAssets.Utility.TimedObjectActivator::Deactivate(UnityStandardAssets.Utility.TimedObjectActivator/Entry)
extern void TimedObjectActivator_Deactivate_mC156085CA25A5A7488B9C96A85346FB8B7947AC7 (void);
// 0x00000134 System.Collections.IEnumerator UnityStandardAssets.Utility.TimedObjectActivator::ReloadLevel(UnityStandardAssets.Utility.TimedObjectActivator/Entry)
extern void TimedObjectActivator_ReloadLevel_mFAA2BADC84D8E9B7D931D67BDDB0960595BA8BB4 (void);
// 0x00000135 System.Void UnityStandardAssets.Utility.TimedObjectActivator::.ctor()
extern void TimedObjectActivator__ctor_m5B33EE9A4CD720344F8DDBFD2B937E2A0E28CA3E (void);
// 0x00000136 System.Void UnityStandardAssets.Utility.TimedObjectActivator/Entry::.ctor()
extern void Entry__ctor_mBF1A0554BE0EE56D56A9C9EF90EF8DE8E0FB5BE4 (void);
// 0x00000137 System.Void UnityStandardAssets.Utility.TimedObjectActivator/Entries::.ctor()
extern void Entries__ctor_m6AC502F787DE068AECB0337B82F3FFAF2862F93B (void);
// 0x00000138 System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5::.ctor(System.Int32)
extern void U3CActivateU3Ed__5__ctor_mFB8778165F139004330CC6A3743D6820FF6ED47B (void);
// 0x00000139 System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5::System.IDisposable.Dispose()
extern void U3CActivateU3Ed__5_System_IDisposable_Dispose_m2FA37B3EB3ED2C9DAC56B61BB56AC25F77D35AB8 (void);
// 0x0000013A System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5::MoveNext()
extern void U3CActivateU3Ed__5_MoveNext_mA4DD23258631024824E15FD18F742BA4D342C4EF (void);
// 0x0000013B System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CActivateU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDA75E7324FFF19894C628190BD88A5C1009BCD5 (void);
// 0x0000013C System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5::System.Collections.IEnumerator.Reset()
extern void U3CActivateU3Ed__5_System_Collections_IEnumerator_Reset_m38243A882F10F8A9411F0AB165CDD2551C38DDFB (void);
// 0x0000013D System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CActivateU3Ed__5_System_Collections_IEnumerator_get_Current_mF2227106EC05A7DD3EA085EE1E8A2FDBEF16B754 (void);
// 0x0000013E System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6::.ctor(System.Int32)
extern void U3CDeactivateU3Ed__6__ctor_m899EA0F4E6FE2879D91EEF688EC6985A160E2562 (void);
// 0x0000013F System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6::System.IDisposable.Dispose()
extern void U3CDeactivateU3Ed__6_System_IDisposable_Dispose_m8DF5874B9F84C5378AC80474B09FA2AC9DD7858F (void);
// 0x00000140 System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6::MoveNext()
extern void U3CDeactivateU3Ed__6_MoveNext_m53744A123319ED281728B3A1B0E745FAA9B13551 (void);
// 0x00000141 System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDeactivateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9E5C199626DA1CD692DC06E3948C6111EEA3E3D8 (void);
// 0x00000142 System.Void UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6::System.Collections.IEnumerator.Reset()
extern void U3CDeactivateU3Ed__6_System_Collections_IEnumerator_Reset_m34A6D814484AD9621AA15A3DB3AB7D304DE7A5F6 (void);
// 0x00000143 System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CDeactivateU3Ed__6_System_Collections_IEnumerator_get_Current_m92519078AE1085CE8B5B47F87484748720BB292D (void);
// 0x00000144 System.Void UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7::.ctor(System.Int32)
extern void U3CReloadLevelU3Ed__7__ctor_mB0C3DD1EBE01A4174A41DF73CA7959A978D3C584 (void);
// 0x00000145 System.Void UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7::System.IDisposable.Dispose()
extern void U3CReloadLevelU3Ed__7_System_IDisposable_Dispose_mDE6472331A37952D2CDF6C319D01EC56938A8F2D (void);
// 0x00000146 System.Boolean UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7::MoveNext()
extern void U3CReloadLevelU3Ed__7_MoveNext_m0313BB28993322AE9B1E48037D492586E2B909CB (void);
// 0x00000147 System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReloadLevelU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9A935D9379F9EB43E8A67E59C53388164E7F254C (void);
// 0x00000148 System.Void UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7::System.Collections.IEnumerator.Reset()
extern void U3CReloadLevelU3Ed__7_System_Collections_IEnumerator_Reset_m5694D3EF695046C6A1579159E27A8F1A35136558 (void);
// 0x00000149 System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CReloadLevelU3Ed__7_System_Collections_IEnumerator_get_Current_mD0E398142BA22111EC8BC841A4A5BB59A71569E8 (void);
// 0x0000014A System.Void UnityStandardAssets.Utility.TimedObjectDestructor::Awake()
extern void TimedObjectDestructor_Awake_m2F595C9C46994AA3AA219F7C9583EF23F9F4539A (void);
// 0x0000014B System.Void UnityStandardAssets.Utility.TimedObjectDestructor::DestroyNow()
extern void TimedObjectDestructor_DestroyNow_mDF4DEF5006FEE0D6A216A8AF1625B1005A842D4A (void);
// 0x0000014C System.Void UnityStandardAssets.Utility.TimedObjectDestructor::.ctor()
extern void TimedObjectDestructor__ctor_m3B0E2CAF3EF3FC9E18F9DE8644F5A6375034DC25 (void);
// 0x0000014D System.Single UnityStandardAssets.Utility.WaypointCircuit::get_Length()
extern void WaypointCircuit_get_Length_m1236B0A843218603E40B10B60B52B2E7422FA3B7 (void);
// 0x0000014E System.Void UnityStandardAssets.Utility.WaypointCircuit::set_Length(System.Single)
extern void WaypointCircuit_set_Length_m863FDE229863388113B92A833DB17384ACAA8E6A (void);
// 0x0000014F UnityEngine.Transform[] UnityStandardAssets.Utility.WaypointCircuit::get_Waypoints()
extern void WaypointCircuit_get_Waypoints_m1548CA404E245A6D76B2EEEA7C537E2438DD89DD (void);
// 0x00000150 System.Void UnityStandardAssets.Utility.WaypointCircuit::Awake()
extern void WaypointCircuit_Awake_mEE8F9D45FB160ED347F77E7B42158CD7E9D0DBE2 (void);
// 0x00000151 UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointCircuit::GetRoutePoint(System.Single)
extern void WaypointCircuit_GetRoutePoint_m3DE0695DAD41C9FE4AC263C618A7FD8B2C7EED81 (void);
// 0x00000152 UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::GetRoutePosition(System.Single)
extern void WaypointCircuit_GetRoutePosition_m99C1CAF1FF0D344A0504D9E7B47EDB26D7C7D144 (void);
// 0x00000153 UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::CatmullRom(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void WaypointCircuit_CatmullRom_mCDE15554815F3FF68DDDC642A2B12C7464309379 (void);
// 0x00000154 System.Void UnityStandardAssets.Utility.WaypointCircuit::CachePositionsAndDistances()
extern void WaypointCircuit_CachePositionsAndDistances_mC15C637B064C3DE5D72CB8AE9AD43AFC253EB421 (void);
// 0x00000155 System.Void UnityStandardAssets.Utility.WaypointCircuit::OnDrawGizmos()
extern void WaypointCircuit_OnDrawGizmos_mA0E135CAA8FAF17A6AE5D528DFCEA8E4A2D40999 (void);
// 0x00000156 System.Void UnityStandardAssets.Utility.WaypointCircuit::OnDrawGizmosSelected()
extern void WaypointCircuit_OnDrawGizmosSelected_m238FB04A7650E4D0F423D8C806E783128E5FF824 (void);
// 0x00000157 System.Void UnityStandardAssets.Utility.WaypointCircuit::DrawGizmos(System.Boolean)
extern void WaypointCircuit_DrawGizmos_mE5F6AB8578D3A1DD584996DC3F705CE4D1223CFD (void);
// 0x00000158 System.Void UnityStandardAssets.Utility.WaypointCircuit::.ctor()
extern void WaypointCircuit__ctor_m7C43D705AC2DAE19854F50E7B6AC70327077DD4A (void);
// 0x00000159 System.Void UnityStandardAssets.Utility.WaypointCircuit/WaypointList::.ctor()
extern void WaypointList__ctor_m0E6BE7CDB93082199992A6248FF7C9A94CAB919B (void);
// 0x0000015A System.Void UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void RoutePoint__ctor_mCCDE64040449EDB283B910FC23935BBB32D1DFD5 (void);
// 0x0000015B UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_targetPoint()
extern void WaypointProgressTracker_get_targetPoint_m4F823E334B64B9FCFD563CA474289AF2B14AFD32 (void);
// 0x0000015C System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_targetPoint(UnityStandardAssets.Utility.WaypointCircuit/RoutePoint)
extern void WaypointProgressTracker_set_targetPoint_m2BED62ABBE7E2C8ECA459D32CBB3C4B541E51854 (void);
// 0x0000015D UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_speedPoint()
extern void WaypointProgressTracker_get_speedPoint_mC52CC0812547C8644009766FB8A0CE33715ED974 (void);
// 0x0000015E System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_speedPoint(UnityStandardAssets.Utility.WaypointCircuit/RoutePoint)
extern void WaypointProgressTracker_set_speedPoint_mC6891F0B93B772C22E068C5C31E155236D29F5DE (void);
// 0x0000015F UnityStandardAssets.Utility.WaypointCircuit/RoutePoint UnityStandardAssets.Utility.WaypointProgressTracker::get_progressPoint()
extern void WaypointProgressTracker_get_progressPoint_mA2914CC30840A7EAAD5724DF6F4C067152CA966B (void);
// 0x00000160 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::set_progressPoint(UnityStandardAssets.Utility.WaypointCircuit/RoutePoint)
extern void WaypointProgressTracker_set_progressPoint_m76B01B28B26FE69B242E058E4A2836D95575841D (void);
// 0x00000161 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Start()
extern void WaypointProgressTracker_Start_m444232E34CBA3AF6548795B03D417A0BAFF56751 (void);
// 0x00000162 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Reset()
extern void WaypointProgressTracker_Reset_m6A346E495442D196FF1666381F3351DF913B4883 (void);
// 0x00000163 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::Update()
extern void WaypointProgressTracker_Update_m44D15F27C065AC7FFBC7EF6C7BBB1151DB1B6A2D (void);
// 0x00000164 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::OnDrawGizmos()
extern void WaypointProgressTracker_OnDrawGizmos_mA178D2CE99E76CEC2DD2B165FD5A7185C2B1FBC5 (void);
// 0x00000165 System.Void UnityStandardAssets.Utility.WaypointProgressTracker::.ctor()
extern void WaypointProgressTracker__ctor_mF3FEE82CF0B834E9B5F602D74B3CD3E402100583 (void);
// 0x00000166 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnEnable()
extern void AfterburnerPhysicsForce_OnEnable_m84FEA393BE5D8FA30EE0331F65FF49E4DEA4FF3F (void);
// 0x00000167 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::FixedUpdate()
extern void AfterburnerPhysicsForce_FixedUpdate_m7ABE56FC167BCCD86437BF62800511E982AB4571 (void);
// 0x00000168 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::OnDrawGizmosSelected()
extern void AfterburnerPhysicsForce_OnDrawGizmosSelected_m2E46B2B0E13D90EB9AF516D01A6B10A5BC6FB650 (void);
// 0x00000169 System.Void UnityStandardAssets.Effects.AfterburnerPhysicsForce::.ctor()
extern void AfterburnerPhysicsForce__ctor_mA80D179FF58F655FE895E1F8E43DE4F39835425A (void);
// 0x0000016A System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionFireAndDebris::Start()
extern void ExplosionFireAndDebris_Start_mFE91656E5941C07D3F2A8E930B6EF3ACFA2767D9 (void);
// 0x0000016B System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::AddFire(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ExplosionFireAndDebris_AddFire_m425D0F537ADF0CA2858A3B8D9E177626AF849E84 (void);
// 0x0000016C System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris::.ctor()
extern void ExplosionFireAndDebris__ctor_m31B9795702F6DDB37F137A1600C60CF424CB9FB2 (void);
// 0x0000016D System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m654866B338818A02D8A859C29170FA103BDF75D6 (void);
// 0x0000016E System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mBE2A3DEE3789134E5B7D728FF7DAB247973A2EEA (void);
// 0x0000016F System.Boolean UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mFB613C537051E474BD2F6D1C4B2FB4740E676045 (void);
// 0x00000170 System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73095AFAE33F9FDB419352040AAF4625E9B93C8C (void);
// 0x00000171 System.Void UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m1A46BFEF351A0D8D4460DEA6051CFDFA78200490 (void);
// 0x00000172 System.Object UnityStandardAssets.Effects.ExplosionFireAndDebris/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m39E21679BAA2E949AA925B01357FE7D766728DFF (void);
// 0x00000173 System.Collections.IEnumerator UnityStandardAssets.Effects.ExplosionPhysicsForce::Start()
extern void ExplosionPhysicsForce_Start_m370D01FE6D7CE5974143C20D8DB1D830AE688ACF (void);
// 0x00000174 System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce::.ctor()
extern void ExplosionPhysicsForce__ctor_mB037B7FBCEE28759C26A0E33D4A958E7A7D69804 (void);
// 0x00000175 System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::.ctor(System.Int32)
extern void U3CStartU3Ed__1__ctor_m460DD3BF0681F7F816234BA6422A293452FDA130 (void);
// 0x00000176 System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::System.IDisposable.Dispose()
extern void U3CStartU3Ed__1_System_IDisposable_Dispose_m01622861FE6176F42905E610F84E19204C2C4E6C (void);
// 0x00000177 System.Boolean UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_m1082097E399D1772248C2916614291B1FFA52292 (void);
// 0x00000178 System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEB8947792CF4695EDBEB87F90688BB4ED6B95506 (void);
// 0x00000179 System.Void UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m887F7AE884FB48C61B7A5AED5FA58B2CAA37FC71 (void);
// 0x0000017A System.Object UnityStandardAssets.Effects.ExplosionPhysicsForce/<Start>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m44F967B0D8CBD51E7B9EDF332B3BFFF256DACEDC (void);
// 0x0000017B System.Void UnityStandardAssets.Effects.Explosive::Start()
extern void Explosive_Start_m27F89690A4785E39E59CE43CF1EBA7AAD72CC171 (void);
// 0x0000017C System.Collections.IEnumerator UnityStandardAssets.Effects.Explosive::OnCollisionEnter(UnityEngine.Collision)
extern void Explosive_OnCollisionEnter_m172BB7CE5D3A54BA968449E84FF2E18367942732 (void);
// 0x0000017D System.Void UnityStandardAssets.Effects.Explosive::Reset()
extern void Explosive_Reset_m83612ECA692A68CECA4FBBFCEBECDA9DC13CF735 (void);
// 0x0000017E System.Void UnityStandardAssets.Effects.Explosive::.ctor()
extern void Explosive__ctor_m5AEF01810918AC8F11E30D8D0CC2D702EF2979D3 (void);
// 0x0000017F System.Void UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8::.ctor(System.Int32)
extern void U3COnCollisionEnterU3Ed__8__ctor_mCB962F88A6E714F58F10A2E5DCB6CE3062FEE0DD (void);
// 0x00000180 System.Void UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8::System.IDisposable.Dispose()
extern void U3COnCollisionEnterU3Ed__8_System_IDisposable_Dispose_m5CF5AFFB8CBAC1F29C0FAAEA1330E851BE0B161F (void);
// 0x00000181 System.Boolean UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8::MoveNext()
extern void U3COnCollisionEnterU3Ed__8_MoveNext_m8AFE7BA66A5C3A481494AE72C63C4BAADCEBC938 (void);
// 0x00000182 System.Object UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnCollisionEnterU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m53A8C41319889D0F3CEE1954D51680A84CFDC174 (void);
// 0x00000183 System.Void UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8::System.Collections.IEnumerator.Reset()
extern void U3COnCollisionEnterU3Ed__8_System_Collections_IEnumerator_Reset_m6B3902A22CF546D2EDC226E2D9A4D2DB0FDED820 (void);
// 0x00000184 System.Object UnityStandardAssets.Effects.Explosive/<OnCollisionEnter>d__8::System.Collections.IEnumerator.get_Current()
extern void U3COnCollisionEnterU3Ed__8_System_Collections_IEnumerator_get_Current_m21F500127088FB6C437C59CA912F2FD2FA6EF3AE (void);
// 0x00000185 System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Start()
extern void ExtinguishableParticleSystem_Start_mC29A8B945025785DE601895F3121153C17052DB1 (void);
// 0x00000186 System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::Extinguish()
extern void ExtinguishableParticleSystem_Extinguish_m084314FFC2FED71A10A79CCD795639C357D33052 (void);
// 0x00000187 System.Void UnityStandardAssets.Effects.ExtinguishableParticleSystem::.ctor()
extern void ExtinguishableParticleSystem__ctor_mFD1FACA848B804BF787F80645D11B30ED6651789 (void);
// 0x00000188 System.Void UnityStandardAssets.Effects.FireLight::Start()
extern void FireLight_Start_m0119386C1B5952F44A8BADE374CB05E636456570 (void);
// 0x00000189 System.Void UnityStandardAssets.Effects.FireLight::Update()
extern void FireLight_Update_mE20D336BAB5800F4E7EE05A3BA1E74F3C070C53C (void);
// 0x0000018A System.Void UnityStandardAssets.Effects.FireLight::Extinguish()
extern void FireLight_Extinguish_mB3B9BE9E97F4DDA2287B39283777FEA2AC25AEBC (void);
// 0x0000018B System.Void UnityStandardAssets.Effects.FireLight::.ctor()
extern void FireLight__ctor_m4B61AB72ED30781C0121834171C63104F01223CE (void);
// 0x0000018C System.Void UnityStandardAssets.Effects.Hose::Update()
extern void Hose_Update_mFFFD4FC702953CB99599C87814E2604EDE737D32 (void);
// 0x0000018D System.Void UnityStandardAssets.Effects.Hose::.ctor()
extern void Hose__ctor_m42EB7EDF65C0B24C0DD96D2F96A7D33C867574BA (void);
// 0x0000018E System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::Start()
extern void ParticleSystemMultiplier_Start_m4655AADBCC9ACEACD4CEF85B17E905216FFBEF4A (void);
// 0x0000018F System.Void UnityStandardAssets.Effects.ParticleSystemMultiplier::.ctor()
extern void ParticleSystemMultiplier__ctor_m4ACC644100AC4F6AA46DC1F33020A4449D869386 (void);
// 0x00000190 System.Void UnityStandardAssets.Effects.SmokeParticles::Start()
extern void SmokeParticles_Start_mB77C48DABD154ED76C53334948115EC87066E1F4 (void);
// 0x00000191 System.Void UnityStandardAssets.Effects.SmokeParticles::.ctor()
extern void SmokeParticles__ctor_m42152342972AE5EE9DFFE1E52C99450F470806A4 (void);
// 0x00000192 System.Void UnityStandardAssets.Effects.WaterHoseParticles::Start()
extern void WaterHoseParticles_Start_m69FD918E507D29D4B392DC5E2E3928F0E46231CD (void);
// 0x00000193 System.Void UnityStandardAssets.Effects.WaterHoseParticles::OnParticleCollision(UnityEngine.GameObject)
extern void WaterHoseParticles_OnParticleCollision_m1BE62264E81B0C60C3229D53BD8EA4D950578D79 (void);
// 0x00000194 System.Void UnityStandardAssets.Effects.WaterHoseParticles::.ctor()
extern void WaterHoseParticles__ctor_m632D0D0466141E640795FD0BC0E7B21BF6A324EF (void);
// 0x00000195 System.Void UnityStandardAssets.Water.WaterBasic::Update()
extern void WaterBasic_Update_m79AE15CDF47AA396823D10220EBA44C34CD6F569 (void);
// 0x00000196 System.Void UnityStandardAssets.Water.WaterBasic::.ctor()
extern void WaterBasic__ctor_mF258EB45327E0929D2CB72B5266080C3F2E9CF4A (void);
// 0x00000197 System.Void UnityStandardAssets.Water.Displace::Awake()
extern void Displace_Awake_mE99DEB129DED3AF1991C408D98A52E342B4643A7 (void);
// 0x00000198 System.Void UnityStandardAssets.Water.Displace::OnEnable()
extern void Displace_OnEnable_m952A8DDD7BF19B262520923CACCAE21927044BD8 (void);
// 0x00000199 System.Void UnityStandardAssets.Water.Displace::OnDisable()
extern void Displace_OnDisable_m344E8D36AD672D92E465CD50F28E512A817B8B08 (void);
// 0x0000019A System.Void UnityStandardAssets.Water.Displace::.ctor()
extern void Displace__ctor_mA627066AA526C79EF3AA8D4FCDF70261CEA5CFA8 (void);
// 0x0000019B System.Void UnityStandardAssets.Water.GerstnerDisplace::.ctor()
extern void GerstnerDisplace__ctor_m718281CC56902E6BF82FAC236AB3043BFA0E9BFF (void);
// 0x0000019C System.Void UnityStandardAssets.Water.MeshContainer::.ctor(UnityEngine.Mesh)
extern void MeshContainer__ctor_mF1E5D7C018B2F9F756CECB8C1A0497406F833E3B (void);
// 0x0000019D System.Void UnityStandardAssets.Water.MeshContainer::Update()
extern void MeshContainer_Update_mA307DB7261AAD0254EBC0013D2DB7E30C12B94CA (void);
// 0x0000019E System.Void UnityStandardAssets.Water.PlanarReflection::Start()
extern void PlanarReflection_Start_mF4B62F45289EDE1798379F46BB22090CFE446874 (void);
// 0x0000019F UnityEngine.Camera UnityStandardAssets.Water.PlanarReflection::CreateReflectionCameraFor(UnityEngine.Camera)
extern void PlanarReflection_CreateReflectionCameraFor_m536231E7168798D1B53C3C69B4C3CE24F96C1734 (void);
// 0x000001A0 System.Void UnityStandardAssets.Water.PlanarReflection::SetStandardCameraParameter(UnityEngine.Camera,UnityEngine.LayerMask)
extern void PlanarReflection_SetStandardCameraParameter_m18039B2FBF79EDCC215634AA15719B070D665DF9 (void);
// 0x000001A1 UnityEngine.RenderTexture UnityStandardAssets.Water.PlanarReflection::CreateTextureFor(UnityEngine.Camera)
extern void PlanarReflection_CreateTextureFor_m5CC4B46CAFD4C2B62F6B2E90A3DDC566F82B73AF (void);
// 0x000001A2 System.Void UnityStandardAssets.Water.PlanarReflection::RenderHelpCameras(UnityEngine.Camera)
extern void PlanarReflection_RenderHelpCameras_m8EC47193F6B3B38703D2FC5FC26A69115FB93241 (void);
// 0x000001A3 System.Void UnityStandardAssets.Water.PlanarReflection::LateUpdate()
extern void PlanarReflection_LateUpdate_m67497232C28C05149AAA4EC85A3CCAB96D4F259C (void);
// 0x000001A4 System.Void UnityStandardAssets.Water.PlanarReflection::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern void PlanarReflection_WaterTileBeingRendered_mAD0310016829E5BB8BAE9F0313A4654C4C7039C1 (void);
// 0x000001A5 System.Void UnityStandardAssets.Water.PlanarReflection::OnEnable()
extern void PlanarReflection_OnEnable_m08B218FDAEA9D8E040927BEF9E011202783CC281 (void);
// 0x000001A6 System.Void UnityStandardAssets.Water.PlanarReflection::OnDisable()
extern void PlanarReflection_OnDisable_mE5DCEA557577A4B4CD3790829AF59BE81E4CAF36 (void);
// 0x000001A7 System.Void UnityStandardAssets.Water.PlanarReflection::RenderReflectionFor(UnityEngine.Camera,UnityEngine.Camera)
extern void PlanarReflection_RenderReflectionFor_m743FA341870F000B1626F691C9DED6E3714D5987 (void);
// 0x000001A8 System.Void UnityStandardAssets.Water.PlanarReflection::SaneCameraSettings(UnityEngine.Camera)
extern void PlanarReflection_SaneCameraSettings_m73A5C561F9B670BC714B3A4B5B6722BF8AD30C1E (void);
// 0x000001A9 UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateObliqueMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern void PlanarReflection_CalculateObliqueMatrix_mFA8D1838E4D40288C73A944485471F15A9EE13AB (void);
// 0x000001AA UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateReflectionMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern void PlanarReflection_CalculateReflectionMatrix_mC0F530AB64BBB6ADCAF28DD8E014D9388CEF9271 (void);
// 0x000001AB System.Single UnityStandardAssets.Water.PlanarReflection::Sgn(System.Single)
extern void PlanarReflection_Sgn_m2C181CAD6E9DADF5B8F618D2D01649A826F8939A (void);
// 0x000001AC UnityEngine.Vector4 UnityStandardAssets.Water.PlanarReflection::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void PlanarReflection_CameraSpacePlane_mBBA681992D91FE0286F9694E11D44BFE640A698B (void);
// 0x000001AD System.Void UnityStandardAssets.Water.PlanarReflection::.ctor()
extern void PlanarReflection__ctor_m8188E385A4C4CE7AB26873A83DB4377EA390A66F (void);
// 0x000001AE System.Void UnityStandardAssets.Water.SpecularLighting::Start()
extern void SpecularLighting_Start_mFE455D641AAD002581EA31536A30C2F01B650CEE (void);
// 0x000001AF System.Void UnityStandardAssets.Water.SpecularLighting::Update()
extern void SpecularLighting_Update_mC376C1CE2ECA4F4121AFE286F1F863EAF07720B1 (void);
// 0x000001B0 System.Void UnityStandardAssets.Water.SpecularLighting::.ctor()
extern void SpecularLighting__ctor_mA0D46E411DF992BD42BCA24875516A8A5A2AAB3E (void);
// 0x000001B1 System.Void UnityStandardAssets.Water.Water::OnWillRenderObject()
extern void Water_OnWillRenderObject_m030DC7B6995FEC2690883CAA454A4A72C0F5BD54 (void);
// 0x000001B2 System.Void UnityStandardAssets.Water.Water::OnDisable()
extern void Water_OnDisable_mBC5924D74718A4A22BA7AFA98296F9E0247F6FFD (void);
// 0x000001B3 System.Void UnityStandardAssets.Water.Water::Update()
extern void Water_Update_m67CF13DF8C862994509BF70D888E84C4CEB8BD84 (void);
// 0x000001B4 System.Void UnityStandardAssets.Water.Water::UpdateCameraModes(UnityEngine.Camera,UnityEngine.Camera)
extern void Water_UpdateCameraModes_m5A9B5A17F3C6714DC1E67305196CA9C477947011 (void);
// 0x000001B5 System.Void UnityStandardAssets.Water.Water::CreateWaterObjects(UnityEngine.Camera,UnityEngine.Camera&,UnityEngine.Camera&)
extern void Water_CreateWaterObjects_m759D0CAE5A10475C04C250DF826FC862BDF6A207 (void);
// 0x000001B6 UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::GetWaterMode()
extern void Water_GetWaterMode_mFF2310EBA5942F0B15B55B884F1923AF390F258D (void);
// 0x000001B7 UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::FindHardwareWaterSupport()
extern void Water_FindHardwareWaterSupport_mCC07F7400748514F5A89E92835812E047D763789 (void);
// 0x000001B8 UnityEngine.Vector4 UnityStandardAssets.Water.Water::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Water_CameraSpacePlane_mDE8B72357B143694C254E3411D4A36DB272F1CD2 (void);
// 0x000001B9 System.Void UnityStandardAssets.Water.Water::CalculateReflectionMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern void Water_CalculateReflectionMatrix_mD27C10A49F7964539E94CBAC003DA65EA42003C0 (void);
// 0x000001BA System.Void UnityStandardAssets.Water.Water::.ctor()
extern void Water__ctor_mF11768BB5C88F59E159BDDA9D4713A95BCDC7EDC (void);
// 0x000001BB System.Void UnityStandardAssets.Water.WaterBase::UpdateShader()
extern void WaterBase_UpdateShader_mF024410B93ACDEEB86F2784367D4371789A8ECC6 (void);
// 0x000001BC System.Void UnityStandardAssets.Water.WaterBase::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern void WaterBase_WaterTileBeingRendered_m5AE3A512DBA0F49C5480DC7AC0832C69DA2007D6 (void);
// 0x000001BD System.Void UnityStandardAssets.Water.WaterBase::Update()
extern void WaterBase_Update_m97AB1D3EC79F89868388DEE04B9C12AC7C4F516C (void);
// 0x000001BE System.Void UnityStandardAssets.Water.WaterBase::.ctor()
extern void WaterBase__ctor_mEF380720D189D0DF1DE0F40ACB847C00D12362CC (void);
// 0x000001BF System.Void UnityStandardAssets.Water.WaterTile::Start()
extern void WaterTile_Start_m59B733629B793EA675F0934D0283148809B2D944 (void);
// 0x000001C0 System.Void UnityStandardAssets.Water.WaterTile::AcquireComponents()
extern void WaterTile_AcquireComponents_mFD2764B5DDB1CC33CA9AF55BB0D1D570EC92AD0B (void);
// 0x000001C1 System.Void UnityStandardAssets.Water.WaterTile::OnWillRenderObject()
extern void WaterTile_OnWillRenderObject_m003657E9F9D7BBC9A618E204B3769E6F849C31E9 (void);
// 0x000001C2 System.Void UnityStandardAssets.Water.WaterTile::.ctor()
extern void WaterTile__ctor_m5B94DD8133797CA9BF11522024360A0A1049F5E5 (void);
// 0x000001C3 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnEnable()
extern void AxisTouchButton_OnEnable_mD8C2E1D724758527A2846C09CC7937830BA33AA9 (void);
// 0x000001C4 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::FindPairedButton()
extern void AxisTouchButton_FindPairedButton_m639BD2D1A09BE33E8C82071921C4C14C57A68B39 (void);
// 0x000001C5 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnDisable()
extern void AxisTouchButton_OnDisable_m5694ED6BFFCD06F121BBEA0FE552FF6B52D3078F (void);
// 0x000001C6 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void AxisTouchButton_OnPointerDown_m9AFBEB27DC7F3EA9E1EAE3BBF652EBC59BC66D4F (void);
// 0x000001C7 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void AxisTouchButton_OnPointerUp_mC38DDA9F687A46DE07977EBCFC2A9EF9242C248A (void);
// 0x000001C8 System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::.ctor()
extern void AxisTouchButton__ctor_mD1833F5DAAC806A70E989E4AC6A1C06BE04F2649 (void);
// 0x000001C9 System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::OnEnable()
extern void ButtonHandler_OnEnable_mD1D2CC3990A4EEDF5F393F0F5117E6634B2A5E9E (void);
// 0x000001CA System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetDownState()
extern void ButtonHandler_SetDownState_m7AB2F0D5E7AD859EBBBB3EDF459EF7279DCFA65E (void);
// 0x000001CB System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetUpState()
extern void ButtonHandler_SetUpState_m2BE684CD4BD7766903C83E3A2A78DDD38F941952 (void);
// 0x000001CC System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisPositiveState()
extern void ButtonHandler_SetAxisPositiveState_mA25C8B1267D683CF9CBF988958190D84EBE9EC2A (void);
// 0x000001CD System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNeutralState()
extern void ButtonHandler_SetAxisNeutralState_mBFD4413300A657D797F1035A3D772E0CD080C990 (void);
// 0x000001CE System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNegativeState()
extern void ButtonHandler_SetAxisNegativeState_mFD8E0D0F76D32DDEB582DA2FEA1F71A2B75C8B2A (void);
// 0x000001CF System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::Update()
extern void ButtonHandler_Update_m9949BD0A3FBB84B766C797804FC347F5CBD52699 (void);
// 0x000001D0 System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::.ctor()
extern void ButtonHandler__ctor_mD247ECA143AE9FA0DAA5D1D2E41ED6CB8D46BB08 (void);
// 0x000001D1 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::.cctor()
extern void CrossPlatformInputManager__cctor_m3DA11007BA9960F289314B3995540642637E6B96 (void);
// 0x000001D2 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SwitchActiveInputMethod(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod)
extern void CrossPlatformInputManager_SwitchActiveInputMethod_m5509783E9C56F86EBCEC9A51323FEAC33F456485 (void);
// 0x000001D3 System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::AxisExists(System.String)
extern void CrossPlatformInputManager_AxisExists_m4766470D2A3BCE44CFFE44BCAC0FDA0623B23372 (void);
// 0x000001D4 System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::ButtonExists(System.String)
extern void CrossPlatformInputManager_ButtonExists_m046F62C8CA5B80CB8B5EC9830C430C12155FBB4F (void);
// 0x000001D5 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::RegisterVirtualAxis(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis)
extern void CrossPlatformInputManager_RegisterVirtualAxis_mDA9D3A2181CD74F9096BF1C7807802DE353E4E1A (void);
// 0x000001D6 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::RegisterVirtualButton(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton)
extern void CrossPlatformInputManager_RegisterVirtualButton_m5E3A328FC6A6EC011EB337125A67468BDD86AAF4 (void);
// 0x000001D7 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::UnRegisterVirtualAxis(System.String)
extern void CrossPlatformInputManager_UnRegisterVirtualAxis_m64342E501FD520FE5FE28A1AB5CF1B56334168F7 (void);
// 0x000001D8 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::UnRegisterVirtualButton(System.String)
extern void CrossPlatformInputManager_UnRegisterVirtualButton_m6EFB512B492D6FD6475DE35B931F27CE5B107741 (void);
// 0x000001D9 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::VirtualAxisReference(System.String)
extern void CrossPlatformInputManager_VirtualAxisReference_m181E96561480060A9559B3749982548E347D634A (void);
// 0x000001DA System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxis(System.String)
extern void CrossPlatformInputManager_GetAxis_mB7AB6DA9693D497643353CF2B97A48C75F95007E (void);
// 0x000001DB System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxisRaw(System.String)
extern void CrossPlatformInputManager_GetAxisRaw_m2B6A9EE32A0AE8ABD902BB5F6B74D0BB814C42E8 (void);
// 0x000001DC System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxis(System.String,System.Boolean)
extern void CrossPlatformInputManager_GetAxis_m9F2D914919AF9FC38087D229A6B6C1845045B5EE (void);
// 0x000001DD System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButton(System.String)
extern void CrossPlatformInputManager_GetButton_mF4FDF5B4FE1B224C4271E9B832CD8BA08AE73EFF (void);
// 0x000001DE System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButtonDown(System.String)
extern void CrossPlatformInputManager_GetButtonDown_mCA7A96BB65979ADD959C6BFD8B6A3A1A7B1CDC26 (void);
// 0x000001DF System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButtonUp(System.String)
extern void CrossPlatformInputManager_GetButtonUp_mFB33294740857F8DE0BF7ACF110938B80C4DBDA5 (void);
// 0x000001E0 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetButtonDown(System.String)
extern void CrossPlatformInputManager_SetButtonDown_mF5AB872C58C8C9A01435623E0C2679F68B768595 (void);
// 0x000001E1 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetButtonUp(System.String)
extern void CrossPlatformInputManager_SetButtonUp_m6676EAE387ED7F03018429E9198113891378087B (void);
// 0x000001E2 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisPositive(System.String)
extern void CrossPlatformInputManager_SetAxisPositive_m4ACAB49EC1004299FDC5A2F547E9E5EC66F2F1DB (void);
// 0x000001E3 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisNegative(System.String)
extern void CrossPlatformInputManager_SetAxisNegative_m5A5DE4693430AB47774341EFB6B368EBD45A4CC1 (void);
// 0x000001E4 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisZero(System.String)
extern void CrossPlatformInputManager_SetAxisZero_m670B5FE17F3239D65FDC176C613309D87D86B1CC (void);
// 0x000001E5 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxis(System.String,System.Single)
extern void CrossPlatformInputManager_SetAxis_mDB627063A19E94CF4C7BDD682EB7205D46F9619A (void);
// 0x000001E6 UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::get_mousePosition()
extern void CrossPlatformInputManager_get_mousePosition_mE656A61832658E0E3DB01CC62D3998454087CAA8 (void);
// 0x000001E7 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionX(System.Single)
extern void CrossPlatformInputManager_SetVirtualMousePositionX_m963A27C3CD1CA33ED0054E029C046E6A492E4EF0 (void);
// 0x000001E8 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionY(System.Single)
extern void CrossPlatformInputManager_SetVirtualMousePositionY_m7EC2205F945F0DF0BAC67DC9907C8EDEFE97169E (void);
// 0x000001E9 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionZ(System.Single)
extern void CrossPlatformInputManager_SetVirtualMousePositionZ_mAA1C2749743422A7326CDC4C40BEABCD1C6C1DDF (void);
// 0x000001EA System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_name()
extern void VirtualAxis_get_name_mBBB8C139AABF771FC91A61B8444F835908F25A39 (void);
// 0x000001EB System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::set_name(System.String)
extern void VirtualAxis_set_name_mE474E7B124D3E784ADF9D3532BC0A75F2684A2A2 (void);
// 0x000001EC System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_matchWithInputManager()
extern void VirtualAxis_get_matchWithInputManager_m531F00E4DD93A41FB0244FB71343FC9FB07DC4DF (void);
// 0x000001ED System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::set_matchWithInputManager(System.Boolean)
extern void VirtualAxis_set_matchWithInputManager_m61D9709975B67900CAD15101BF9C3AD57D2D88D0 (void);
// 0x000001EE System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::.ctor(System.String)
extern void VirtualAxis__ctor_m88EDC66F4BAB51D1BA2BDB502EBE995F08F42E64 (void);
// 0x000001EF System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::.ctor(System.String,System.Boolean)
extern void VirtualAxis__ctor_m486C4129232F0F15151DA882C1C9F1DFDFE5D047 (void);
// 0x000001F0 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::Remove()
extern void VirtualAxis_Remove_mCC5EF7DB8EC863AC7030AC9AB92F46A723BB7748 (void);
// 0x000001F1 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::Update(System.Single)
extern void VirtualAxis_Update_m2A06394E13EA748D09D1506235BAB669636D9CBB (void);
// 0x000001F2 System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_GetValue()
extern void VirtualAxis_get_GetValue_mAC358FAC6484FAA00EB187E0583ECD4576794C44 (void);
// 0x000001F3 System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_GetValueRaw()
extern void VirtualAxis_get_GetValueRaw_mDCDC78FB43C16D8F65B9BE8799F0053DBB64007E (void);
// 0x000001F4 System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_name()
extern void VirtualButton_get_name_m0B8D3FE4453224CE39D4316089F38D80399B449C (void);
// 0x000001F5 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::set_name(System.String)
extern void VirtualButton_set_name_mEF2365424C0A01C48B1D95066D01E5DC0B5B6DFA (void);
// 0x000001F6 System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_matchWithInputManager()
extern void VirtualButton_get_matchWithInputManager_m614D75F3386EB15F081A2F5D548B5743589BE939 (void);
// 0x000001F7 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::set_matchWithInputManager(System.Boolean)
extern void VirtualButton_set_matchWithInputManager_m371812D456658C76DBC0128EC168A3091BBDF5C0 (void);
// 0x000001F8 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::.ctor(System.String)
extern void VirtualButton__ctor_m71595BAF216317FBF79F564F306D3A87F430EDE4 (void);
// 0x000001F9 System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::.ctor(System.String,System.Boolean)
extern void VirtualButton__ctor_m50F9D1236BD4CFA9C3136E0D9321DF9604D5C021 (void);
// 0x000001FA System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::Pressed()
extern void VirtualButton_Pressed_mAAC725DA03D80EC7275B0F0B82528E3C21670ADE (void);
// 0x000001FB System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::Released()
extern void VirtualButton_Released_mD4D0FD8E203575FE98152A62BF6B16071E383F5C (void);
// 0x000001FC System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::Remove()
extern void VirtualButton_Remove_m040109DCD13EF3704399353ED4BC4AAB35539DF6 (void);
// 0x000001FD System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_GetButton()
extern void VirtualButton_get_GetButton_mD1EBB3A0B0A88B5CC0589120B42106447F9ED065 (void);
// 0x000001FE System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_GetButtonDown()
extern void VirtualButton_get_GetButtonDown_m6C10A64F6C990B87627E8DDE6C1FFCFEBCD8FDB7 (void);
// 0x000001FF System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_GetButtonUp()
extern void VirtualButton_get_GetButtonUp_mE7D1541E27B10531F1542C55781ED62EED0DC37F (void);
// 0x00000200 System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::Update()
extern void InputAxisScrollbar_Update_m2E6071A5B2BA9BBE9F0D5AA4B7285053BD83FDCB (void);
// 0x00000201 System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::HandleInput(System.Single)
extern void InputAxisScrollbar_HandleInput_m4E53774409CC762C0D34D83B186D18266E21A280 (void);
// 0x00000202 System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::.ctor()
extern void InputAxisScrollbar__ctor_mBDEC7527CCE962AC682F51B9179CC6E139CDCE98 (void);
// 0x00000203 System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnEnable()
extern void Joystick_OnEnable_m24A5EA0EDD73F1486837F1F08DF4B80721EF754C (void);
// 0x00000204 System.Void UnityStandardAssets.CrossPlatformInput.Joystick::Start()
extern void Joystick_Start_m8CE36CA457A4A410C33F008316130997880DB58F (void);
// 0x00000205 System.Void UnityStandardAssets.CrossPlatformInput.Joystick::UpdateVirtualAxes(UnityEngine.Vector3)
extern void Joystick_UpdateVirtualAxes_m7D6CB9CF992E3F6A52D21E37B0293E2EF11EDDE0 (void);
// 0x00000206 System.Void UnityStandardAssets.CrossPlatformInput.Joystick::CreateVirtualAxes()
extern void Joystick_CreateVirtualAxes_m10235FD4D0F3393790EBF5A58391886D78549173 (void);
// 0x00000207 System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnDrag_mA04BB282F1D24DA48FCFCBD7889D4964C15BC7F0 (void);
// 0x00000208 System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerUp_m8E4F8B50A7E5665DA2226F16EF6CDE5BAE5326A5 (void);
// 0x00000209 System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerDown_m6AF1B2BF22F35B5E3498907EC51C8C0561729FAD (void);
// 0x0000020A System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnDisable()
extern void Joystick_OnDisable_m656AD25023372C1AB0A455F73218ABFB0145B8FF (void);
// 0x0000020B System.Void UnityStandardAssets.CrossPlatformInput.Joystick::.ctor()
extern void Joystick__ctor_mE448490D7C35B109542036993D0D4C1A94998D4E (void);
// 0x0000020C System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::OnEnable()
extern void MobileControlRig_OnEnable_mD1C1F78BDA9EA618AB12221F826AE0DDB93BB1E6 (void);
// 0x0000020D System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::Start()
extern void MobileControlRig_Start_m5DE87E0E24CB89A6D9DB4755EC7443592630ADA1 (void);
// 0x0000020E System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::CheckEnableControlRig()
extern void MobileControlRig_CheckEnableControlRig_m1708EC5391D82F8050E2F3E2939FE19A02B01DED (void);
// 0x0000020F System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::EnableControlRig(System.Boolean)
extern void MobileControlRig_EnableControlRig_m8F79B98DE64549179F06D849E9F97E139B388221 (void);
// 0x00000210 System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::.ctor()
extern void MobileControlRig__ctor_m127520DB08F2DFE95FA839E1C5043E603D2C61DA (void);
// 0x00000211 System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnEnable()
extern void TiltInput_OnEnable_m9419C0AFC8EABA0CB8F02488E6653CB6C0ED262B (void);
// 0x00000212 System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::Update()
extern void TiltInput_Update_mC35CD38493ECF6EB17DB66B5219C8C817F88F034 (void);
// 0x00000213 System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnDisable()
extern void TiltInput_OnDisable_m2125CE9F5BD06B2C184F6642BCE6585218A6E223 (void);
// 0x00000214 System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::.ctor()
extern void TiltInput__ctor_m20D49463D098180E19FA5980DCE712A520358551 (void);
// 0x00000215 System.Void UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::.ctor()
extern void AxisMapping__ctor_mC15745B60FFEA4482E885B02C8910FC2A0972981 (void);
// 0x00000216 System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnEnable()
extern void TouchPad_OnEnable_m8046F501C2F56DD64ADF83DCD0923DD745E4806F (void);
// 0x00000217 System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::Start()
extern void TouchPad_Start_mCD5535ABCB35D5C1660120B56C74EFC6FD7D8D93 (void);
// 0x00000218 System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::CreateVirtualAxes()
extern void TouchPad_CreateVirtualAxes_m09ACDDC26B1A01D3904821C3547FE8CFC67CD6F0 (void);
// 0x00000219 System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::UpdateVirtualAxes(UnityEngine.Vector3)
extern void TouchPad_UpdateVirtualAxes_m1E34D78B61E20D535CF499A7D7A0690D8082EBDA (void);
// 0x0000021A System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void TouchPad_OnPointerDown_mDE5A780D09754F413B01C3D71717A82E101F492B (void);
// 0x0000021B System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::Update()
extern void TouchPad_Update_m89E6817453070E87C77A803271689E69AC83111C (void);
// 0x0000021C System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TouchPad_OnPointerUp_m88F91FCE7A72803EF92EF4F2518C1082AA221C68 (void);
// 0x0000021D System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnDisable()
extern void TouchPad_OnDisable_mB3CFD5F3AF6F4BBB0DB43DFDFFBC6ABCAE9B25F2 (void);
// 0x0000021E System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::.ctor()
extern void TouchPad__ctor_m222A3522FA022C53F488FBB4F482F913ED34C009 (void);
// 0x0000021F UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::get_virtualMousePosition()
extern void VirtualInput_get_virtualMousePosition_m9DF87F8DAE8FA5CF9BC85284922026025AABB1FF (void);
// 0x00000220 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::set_virtualMousePosition(UnityEngine.Vector3)
extern void VirtualInput_set_virtualMousePosition_m3D48CAC3DC8D5A673C81F6986C8FE48DCD19CB59 (void);
// 0x00000221 System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::AxisExists(System.String)
extern void VirtualInput_AxisExists_m01EC1FD139D4DC78B03BD91601E694BB0FD99FCD (void);
// 0x00000222 System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::ButtonExists(System.String)
extern void VirtualInput_ButtonExists_m504FCFBACFAF025BD335F73B9E6365D0877F21FC (void);
// 0x00000223 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::RegisterVirtualAxis(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis)
extern void VirtualInput_RegisterVirtualAxis_m4D5DAB8CD547D5200513D860FDA6DC3930150BAC (void);
// 0x00000224 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::RegisterVirtualButton(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton)
extern void VirtualInput_RegisterVirtualButton_m7AEF52824F354DA313380E110D03D2800F1A9B21 (void);
// 0x00000225 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::UnRegisterVirtualAxis(System.String)
extern void VirtualInput_UnRegisterVirtualAxis_mDC436E4797B5E7091462A67AC29559F8DE1FD688 (void);
// 0x00000226 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::UnRegisterVirtualButton(System.String)
extern void VirtualInput_UnRegisterVirtualButton_mFD11E0016A1865D5A0C3E05ED7DC6C3F15DAF2EB (void);
// 0x00000227 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.VirtualInput::VirtualAxisReference(System.String)
extern void VirtualInput_VirtualAxisReference_mB9BFA6AD246B52D158CE8309581FE468DF756914 (void);
// 0x00000228 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionX(System.Single)
extern void VirtualInput_SetVirtualMousePositionX_mDEE15F467D72B1B64C99473FFB2E7C3D65175B70 (void);
// 0x00000229 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionY(System.Single)
extern void VirtualInput_SetVirtualMousePositionY_m4A9FB6CF5DBD17EABABABFC241BBCBD2720B3EA9 (void);
// 0x0000022A System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionZ(System.Single)
extern void VirtualInput_SetVirtualMousePositionZ_m5EF12D8C9F3A8F7FCC443E682DA1F456AAD5B1DD (void);
// 0x0000022B System.Single UnityStandardAssets.CrossPlatformInput.VirtualInput::GetAxis(System.String,System.Boolean)
// 0x0000022C System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButton(System.String)
// 0x0000022D System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButtonDown(System.String)
// 0x0000022E System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButtonUp(System.String)
// 0x0000022F System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetButtonDown(System.String)
// 0x00000230 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetButtonUp(System.String)
// 0x00000231 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisPositive(System.String)
// 0x00000232 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisNegative(System.String)
// 0x00000233 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisZero(System.String)
// 0x00000234 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxis(System.String,System.Single)
// 0x00000235 UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::MousePosition()
// 0x00000236 System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::.ctor()
extern void VirtualInput__ctor_m39FA60B6A3FB2A793B3825E6EE488A0262224DCB (void);
// 0x00000237 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::AddButton(System.String)
extern void MobileInput_AddButton_mCE54D249830BB224C0E7868831F5DE9872721ECA (void);
// 0x00000238 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::AddAxes(System.String)
extern void MobileInput_AddAxes_m9E1EF0D37885B89830E25966ACA9241FE6A67B05 (void);
// 0x00000239 System.Single UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetAxis(System.String,System.Boolean)
extern void MobileInput_GetAxis_mF7E4D1D5BB15C8D6DBBFA1D0ACB34C07BC52C3AB (void);
// 0x0000023A System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetButtonDown(System.String)
extern void MobileInput_SetButtonDown_mC9D7C110515111912F656057FEFBEF8E5880D70C (void);
// 0x0000023B System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetButtonUp(System.String)
extern void MobileInput_SetButtonUp_mAC2717F036575064C64280479659887F0B448410 (void);
// 0x0000023C System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisPositive(System.String)
extern void MobileInput_SetAxisPositive_mDB01EA55F7F75B0C1FA2D79DC42D742F4B5FC45D (void);
// 0x0000023D System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisNegative(System.String)
extern void MobileInput_SetAxisNegative_mD1181DCDCC49DF0D476E9B525AF8274D2E2386FF (void);
// 0x0000023E System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisZero(System.String)
extern void MobileInput_SetAxisZero_m93E3632453634975E55A9D4181FED4D5DBE15694 (void);
// 0x0000023F System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxis(System.String,System.Single)
extern void MobileInput_SetAxis_m57CFF9414DCE627C9AA83AECA9E3F61013EB94FB (void);
// 0x00000240 System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButtonDown(System.String)
extern void MobileInput_GetButtonDown_m4AA7D6AB5B05ABCCA3C1908FA877E26998FBB71B (void);
// 0x00000241 System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButtonUp(System.String)
extern void MobileInput_GetButtonUp_m473F8F5AA2D5CDCB5505837AA1CE17077C0D2C0A (void);
// 0x00000242 System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButton(System.String)
extern void MobileInput_GetButton_mAAF8F610076711577A20B084469AC344454E6632 (void);
// 0x00000243 UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::MousePosition()
extern void MobileInput_MousePosition_mDE6DFB3175A5C3A75D210339227B70DA6E0CEA35 (void);
// 0x00000244 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::.ctor()
extern void MobileInput__ctor_m50B1FDC06838FEABE2C9E43F8AE4476426413240 (void);
// 0x00000245 System.Single UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetAxis(System.String,System.Boolean)
extern void StandaloneInput_GetAxis_m879778236E844FFB175FC5EFED4D50C31FE914A0 (void);
// 0x00000246 System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButton(System.String)
extern void StandaloneInput_GetButton_m6082577C808C1ED0E6D41434A917CAA90C6054DC (void);
// 0x00000247 System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButtonDown(System.String)
extern void StandaloneInput_GetButtonDown_m1170A0D0F15EFAA2C69F865A8FBED63EED6F55C2 (void);
// 0x00000248 System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButtonUp(System.String)
extern void StandaloneInput_GetButtonUp_mD1A9A9DA63DEF0BB1F5743E1E92A5A65DA89B28B (void);
// 0x00000249 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetButtonDown(System.String)
extern void StandaloneInput_SetButtonDown_mC273D780F0692CD7233597FF543B1A83A947F34A (void);
// 0x0000024A System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetButtonUp(System.String)
extern void StandaloneInput_SetButtonUp_mFF786A10495F3FFAB08DA57378B11BE278C9670D (void);
// 0x0000024B System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisPositive(System.String)
extern void StandaloneInput_SetAxisPositive_m48E3A614364EA257E208F312D690B4156545E314 (void);
// 0x0000024C System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisNegative(System.String)
extern void StandaloneInput_SetAxisNegative_mFC6573E09AAD9371BDF288183584126E3C45BB02 (void);
// 0x0000024D System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisZero(System.String)
extern void StandaloneInput_SetAxisZero_m76EE6A91ADD9C7A867B279C16239FCC197FCBA0B (void);
// 0x0000024E System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxis(System.String,System.Single)
extern void StandaloneInput_SetAxis_m38950004F9B6DD18B1ADFE2061DEF3356816E31F (void);
// 0x0000024F UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::MousePosition()
extern void StandaloneInput_MousePosition_mFBD800C92551160E40AC7EB8549E760240334BFE (void);
// 0x00000250 System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::.ctor()
extern void StandaloneInput__ctor_mD89211F517B446A581B7D7AA1079D8FEBFEC389D (void);
// 0x00000251 System.Void UnityStandardAssets.Vehicles.Car.BrakeLight::Start()
extern void BrakeLight_Start_m79E75E76BA79E6E5244DB4A9CCB9B98F494F763B (void);
// 0x00000252 System.Void UnityStandardAssets.Vehicles.Car.BrakeLight::Update()
extern void BrakeLight_Update_m6FC731716E0455D3D605DE4C70BD708FC1102FAA (void);
// 0x00000253 System.Void UnityStandardAssets.Vehicles.Car.BrakeLight::.ctor()
extern void BrakeLight__ctor_m9AF99D86B450EE4739C6AD76EB98B83EE8B7E56D (void);
// 0x00000254 System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::Awake()
extern void CarAIControl_Awake_mD2089BE1D633E8625AE3465FB4A97EDD7228BADB (void);
// 0x00000255 System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::FixedUpdate()
extern void CarAIControl_FixedUpdate_m08EAE849DA5C637C28BF4A76A820940995B79CFB (void);
// 0x00000256 System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::OnCollisionStay(UnityEngine.Collision)
extern void CarAIControl_OnCollisionStay_m355A6C6957FF4368F30C2BA2C1437CFA4C7BEA8D (void);
// 0x00000257 System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::SetTarget(UnityEngine.Transform)
extern void CarAIControl_SetTarget_mA7FD3633368824E4AF643F5D8745D290122F7F57 (void);
// 0x00000258 System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::.ctor()
extern void CarAIControl__ctor_mC112AA6249F00CA8A4DA12A2A2DB4ACA65E30A25 (void);
// 0x00000259 System.Void UnityStandardAssets.Vehicles.Car.CarAudio::StartSound()
extern void CarAudio_StartSound_mDC58A4590A6CCE8F65EDF936F6A1F8335E20ECD6 (void);
// 0x0000025A System.Void UnityStandardAssets.Vehicles.Car.CarAudio::StopSound()
extern void CarAudio_StopSound_m4D15199B8BD77034BF556D616839D9D0DC7C6989 (void);
// 0x0000025B System.Void UnityStandardAssets.Vehicles.Car.CarAudio::Update()
extern void CarAudio_Update_mE4A3EA645511C5948CD8DDAF5A8F4CCA8C596F86 (void);
// 0x0000025C UnityEngine.AudioSource UnityStandardAssets.Vehicles.Car.CarAudio::SetUpEngineAudioSource(UnityEngine.AudioClip)
extern void CarAudio_SetUpEngineAudioSource_m27A0CB49228CF678DDB00E47EF1B997C095D2867 (void);
// 0x0000025D System.Single UnityStandardAssets.Vehicles.Car.CarAudio::ULerp(System.Single,System.Single,System.Single)
extern void CarAudio_ULerp_m224D3E2D54A1BAEC55443940996F25A262766726 (void);
// 0x0000025E System.Void UnityStandardAssets.Vehicles.Car.CarAudio::.ctor()
extern void CarAudio__ctor_m2A1E401BA72ED226F84CD57335C8A3E1EC4FF569 (void);
// 0x0000025F System.Boolean UnityStandardAssets.Vehicles.Car.CarController::get_Skidding()
extern void CarController_get_Skidding_m30527ED8ED64198664ABD9F52683D5175E01E53D (void);
// 0x00000260 System.Void UnityStandardAssets.Vehicles.Car.CarController::set_Skidding(System.Boolean)
extern void CarController_set_Skidding_m0E17DA6D5A3CF8210BF8A032896B4889146EC619 (void);
// 0x00000261 System.Single UnityStandardAssets.Vehicles.Car.CarController::get_BrakeInput()
extern void CarController_get_BrakeInput_m38CBD2496997600DCE24280D4A6C8BD45D764EB1 (void);
// 0x00000262 System.Void UnityStandardAssets.Vehicles.Car.CarController::set_BrakeInput(System.Single)
extern void CarController_set_BrakeInput_m2D9D58EE619793C1694E8425B5E753B73A088BA8 (void);
// 0x00000263 System.Single UnityStandardAssets.Vehicles.Car.CarController::get_CurrentSteerAngle()
extern void CarController_get_CurrentSteerAngle_mA67C5DFB1A0EA0C52D8DD24A99E9E403F556B026 (void);
// 0x00000264 System.Single UnityStandardAssets.Vehicles.Car.CarController::get_CurrentSpeed()
extern void CarController_get_CurrentSpeed_mAAF3026B379B0C2B7DA31EAB953B5FF27EE8AA17 (void);
// 0x00000265 System.Single UnityStandardAssets.Vehicles.Car.CarController::get_MaxSpeed()
extern void CarController_get_MaxSpeed_m36AF0F96E3D85904C03313EE0BBC1C97B29247FC (void);
// 0x00000266 System.Single UnityStandardAssets.Vehicles.Car.CarController::get_Revs()
extern void CarController_get_Revs_mF62F270CF0A3EE218FC8706431206D8B4A97D756 (void);
// 0x00000267 System.Void UnityStandardAssets.Vehicles.Car.CarController::set_Revs(System.Single)
extern void CarController_set_Revs_m4CFD510A5533F9EF3CFA8E2C80B2A3188087FCAF (void);
// 0x00000268 System.Single UnityStandardAssets.Vehicles.Car.CarController::get_AccelInput()
extern void CarController_get_AccelInput_m6508BF98BCC6E7EFC016ADE800B6C2FFDF2A2327 (void);
// 0x00000269 System.Void UnityStandardAssets.Vehicles.Car.CarController::set_AccelInput(System.Single)
extern void CarController_set_AccelInput_m7398B7A62BA5D9C65E4D1A8E6C86E2ACA082E65C (void);
// 0x0000026A System.Void UnityStandardAssets.Vehicles.Car.CarController::Start()
extern void CarController_Start_m56C0595BD2EF0197B75057F46CFF0D28C93F19B2 (void);
// 0x0000026B System.Void UnityStandardAssets.Vehicles.Car.CarController::GearChanging()
extern void CarController_GearChanging_m44D35FE8CF69D24143C83F445D0FA9988AB6D5F0 (void);
// 0x0000026C System.Single UnityStandardAssets.Vehicles.Car.CarController::CurveFactor(System.Single)
extern void CarController_CurveFactor_m063F5F48DE03D5901B393E327164E1E10F49CE52 (void);
// 0x0000026D System.Single UnityStandardAssets.Vehicles.Car.CarController::ULerp(System.Single,System.Single,System.Single)
extern void CarController_ULerp_m53C2DB4BA45DC0464846C839B8D27AAB395BFFEA (void);
// 0x0000026E System.Void UnityStandardAssets.Vehicles.Car.CarController::CalculateGearFactor()
extern void CarController_CalculateGearFactor_mB7A33A4892FBC6768D35A6D0E1C7462C645EEAD8 (void);
// 0x0000026F System.Void UnityStandardAssets.Vehicles.Car.CarController::CalculateRevs()
extern void CarController_CalculateRevs_mB96F83186C7F570B0B379FC9360EF723EC96AEED (void);
// 0x00000270 System.Void UnityStandardAssets.Vehicles.Car.CarController::Move(System.Single,System.Single,System.Single,System.Single)
extern void CarController_Move_m5F35A2C2386A7CBAED5561846C742F80647FCB0A (void);
// 0x00000271 System.Void UnityStandardAssets.Vehicles.Car.CarController::CapSpeed()
extern void CarController_CapSpeed_mBCACA1CB04D7A83D933A1C9DE99F56C0965A9CC1 (void);
// 0x00000272 System.Void UnityStandardAssets.Vehicles.Car.CarController::ApplyDrive(System.Single,System.Single)
extern void CarController_ApplyDrive_mE52790A94B93C3BD86B9CBEA3259A7E2CF2A5D00 (void);
// 0x00000273 System.Void UnityStandardAssets.Vehicles.Car.CarController::SteerHelper()
extern void CarController_SteerHelper_m7493A75F868F43301DB0C074EE823CDF8CBC1A26 (void);
// 0x00000274 System.Void UnityStandardAssets.Vehicles.Car.CarController::AddDownForce()
extern void CarController_AddDownForce_m31689FF75806FE6129C6E9605B4FB79A7BEDBC98 (void);
// 0x00000275 System.Void UnityStandardAssets.Vehicles.Car.CarController::CheckForWheelSpin()
extern void CarController_CheckForWheelSpin_m58909D01006D82C65D0963E105B8BEEE1EE50D5C (void);
// 0x00000276 System.Void UnityStandardAssets.Vehicles.Car.CarController::TractionControl()
extern void CarController_TractionControl_mFD6A5BD1002A9A02A2FF8F507F9D87CA9596F2D6 (void);
// 0x00000277 System.Void UnityStandardAssets.Vehicles.Car.CarController::AdjustTorque(System.Single)
extern void CarController_AdjustTorque_mC1E7ABD0BBFA5D26ABD82AAFDD02CFB27452CD75 (void);
// 0x00000278 System.Boolean UnityStandardAssets.Vehicles.Car.CarController::AnySkidSoundPlaying()
extern void CarController_AnySkidSoundPlaying_m1EDCA6E8100899C1C54E1EC10F8CCD37D111BAD4 (void);
// 0x00000279 System.Void UnityStandardAssets.Vehicles.Car.CarController::.ctor()
extern void CarController__ctor_m83D4A43409B7D2E4859598C533392550CDC46F57 (void);
// 0x0000027A System.Void UnityStandardAssets.Vehicles.Car.CarController::.cctor()
extern void CarController__cctor_m1D39E9B2ACE5BC3D53CC3FB2C47C9416877B8E65 (void);
// 0x0000027B System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::Start()
extern void CarSelfRighting_Start_m4996445BA84866C4C0C59F5409604510B164E308 (void);
// 0x0000027C System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::Update()
extern void CarSelfRighting_Update_m8ED1D5510F5794AD2521B39D35A5332DF52B2407 (void);
// 0x0000027D System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::RightCar()
extern void CarSelfRighting_RightCar_mFD7EBDC74A6F7690147946962C95D6598049C0C4 (void);
// 0x0000027E System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::.ctor()
extern void CarSelfRighting__ctor_m7A3309295BF03808C023FBFFA718F423308BA38E (void);
// 0x0000027F System.Void UnityStandardAssets.Vehicles.Car.CarUserControl::Awake()
extern void CarUserControl_Awake_m18597F775E26B5CB33F33FA399D407672596A5B8 (void);
// 0x00000280 System.Void UnityStandardAssets.Vehicles.Car.CarUserControl::FixedUpdate()
extern void CarUserControl_FixedUpdate_m392895DDE302A2119D120574923A3509A47EE843 (void);
// 0x00000281 System.Void UnityStandardAssets.Vehicles.Car.CarUserControl::.ctor()
extern void CarUserControl__ctor_mDE50C7EECDF3B283FE46CF2C9AA4B180BC23C2CA (void);
// 0x00000282 System.Void UnityStandardAssets.Vehicles.Car.Mudguard::Start()
extern void Mudguard_Start_mB6997804B6CF001D668D0EA6178F9E129BC176F0 (void);
// 0x00000283 System.Void UnityStandardAssets.Vehicles.Car.Mudguard::Update()
extern void Mudguard_Update_m227D04AF7B9078A8E3F3486B75F8ADCCB1914367 (void);
// 0x00000284 System.Void UnityStandardAssets.Vehicles.Car.Mudguard::.ctor()
extern void Mudguard__ctor_mAF35227ABDB228B503C74F20EF3369488E4F730D (void);
// 0x00000285 System.Collections.IEnumerator UnityStandardAssets.Vehicles.Car.SkidTrail::Start()
extern void SkidTrail_Start_m2695ED24C08B06196C72289F1966031DEF0EA411 (void);
// 0x00000286 System.Void UnityStandardAssets.Vehicles.Car.SkidTrail::.ctor()
extern void SkidTrail__ctor_m91FCFC15149FA5B42C14DC21BAAC3FBF508ED90B (void);
// 0x00000287 System.Void UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>d__1::.ctor(System.Int32)
extern void U3CStartU3Ed__1__ctor_m7AFCB32BFE8C866374C483F7A8B4C8406CC6B5CD (void);
// 0x00000288 System.Void UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>d__1::System.IDisposable.Dispose()
extern void U3CStartU3Ed__1_System_IDisposable_Dispose_m50EEBFFF90F957EAA72D3FC5A5B0AC712C64C563 (void);
// 0x00000289 System.Boolean UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_m2A889D196AC731C52CBFAD3F7DE71681D7BBD25E (void);
// 0x0000028A System.Object UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0BFD033FFC2B2E1DD5B1026D5BAF6BE7C97481C (void);
// 0x0000028B System.Void UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>d__1::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m9513A2F5ED5939A57CE31950DC6594B71D4FE876 (void);
// 0x0000028C System.Object UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mA7EABF66D3685C35C314600934F17B6C28BB62AD (void);
// 0x0000028D System.Void UnityStandardAssets.Vehicles.Car.Suspension::Start()
extern void Suspension_Start_mF2921EA9BEDC0FA76051D860607EB470CEF91D78 (void);
// 0x0000028E System.Void UnityStandardAssets.Vehicles.Car.Suspension::Update()
extern void Suspension_Update_m8195490C7663062F131370651C6D2291ABBDE3F5 (void);
// 0x0000028F System.Void UnityStandardAssets.Vehicles.Car.Suspension::.ctor()
extern void Suspension__ctor_m8AA95141EE8417156AEEB201139DF6DA4EBD2F30 (void);
// 0x00000290 System.Boolean UnityStandardAssets.Vehicles.Car.WheelEffects::get_skidding()
extern void WheelEffects_get_skidding_m1EBCDCFEDB16686E9B2D4B73FEE0482B9337EE97 (void);
// 0x00000291 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::set_skidding(System.Boolean)
extern void WheelEffects_set_skidding_mBA4FE70870D08789EDF7C7EF7060E53BBA7C8C49 (void);
// 0x00000292 System.Boolean UnityStandardAssets.Vehicles.Car.WheelEffects::get_PlayingAudio()
extern void WheelEffects_get_PlayingAudio_mA04B2C917A3F14A87066DC812D7FBA268F02AF36 (void);
// 0x00000293 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::set_PlayingAudio(System.Boolean)
extern void WheelEffects_set_PlayingAudio_m2EEE94A5D8D9BF9517AD6DA76FA26E9D238A23AC (void);
// 0x00000294 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::Start()
extern void WheelEffects_Start_mF995D30D628E703C7CC835FBBFBDEDBFBDE5DA92 (void);
// 0x00000295 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::EmitTyreSmoke()
extern void WheelEffects_EmitTyreSmoke_m58A2563671E5A909B9544E4754101B8F2EC91CDF (void);
// 0x00000296 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::PlayAudio()
extern void WheelEffects_PlayAudio_mD4BDB2BFA5BB4FB8BE4024FCF2382CEC30CCA071 (void);
// 0x00000297 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::StopAudio()
extern void WheelEffects_StopAudio_mF919DD64488467022850E3323492E07CE88497DD (void);
// 0x00000298 System.Collections.IEnumerator UnityStandardAssets.Vehicles.Car.WheelEffects::StartSkidTrail()
extern void WheelEffects_StartSkidTrail_mAA0A3A8AE8E3C79A9C80898154942444FEC10323 (void);
// 0x00000299 System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::EndSkidTrail()
extern void WheelEffects_EndSkidTrail_mDABC126E04064635FA218790E33C19E171030975 (void);
// 0x0000029A System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::.ctor()
extern void WheelEffects__ctor_m39F4618C7E58523783B781C74CDB25D115E4E4BE (void);
// 0x0000029B System.Void UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>d__18::.ctor(System.Int32)
extern void U3CStartSkidTrailU3Ed__18__ctor_mC72F95742347617153FEBDD8C8675023C457A0E4 (void);
// 0x0000029C System.Void UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>d__18::System.IDisposable.Dispose()
extern void U3CStartSkidTrailU3Ed__18_System_IDisposable_Dispose_mD3F45C2E53A7527B65E4DF777B4D97296E42FB37 (void);
// 0x0000029D System.Boolean UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>d__18::MoveNext()
extern void U3CStartSkidTrailU3Ed__18_MoveNext_m1091256D4BD86BD61651285CD5D8CD518563C5D0 (void);
// 0x0000029E System.Object UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartSkidTrailU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA8759211E822567D82E01677B3C49B92F5A63395 (void);
// 0x0000029F System.Void UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>d__18::System.Collections.IEnumerator.Reset()
extern void U3CStartSkidTrailU3Ed__18_System_Collections_IEnumerator_Reset_m981418E0BAC5979456EF45FCD3D5917BA5B4F352 (void);
// 0x000002A0 System.Object UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CStartSkidTrailU3Ed__18_System_Collections_IEnumerator_get_Current_mF42C32E83A0BAD7E47FD5B5646B55E355EC8DC4A (void);
// 0x000002A1 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::Awake()
extern void AeroplaneAiControl_Awake_mBB1F332AC1DD045C2A966BA70D5953DF13A28856 (void);
// 0x000002A2 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::Reset()
extern void AeroplaneAiControl_Reset_m8AE5A7318E2DABB2F9FC199D1A63CEFF31DF624C (void);
// 0x000002A3 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::FixedUpdate()
extern void AeroplaneAiControl_FixedUpdate_mA4FB1E3A82B6BD908B6442AF1EC25BA912FD10B8 (void);
// 0x000002A4 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::SetTarget(UnityEngine.Transform)
extern void AeroplaneAiControl_SetTarget_m1D2251D711AF77634C31CD681767DDA4D69A2ABF (void);
// 0x000002A5 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAiControl::.ctor()
extern void AeroplaneAiControl__ctor_m0D633CFE318B1C993BFDB21B58B9063EEF083123 (void);
// 0x000002A6 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::Awake()
extern void AeroplaneAudio_Awake_mD1C3764DA8C85EE774B0498DA5E9780A661DD104 (void);
// 0x000002A7 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::Update()
extern void AeroplaneAudio_Update_mE0F1409B7B370A2C794F3B025C5693FDE22F8B76 (void);
// 0x000002A8 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio::.ctor()
extern void AeroplaneAudio__ctor_mA7BF192E0267AE2CCC0E12B940961C322706158D (void);
// 0x000002A9 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneAudio/AdvancedSetttings::.ctor()
extern void AdvancedSetttings__ctor_mFAB9E670E2A2A4F1C0A437146AA5295D52282D78 (void);
// 0x000002AA System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_Altitude()
extern void AeroplaneController_get_Altitude_m4E875DCD8BCB99EE7F07DA2E8762AFD46BE541FE (void);
// 0x000002AB System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_Altitude(System.Single)
extern void AeroplaneController_set_Altitude_mAE9A6518154CFD78E7CFF142CE90D962CD3227CB (void);
// 0x000002AC System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_Throttle()
extern void AeroplaneController_get_Throttle_m4AD61D14FF2EB832F32EF2201EEEB88D8C00112F (void);
// 0x000002AD System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_Throttle(System.Single)
extern void AeroplaneController_set_Throttle_m92D547C38B62C8BC0627EA481B518D9814471C51 (void);
// 0x000002AE System.Boolean UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_AirBrakes()
extern void AeroplaneController_get_AirBrakes_mA6A92442654786A3A81FC10F115E19388289B321 (void);
// 0x000002AF System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_AirBrakes(System.Boolean)
extern void AeroplaneController_set_AirBrakes_m219D70D6F0149377B82BFD9C5EBC04F704F69E5B (void);
// 0x000002B0 System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_ForwardSpeed()
extern void AeroplaneController_get_ForwardSpeed_m24FBA8E719F9947C59C04A99A82F023BB880AA56 (void);
// 0x000002B1 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_ForwardSpeed(System.Single)
extern void AeroplaneController_set_ForwardSpeed_mA83C4E387E8F7BE0EB0513469189C1EAA7B15488 (void);
// 0x000002B2 System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_EnginePower()
extern void AeroplaneController_get_EnginePower_m6820B0A8A0667DF3F875DD6B946CAFA90D49886F (void);
// 0x000002B3 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_EnginePower(System.Single)
extern void AeroplaneController_set_EnginePower_mC367A1178518B9BD614703C790ED9C3E905C6C8B (void);
// 0x000002B4 System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_MaxEnginePower()
extern void AeroplaneController_get_MaxEnginePower_m7C993DEBFB6C0BDC12585CC7EEFEF54482841A04 (void);
// 0x000002B5 System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_RollAngle()
extern void AeroplaneController_get_RollAngle_m0EE0E1FF7C938837030E55880D5C1C00672952FC (void);
// 0x000002B6 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_RollAngle(System.Single)
extern void AeroplaneController_set_RollAngle_m194392FEB7B4232ECC7668DC939835A964FA66F9 (void);
// 0x000002B7 System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_PitchAngle()
extern void AeroplaneController_get_PitchAngle_m256401C8A133DF9FECD7F0ED7039C402C54CF7DA (void);
// 0x000002B8 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_PitchAngle(System.Single)
extern void AeroplaneController_set_PitchAngle_m6B3800033AF9875C9C281698484D2E231FCBB22F (void);
// 0x000002B9 System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_RollInput()
extern void AeroplaneController_get_RollInput_m0C5664B7B308AA31B69423E3D36DAB1A3281BAD7 (void);
// 0x000002BA System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_RollInput(System.Single)
extern void AeroplaneController_set_RollInput_m4BB6E33F4D4A5E9625B7DCE2630A83A3A41B654F (void);
// 0x000002BB System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_PitchInput()
extern void AeroplaneController_get_PitchInput_mC727744982F5F76244E92A6D6E47FCCC08426112 (void);
// 0x000002BC System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_PitchInput(System.Single)
extern void AeroplaneController_set_PitchInput_mF42D986AC4A0106FB23CC615FBB0FC23373E3338 (void);
// 0x000002BD System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_YawInput()
extern void AeroplaneController_get_YawInput_m592983EDFA03B03DDBBA4EE77907AA42E32054FE (void);
// 0x000002BE System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_YawInput(System.Single)
extern void AeroplaneController_set_YawInput_m69C88A4428AFB957949221A11B65A31FEC45651E (void);
// 0x000002BF System.Single UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::get_ThrottleInput()
extern void AeroplaneController_get_ThrottleInput_m59851ED24E7C475C0C70D4864F6FF459316F0D07 (void);
// 0x000002C0 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::set_ThrottleInput(System.Single)
extern void AeroplaneController_set_ThrottleInput_m2743123CE0B04B4916503B63E69910CE7744095F (void);
// 0x000002C1 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::Start()
extern void AeroplaneController_Start_m7FBCD8FBF0BCC1462B1A8BC4E2B5FF8D6AE5EE4B (void);
// 0x000002C2 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::Move(System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern void AeroplaneController_Move_m50739E5FDF5DC4DC9CFA0F433DBA454D00895F1D (void);
// 0x000002C3 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::ClampInputs()
extern void AeroplaneController_ClampInputs_m11BFF054FCFFD465F2092EEEC1569089D32ED574 (void);
// 0x000002C4 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::CalculateRollAndPitchAngles()
extern void AeroplaneController_CalculateRollAndPitchAngles_m256DFBCD062BE41329387558298C11E5120E1CEA (void);
// 0x000002C5 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::AutoLevel()
extern void AeroplaneController_AutoLevel_m769E68621FF95E4F36EF22B21C1D5534B0B64647 (void);
// 0x000002C6 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::CalculateForwardSpeed()
extern void AeroplaneController_CalculateForwardSpeed_mEDE489FF22663E14B1D0EBD4A8C711AA82430ABC (void);
// 0x000002C7 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::ControlThrottle()
extern void AeroplaneController_ControlThrottle_m23FD0CBDDCE899A8C33DD36D20E530F29ABF5692 (void);
// 0x000002C8 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::CalculateDrag()
extern void AeroplaneController_CalculateDrag_m5F4B70479BD915D297D6F251AD8B0B08B9516914 (void);
// 0x000002C9 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::CaluclateAerodynamicEffect()
extern void AeroplaneController_CaluclateAerodynamicEffect_m06EB3AD0D5A5A7B3D5C160DA498B0813561E4BB8 (void);
// 0x000002CA System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::CalculateLinearForces()
extern void AeroplaneController_CalculateLinearForces_m16697D299B525FE4771CA9EA1D8E768D5BF2EB7D (void);
// 0x000002CB System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::CalculateTorque()
extern void AeroplaneController_CalculateTorque_m83F88301C503C2A4EFE346086E8580C4231D1490 (void);
// 0x000002CC System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::CalculateAltitude()
extern void AeroplaneController_CalculateAltitude_m4E655414BAA1B4E1B556DF0D0ED88C85A945E90D (void);
// 0x000002CD System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::Immobilize()
extern void AeroplaneController_Immobilize_m2E23F22E0724E48E28928285DBB419318A5B4B91 (void);
// 0x000002CE System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::Reset()
extern void AeroplaneController_Reset_m6AC87A785943BEA306E592215BD489DE80E4510A (void);
// 0x000002CF System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController::.ctor()
extern void AeroplaneController__ctor_m1FC615E68A962649FEC23298121027B4E1CE3349 (void);
// 0x000002D0 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator::Start()
extern void AeroplaneControlSurfaceAnimator_Start_mF59FE734FA30A77F3252848150CFEF108128207E (void);
// 0x000002D1 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator::Update()
extern void AeroplaneControlSurfaceAnimator_Update_m226B44E84AB643FD36508C6DD9B9C6C9CB0B0B10 (void);
// 0x000002D2 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator::RotateSurface(UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator/ControlSurface,UnityEngine.Quaternion)
extern void AeroplaneControlSurfaceAnimator_RotateSurface_m1C533431497586B6C6B9604556B350E8117C1D8D (void);
// 0x000002D3 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator::.ctor()
extern void AeroplaneControlSurfaceAnimator__ctor_m31502B7EB27FE87B5ED3CF500BCDDECF6EB52131 (void);
// 0x000002D4 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneControlSurfaceAnimator/ControlSurface::.ctor()
extern void ControlSurface__ctor_m8432698A6B9941EC562E2F3C7D5D8067CFA79076 (void);
// 0x000002D5 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::Awake()
extern void AeroplanePropellerAnimator_Awake_mE329F441D5138ACC3CC9B5AB838851F4C5C2FCC4 (void);
// 0x000002D6 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::Update()
extern void AeroplanePropellerAnimator_Update_m0ED917491FE41855D1B4205B49E73BC66D6E9B96 (void);
// 0x000002D7 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplanePropellerAnimator::.ctor()
extern void AeroplanePropellerAnimator__ctor_mEE35C8DC327F6D127671F6236B3470980764E3CB (void);
// 0x000002D8 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl2Axis::Awake()
extern void AeroplaneUserControl2Axis_Awake_mE6284378E8163C26FDB122A2721ED98CF2EE5921 (void);
// 0x000002D9 System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl2Axis::FixedUpdate()
extern void AeroplaneUserControl2Axis_FixedUpdate_m85869B57F1E6125C4279263942F09785F43B5888 (void);
// 0x000002DA System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl2Axis::AdjustInputForMobileControls(System.Single&,System.Single&,System.Single&)
extern void AeroplaneUserControl2Axis_AdjustInputForMobileControls_mF8743B3626967E1836696860024F755B0439234F (void);
// 0x000002DB System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl2Axis::.ctor()
extern void AeroplaneUserControl2Axis__ctor_mEF5248CD690ABADA04E8DD4FE8498AF34927C8F1 (void);
// 0x000002DC System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl4Axis::Awake()
extern void AeroplaneUserControl4Axis_Awake_m2E197C2C29E1F56E6C25FB657AF0313D2C16ED31 (void);
// 0x000002DD System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl4Axis::FixedUpdate()
extern void AeroplaneUserControl4Axis_FixedUpdate_m59F01670B6E8FF68815980D7F5B376C3DC3CF85A (void);
// 0x000002DE System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl4Axis::AdjustInputForMobileControls(System.Single&,System.Single&,System.Single&)
extern void AeroplaneUserControl4Axis_AdjustInputForMobileControls_m7A8B54330AC2872BEC5CA02F6714429CF557653A (void);
// 0x000002DF System.Void UnityStandardAssets.Vehicles.Aeroplane.AeroplaneUserControl4Axis::.ctor()
extern void AeroplaneUserControl4Axis__ctor_mC752A563940E50FF9E13976C5688F05291584B7F (void);
// 0x000002E0 System.Void UnityStandardAssets.Vehicles.Aeroplane.JetParticleEffect::Start()
extern void JetParticleEffect_Start_m2FB30A37B89344109B7E39F2FF2C3F35D71AF265 (void);
// 0x000002E1 System.Void UnityStandardAssets.Vehicles.Aeroplane.JetParticleEffect::Update()
extern void JetParticleEffect_Update_m149824E423722F077F054BE895B88FF8FAF93AD9 (void);
// 0x000002E2 UnityStandardAssets.Vehicles.Aeroplane.AeroplaneController UnityStandardAssets.Vehicles.Aeroplane.JetParticleEffect::FindAeroplaneParent()
extern void JetParticleEffect_FindAeroplaneParent_m7910F189D8D43BE33589A795685FCFEBF6832F16 (void);
// 0x000002E3 System.Void UnityStandardAssets.Vehicles.Aeroplane.JetParticleEffect::.ctor()
extern void JetParticleEffect__ctor_mF8E9E08946B553295EAF785029EE39A4B4A47676 (void);
// 0x000002E4 System.Void UnityStandardAssets.Vehicles.Aeroplane.LandingGear::Start()
extern void LandingGear_Start_m70C789B2041BD240C035E877C24B286DCFDA09E8 (void);
// 0x000002E5 System.Void UnityStandardAssets.Vehicles.Aeroplane.LandingGear::Update()
extern void LandingGear_Update_m73400D86CB6743CD87E9D40E42F0B875DB870AB6 (void);
// 0x000002E6 System.Void UnityStandardAssets.Vehicles.Aeroplane.LandingGear::.ctor()
extern void LandingGear__ctor_m8DFED9790FE9F6BF42F1391CCA7AE61FCEF10349 (void);
// 0x000002E7 System.Void UnityStandardAssets.Vehicles.Ball.Ball::Start()
extern void Ball_Start_m5D0B8C28F1754209F6D16469D336F4B1512B7A5C (void);
// 0x000002E8 System.Void UnityStandardAssets.Vehicles.Ball.Ball::Move(UnityEngine.Vector3,System.Boolean)
extern void Ball_Move_m8F0B2AEF76FB1CE02759A6821D547694DC6C206A (void);
// 0x000002E9 System.Void UnityStandardAssets.Vehicles.Ball.Ball::.ctor()
extern void Ball__ctor_mFF6055A2CCEA332012FA5096FD74AFCC3AA9DE83 (void);
// 0x000002EA System.Void UnityStandardAssets.Vehicles.Ball.BallUserControl::Awake()
extern void BallUserControl_Awake_m2E99883139DA507FD403B93D914012CD0E5A7469 (void);
// 0x000002EB System.Void UnityStandardAssets.Vehicles.Ball.BallUserControl::Update()
extern void BallUserControl_Update_m9914D43D57FCE7CF3D2005007BD40D494B402029 (void);
// 0x000002EC System.Void UnityStandardAssets.Vehicles.Ball.BallUserControl::FixedUpdate()
extern void BallUserControl_FixedUpdate_mF8BAAC8DEE572570B46E3427A0A439EBD763CC59 (void);
// 0x000002ED System.Void UnityStandardAssets.Vehicles.Ball.BallUserControl::.ctor()
extern void BallUserControl__ctor_m464B644240493E89135688A53AA2ABE78ED388D8 (void);
// 0x000002EE UnityEngine.AI.NavMeshAgent UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::get_agent()
extern void AICharacterControl_get_agent_mE9D2D96A9F9C131E51806B002B2EAF007C1D6F95 (void);
// 0x000002EF System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::set_agent(UnityEngine.AI.NavMeshAgent)
extern void AICharacterControl_set_agent_m578A2A67480A8EA83153AC7634411B77FA9CCC46 (void);
// 0x000002F0 UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::get_character()
extern void AICharacterControl_get_character_m428E6810E7895CA422D1F78CCC0D55062CD3EB32 (void);
// 0x000002F1 System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::set_character(UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter)
extern void AICharacterControl_set_character_m7500A5CBD3DBC30E9EC8E77F02094901A56F08DE (void);
// 0x000002F2 System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::Start()
extern void AICharacterControl_Start_m16F993FE523F1D911D1A7810F5D2E57F8CAFEEC2 (void);
// 0x000002F3 System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::Update()
extern void AICharacterControl_Update_m94A6E10CB8196AA80D5AA78048464D589DBC726F (void);
// 0x000002F4 System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::SetTarget(UnityEngine.Transform)
extern void AICharacterControl_SetTarget_m8BC1A01E10C8ED6CDB8CFA5B06F8A90D2923FF79 (void);
// 0x000002F5 System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::.ctor()
extern void AICharacterControl__ctor_m5329061559B50669BBD6DF1A0B509EEBB2E6F53A (void);
// 0x000002F6 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::Start()
extern void ThirdPersonCharacter_Start_m5AA32FD6C771EE3BB7AC492F35DEB17164389EEE (void);
// 0x000002F7 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::Move(UnityEngine.Vector3,System.Boolean,System.Boolean)
extern void ThirdPersonCharacter_Move_m14BD868DA889B216D21EBD4B65822D1301A8D817 (void);
// 0x000002F8 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::ScaleCapsuleForCrouching(System.Boolean)
extern void ThirdPersonCharacter_ScaleCapsuleForCrouching_m9ACB83ACE0F987E8F3D0793ADA140C4744798505 (void);
// 0x000002F9 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::PreventStandingInLowHeadroom()
extern void ThirdPersonCharacter_PreventStandingInLowHeadroom_m77123AD6DFC0726F46E3CAFF44DC7A918233BECF (void);
// 0x000002FA System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::UpdateAnimator(UnityEngine.Vector3)
extern void ThirdPersonCharacter_UpdateAnimator_m5D81E79A8CC7B4F7AB9CEBA64A6BFD1A100847D8 (void);
// 0x000002FB System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::HandleAirborneMovement()
extern void ThirdPersonCharacter_HandleAirborneMovement_m058EEAB7BFBFCC5C3B80227B8F3FE5A8797BEF46 (void);
// 0x000002FC System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::HandleGroundedMovement(System.Boolean,System.Boolean)
extern void ThirdPersonCharacter_HandleGroundedMovement_m1DCF7A4ED5FFC241FCCF8DC3174D7C31B110522C (void);
// 0x000002FD System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::ApplyExtraTurnRotation()
extern void ThirdPersonCharacter_ApplyExtraTurnRotation_m17BF71EAC445A20DB271B94C5B024C17F858677F (void);
// 0x000002FE System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::OnAnimatorMove()
extern void ThirdPersonCharacter_OnAnimatorMove_mD0430AE51DCE26AB275DEFFB377A4C76ACB14CEB (void);
// 0x000002FF System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::CheckGroundStatus()
extern void ThirdPersonCharacter_CheckGroundStatus_mE303C4E82C923B49B57CB218228945068A8A10C4 (void);
// 0x00000300 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::.ctor()
extern void ThirdPersonCharacter__ctor_m5C6F59AE87039781AC92B9C9743D3518D50987A3 (void);
// 0x00000301 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::Start()
extern void ThirdPersonUserControl_Start_m0A4BA00B12E8F11166BF653A6E9D5BE02CFBC3ED (void);
// 0x00000302 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::Update()
extern void ThirdPersonUserControl_Update_m3635F6E70F540A56F063280BFC1FE283E5CE5643 (void);
// 0x00000303 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::FixedUpdate()
extern void ThirdPersonUserControl_FixedUpdate_m5410D2090502C79F94D9B5C93B2C988F9861310B (void);
// 0x00000304 System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::.ctor()
extern void ThirdPersonUserControl__ctor_m88F867E11FE8AA21EB19765F4CBDA14E68DB751E (void);
// 0x00000305 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::Start()
extern void FirstPersonController_Start_mAA4A49EB87FBB7902C8EB60B2DBCB98560A86539 (void);
// 0x00000306 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::Update()
extern void FirstPersonController_Update_m7A06B61C940B9C029DEA5CAA52E071FEF1E4101F (void);
// 0x00000307 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::PlayLandingSound()
extern void FirstPersonController_PlayLandingSound_m1163F5994A1ECC6354A71D3B14463AA52BD6AD0C (void);
// 0x00000308 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::FixedUpdate()
extern void FirstPersonController_FixedUpdate_m885A83AC79736376ADA18D55A166A552B364EE39 (void);
// 0x00000309 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::PlayJumpSound()
extern void FirstPersonController_PlayJumpSound_m724953DFC405CE754883F265282A759DF54F0CD9 (void);
// 0x0000030A System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::ProgressStepCycle(System.Single)
extern void FirstPersonController_ProgressStepCycle_m4A0C50AA385E80A025CD791C8551A0A59428DFD1 (void);
// 0x0000030B System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::PlayFootStepAudio()
extern void FirstPersonController_PlayFootStepAudio_mEAFE8B9F81AC4CE7A91E3B2D7ADA146FBC2C38DD (void);
// 0x0000030C System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::UpdateCameraPosition(System.Single)
extern void FirstPersonController_UpdateCameraPosition_m45F7DDF70CB43963FB5A36BA11EF8ADF67CD127E (void);
// 0x0000030D System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::GetInput(System.Single&)
extern void FirstPersonController_GetInput_mF4AB933D87EDE6957A437045F1BFEA59C7E6A07A (void);
// 0x0000030E System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::RotateView()
extern void FirstPersonController_RotateView_mD729D6CB1C9C70DAFC522878C2655D6E22989319 (void);
// 0x0000030F System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern void FirstPersonController_OnControllerColliderHit_m61BE461A3EFCAFDA23EBD42CF46427753A1D5975 (void);
// 0x00000310 System.Void UnityStandardAssets.Characters.FirstPerson.FirstPersonController::.ctor()
extern void FirstPersonController__ctor_mC29C4274392FB92FA99B4CBD7CAE7EAD76E71D60 (void);
// 0x00000311 System.Void UnityStandardAssets.Characters.FirstPerson.HeadBob::Start()
extern void HeadBob_Start_m0461CAD7A2559ABA05871DC2637C917EE831637E (void);
// 0x00000312 System.Void UnityStandardAssets.Characters.FirstPerson.HeadBob::Update()
extern void HeadBob_Update_m39FFE350BAB4E1680603665E4F569E56E6BD72E6 (void);
// 0x00000313 System.Void UnityStandardAssets.Characters.FirstPerson.HeadBob::.ctor()
extern void HeadBob__ctor_m5660486EDE7ECED49229156E2E97C9FFE5F7991C (void);
// 0x00000314 System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::Init(UnityEngine.Transform,UnityEngine.Transform)
extern void MouseLook_Init_m65D7184A71FB03E7D8A91B5568B29B95A2966F81 (void);
// 0x00000315 System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::LookRotation(UnityEngine.Transform,UnityEngine.Transform)
extern void MouseLook_LookRotation_mA488F8CAAF8FF957AC2CDF5C6BC13C6AECE13FE4 (void);
// 0x00000316 System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::SetCursorLock(System.Boolean)
extern void MouseLook_SetCursorLock_m232686DFCD6A9ACD9A7A00A097985F7B219D5C09 (void);
// 0x00000317 System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::UpdateCursorLock()
extern void MouseLook_UpdateCursorLock_m2A544D023BF6BC47AD20026BC6BD2ECCAA3DFABB (void);
// 0x00000318 System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::InternalLockUpdate()
extern void MouseLook_InternalLockUpdate_mBF77944AF4DB12E517B3BF223C471750E7B7AA10 (void);
// 0x00000319 UnityEngine.Quaternion UnityStandardAssets.Characters.FirstPerson.MouseLook::ClampRotationAroundXAxis(UnityEngine.Quaternion)
extern void MouseLook_ClampRotationAroundXAxis_m300D0DC7B8F3C07914205392FC3E4953EF949049 (void);
// 0x0000031A System.Void UnityStandardAssets.Characters.FirstPerson.MouseLook::.ctor()
extern void MouseLook__ctor_m3A4849EBD9493C9A93CF072D4D8F89579B75CDCA (void);
// 0x0000031B UnityEngine.Vector3 UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::get_Velocity()
extern void RigidbodyFirstPersonController_get_Velocity_mB70B043E4FF2263A33E379FF712BACB02633C339 (void);
// 0x0000031C System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::get_Grounded()
extern void RigidbodyFirstPersonController_get_Grounded_m77BF29EDF29AC361B1DED301A83CC1A12110CA61 (void);
// 0x0000031D System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::get_Jumping()
extern void RigidbodyFirstPersonController_get_Jumping_mCEEF2A49DDD908B8331D63CDC79271BF18505F86 (void);
// 0x0000031E System.Boolean UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::get_Running()
extern void RigidbodyFirstPersonController_get_Running_mB5D2C7EE08B1425F737F83EB7BB1CEB1A4AA3DF9 (void);
// 0x0000031F System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::Start()
extern void RigidbodyFirstPersonController_Start_m007AC5C8C9C6A023F268E43B2547911DED6F41DE (void);
// 0x00000320 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::Update()
extern void RigidbodyFirstPersonController_Update_m9AE00B033CD92A95DF1D72AA04777C7243DCBF53 (void);
// 0x00000321 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::FixedUpdate()
extern void RigidbodyFirstPersonController_FixedUpdate_m2627ED832700FEED40B40D4199DA0EF301207D2D (void);
// 0x00000322 System.Single UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::SlopeMultiplier()
extern void RigidbodyFirstPersonController_SlopeMultiplier_mC48D2F329905D984EA3080CF0A8C6290B50327C9 (void);
// 0x00000323 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::StickToGroundHelper()
extern void RigidbodyFirstPersonController_StickToGroundHelper_mE9E633A49F5FE50DFCB6D2244FCBA5AB3D518048 (void);
// 0x00000324 UnityEngine.Vector2 UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::GetInput()
extern void RigidbodyFirstPersonController_GetInput_m679BF16A9B5F17B9258B11CE29222CCCD72F352F (void);
// 0x00000325 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::RotateView()
extern void RigidbodyFirstPersonController_RotateView_mD297E393DA2494AD94EB0F5DAAB150448EE0CEFF (void);
// 0x00000326 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::GroundCheck()
extern void RigidbodyFirstPersonController_GroundCheck_mB8795C247C54E60D56AAF6B923BA9A368273C147 (void);
// 0x00000327 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController::.ctor()
extern void RigidbodyFirstPersonController__ctor_m67B9AA5754CEF1E8145B0CB196DAD988126AFC0C (void);
// 0x00000328 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::UpdateDesiredTargetSpeed(UnityEngine.Vector2)
extern void MovementSettings_UpdateDesiredTargetSpeed_m6D418A9690124CFC9FA7DEA81FB2E521CFC73D12 (void);
// 0x00000329 System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/MovementSettings::.ctor()
extern void MovementSettings__ctor_m349160EB18090C7916E6210DC54F023AC6410527 (void);
// 0x0000032A System.Void UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController/AdvancedSettings::.ctor()
extern void AdvancedSettings__ctor_mC6C95C0E431E720B8F952FD872E22A801CEE4D46 (void);
// 0x0000032B System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::Start()
extern void AbstractTargetFollower_Start_m7F61268C7065276D04619317D1B609C06C37C398 (void);
// 0x0000032C System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FixedUpdate()
extern void AbstractTargetFollower_FixedUpdate_m9BBDE9FD938E5F3C379CC827B2D14B2BF1E197B3 (void);
// 0x0000032D System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::LateUpdate()
extern void AbstractTargetFollower_LateUpdate_m828E15AD2C38E4F0947474CB179D054E962FF6ED (void);
// 0x0000032E System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::ManualUpdate()
extern void AbstractTargetFollower_ManualUpdate_m001A4048A98BFF454F9E03BFB17478A02C4E80EA (void);
// 0x0000032F System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FollowTarget(System.Single)
// 0x00000330 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::FindAndTargetPlayer()
extern void AbstractTargetFollower_FindAndTargetPlayer_m692A40676591951502AB0C1B7904AE247E14B3C3 (void);
// 0x00000331 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::SetTarget(UnityEngine.Transform)
extern void AbstractTargetFollower_SetTarget_mCB80D61FD43B809825CF5685694F069163835A80 (void);
// 0x00000332 UnityEngine.Transform UnityStandardAssets.Cameras.AbstractTargetFollower::get_Target()
extern void AbstractTargetFollower_get_Target_m9D95FB6B7D394313A438474A9F3F2A2E69CFEF1B (void);
// 0x00000333 System.Void UnityStandardAssets.Cameras.AbstractTargetFollower::.ctor()
extern void AbstractTargetFollower__ctor_m9383202CC56D1418B9B62CDA9A85A00D4F7EB181 (void);
// 0x00000334 System.Void UnityStandardAssets.Cameras.AutoCam::FollowTarget(System.Single)
extern void AutoCam_FollowTarget_mAEE3C3DBFAF42EFBB5A903C2BD63D070B884BFF2 (void);
// 0x00000335 System.Void UnityStandardAssets.Cameras.AutoCam::.ctor()
extern void AutoCam__ctor_m9DD923DB2FE3DF7B76B1CB48D5D3FC86256C1103 (void);
// 0x00000336 System.Void UnityStandardAssets.Cameras.FreeLookCam::Awake()
extern void FreeLookCam_Awake_m14CCC793AE86B51FB3B5939DFF90E402BCA5BA6E (void);
// 0x00000337 System.Void UnityStandardAssets.Cameras.FreeLookCam::Update()
extern void FreeLookCam_Update_m52A2CF02E928E5FEB6CB7F080873258A0A0E510B (void);
// 0x00000338 System.Void UnityStandardAssets.Cameras.FreeLookCam::OnDisable()
extern void FreeLookCam_OnDisable_mCDF2848FDCEAE527591F1B4187145D6C89243C8C (void);
// 0x00000339 System.Void UnityStandardAssets.Cameras.FreeLookCam::FollowTarget(System.Single)
extern void FreeLookCam_FollowTarget_m6FBDD37A7C75C9E0B2CAB6081A20365A0DBCA8A3 (void);
// 0x0000033A System.Void UnityStandardAssets.Cameras.FreeLookCam::HandleRotationMovement()
extern void FreeLookCam_HandleRotationMovement_mB03D65D9D7D28863ADB3AC892B7F03882ABFD9CC (void);
// 0x0000033B System.Void UnityStandardAssets.Cameras.FreeLookCam::.ctor()
extern void FreeLookCam__ctor_mC068E3574DCE19C486D767F4B8E5D0DA17A74901 (void);
// 0x0000033C System.Void UnityStandardAssets.Cameras.HandHeldCam::FollowTarget(System.Single)
extern void HandHeldCam_FollowTarget_m8FA00A4BF32224C5A9EF0C7FBEA378ADD2F60A81 (void);
// 0x0000033D System.Void UnityStandardAssets.Cameras.HandHeldCam::.ctor()
extern void HandHeldCam__ctor_mFEBA00B6A4892C79C8C780E7FC290AB4BC3E28E1 (void);
// 0x0000033E System.Void UnityStandardAssets.Cameras.LookatTarget::Start()
extern void LookatTarget_Start_m6C0EA22CAA8DD3279AC54D149D8BE5751DC90820 (void);
// 0x0000033F System.Void UnityStandardAssets.Cameras.LookatTarget::FollowTarget(System.Single)
extern void LookatTarget_FollowTarget_m8E28D73BA8EFF01C3CF34147B8B30E483075BD34 (void);
// 0x00000340 System.Void UnityStandardAssets.Cameras.LookatTarget::.ctor()
extern void LookatTarget__ctor_m46B5A2FF61935C88A06F7D1CE8B734EFA77623C1 (void);
// 0x00000341 System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::Awake()
extern void PivotBasedCameraRig_Awake_m983D94AEF67006D5DB60ECF7E37A4A2E80C06627 (void);
// 0x00000342 System.Void UnityStandardAssets.Cameras.PivotBasedCameraRig::.ctor()
extern void PivotBasedCameraRig__ctor_m2E64852C2F38E2BFA5525713519BBEC14DF15C01 (void);
// 0x00000343 System.Boolean UnityStandardAssets.Cameras.ProtectCameraFromWallClip::get_protecting()
extern void ProtectCameraFromWallClip_get_protecting_m0D765A8440B7565261BD788EB73C1EE4EE3DAD65 (void);
// 0x00000344 System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::set_protecting(System.Boolean)
extern void ProtectCameraFromWallClip_set_protecting_m48C0228FFDBCF906769B116CD846C9EE0F7EDDE3 (void);
// 0x00000345 System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::Start()
extern void ProtectCameraFromWallClip_Start_m7B988A51AF1E1399BF7CF2F34436289BD2271260 (void);
// 0x00000346 System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::LateUpdate()
extern void ProtectCameraFromWallClip_LateUpdate_mDEA1282E559819EA00553332A82BEC9BF7393655 (void);
// 0x00000347 System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::.ctor()
extern void ProtectCameraFromWallClip__ctor_m475D40249900346AB58EA88AC94CAC77D21D3B0D (void);
// 0x00000348 System.Int32 UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer::Compare(System.Object,System.Object)
extern void RayHitComparer_Compare_m58E06CAB53C50F64CD9BC9C64050BAF4E5E9354D (void);
// 0x00000349 System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer::.ctor()
extern void RayHitComparer__ctor_mE0506654EA6FCFED553243206FF268F188737B00 (void);
// 0x0000034A System.Void UnityStandardAssets.Cameras.TargetFieldOfView::Start()
extern void TargetFieldOfView_Start_mFCD6A3AC71B2828B939F54C2700B2267CA674FD7 (void);
// 0x0000034B System.Void UnityStandardAssets.Cameras.TargetFieldOfView::FollowTarget(System.Single)
extern void TargetFieldOfView_FollowTarget_mE76F2BD563C4BE107F02AA643D1B44F2182AF839 (void);
// 0x0000034C System.Void UnityStandardAssets.Cameras.TargetFieldOfView::SetTarget(UnityEngine.Transform)
extern void TargetFieldOfView_SetTarget_m0A958EA19FD6A8DBE394C8C95A7B2857EE9C3F64 (void);
// 0x0000034D System.Single UnityStandardAssets.Cameras.TargetFieldOfView::MaxBoundsExtent(UnityEngine.Transform,System.Boolean)
extern void TargetFieldOfView_MaxBoundsExtent_mD5B03F520F5DDED1529DC43817C7159CEBA6A955 (void);
// 0x0000034E System.Void UnityStandardAssets.Cameras.TargetFieldOfView::.ctor()
extern void TargetFieldOfView__ctor_mE55C2793450450A863BE51AB81A1670184D9EE19 (void);
// 0x0000034F System.Void UnityStandardAssets._2D.Camera2DFollow::Start()
extern void Camera2DFollow_Start_mBBCB0756B72E6A35767B21551B149655C99B4710 (void);
// 0x00000350 System.Void UnityStandardAssets._2D.Camera2DFollow::Update()
extern void Camera2DFollow_Update_mFCEB81CAF13114E1AC1DAC417CC6F9207FF59523 (void);
// 0x00000351 System.Void UnityStandardAssets._2D.Camera2DFollow::.ctor()
extern void Camera2DFollow__ctor_m5D8B513FD9D7A833F4C42EE1F4C30452BF3C42C6 (void);
// 0x00000352 System.Void UnityStandardAssets._2D.CameraFollow::Awake()
extern void CameraFollow_Awake_mBBAF3F7EC4EB6661BC8175A30FCEBB56FD593874 (void);
// 0x00000353 System.Boolean UnityStandardAssets._2D.CameraFollow::CheckXMargin()
extern void CameraFollow_CheckXMargin_mBA18B08E43C19B547573C57A58196FFF05F88B0B (void);
// 0x00000354 System.Boolean UnityStandardAssets._2D.CameraFollow::CheckYMargin()
extern void CameraFollow_CheckYMargin_m1036E178E98FBF08B43DAA2CFD4D17E8A594B1C7 (void);
// 0x00000355 System.Void UnityStandardAssets._2D.CameraFollow::Update()
extern void CameraFollow_Update_m95F27CA16E2239544092698F286B099E82944FD2 (void);
// 0x00000356 System.Void UnityStandardAssets._2D.CameraFollow::TrackPlayer()
extern void CameraFollow_TrackPlayer_m4E783430A9FAF82ED6F5B14BD1A18D1681538097 (void);
// 0x00000357 System.Void UnityStandardAssets._2D.CameraFollow::.ctor()
extern void CameraFollow__ctor_m2AE326C2998FB64EF10070E8F968D10F8F93945F (void);
// 0x00000358 System.Void UnityStandardAssets._2D.Platformer2DUserControl::Awake()
extern void Platformer2DUserControl_Awake_mDB0746580C433A28419249C9A4B479F5047CB504 (void);
// 0x00000359 System.Void UnityStandardAssets._2D.Platformer2DUserControl::Update()
extern void Platformer2DUserControl_Update_mD6F1860515F415BE3049299A04F6733D57EFBEF7 (void);
// 0x0000035A System.Void UnityStandardAssets._2D.Platformer2DUserControl::FixedUpdate()
extern void Platformer2DUserControl_FixedUpdate_m0E625C864E17EDFDC887737570D1323FF42AE914 (void);
// 0x0000035B System.Void UnityStandardAssets._2D.Platformer2DUserControl::.ctor()
extern void Platformer2DUserControl__ctor_m867784A332298E7C83BC03F98D44497049D5223B (void);
// 0x0000035C System.Void UnityStandardAssets._2D.PlatformerCharacter2D::Awake()
extern void PlatformerCharacter2D_Awake_m51C97A56A28460A2AD20D4935A9CC2C3E4F8CE44 (void);
// 0x0000035D System.Void UnityStandardAssets._2D.PlatformerCharacter2D::FixedUpdate()
extern void PlatformerCharacter2D_FixedUpdate_mB4BCA3EAD27575C9465D6F33AE248676B6D3B1C7 (void);
// 0x0000035E System.Void UnityStandardAssets._2D.PlatformerCharacter2D::Move(System.Single,System.Boolean,System.Boolean)
extern void PlatformerCharacter2D_Move_m5C9E0B1A3FA19184A28BBC94FFD0DA09B3B66382 (void);
// 0x0000035F System.Void UnityStandardAssets._2D.PlatformerCharacter2D::Flip()
extern void PlatformerCharacter2D_Flip_m3BADEAB3C7723ED0447C289F89A14228226B6611 (void);
// 0x00000360 System.Void UnityStandardAssets._2D.PlatformerCharacter2D::.ctor()
extern void PlatformerCharacter2D__ctor_mEA1EE535C6C514E3F455E216E456BF1B74D01077 (void);
// 0x00000361 System.Void UnityStandardAssets._2D.Restarter::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Restarter_OnTriggerEnter2D_m088B16EF1BC6AF41BBE0ED226F107B807851567D (void);
// 0x00000362 System.Void UnityStandardAssets._2D.Restarter::.ctor()
extern void Restarter__ctor_mD6B984BB34344BFD9D5B41980F8AA8636F350CBF (void);
static Il2CppMethodPointer s_methodPointers[866] = 
{
	BuggyControl_SetWheelComponent_mDA0A83284216CCE4617B2D91DD51C06E7EAB3601,
	BuggyControl_Awake_mAE7DAACA6D6C79DC05805C4988BD175436BE3D26,
	BuggyControl_ShiftUp_m240F4182487427C6C3C33747E92C0E44D1C198B9,
	BuggyControl_ShiftDown_m7DC7E2F78E665309B651A1A0F9E870BAA6FFB45A,
	BuggyControl_OnCollisionEnter_mCD4AF2091BD7A4D1172B080E05EF2C9E986FB61C,
	BuggyControl_OnCollisionStay_m58AA1AF5AB6A982E7C76D5BC68D9AC35F885012B,
	BuggyControl_Update_m8CBD000D95FD28BC3FD1E23B05DE0F2D3792604F,
	BuggyControl_FixedUpdate_m16770D6FA4F9D034EEED8D06105B61B911177282,
	BuggyControl_OnDrawGizmos_m46348F0CBA6C49A1D19815E54DB8463CB4E458E6,
	BuggyControl__ctor_mABF5843B49AC233AE41CD869BAE07EEBF9E08B23,
	CarWheels__ctor_mF04AA3C3EFEE9244B28873F8F1FB7562B5F559BC,
	ConnectWheel__ctor_m0F2DA1599E913AF9FBBD61802CC985DD528C93E3,
	WheelSetting__ctor_mD57418DAD333AD86355F1477AC7066DCA9782141,
	CarLights__ctor_mDEC1742CF5441FF292B68204BCA657841E89EACB,
	CarSounds__ctor_m7430D2202D45FAC242C60334AF4C6FFA224F51CC,
	CarParticles__ctor_mABD18B780B67A45D2BE45BA08571AED8502817A2,
	CarSetting__ctor_mD5F53F3C1E2B46EE83FF9BE914151592A9C1E4A5,
	HitGround__ctor_mC47369434578A488EC2E930EDC6C1A095140E154,
	WheelComponent__ctor_m6D3A95D9A508D5DE54A7F3731AE063A381CBE83B,
	CarSwitch_CurrentCarActive_m40525385AACE666CDDD0C18AE74D6D91AC170664,
	CarSwitch__ctor_m4973A47FF6FA0C613D9CBD43123E6A46CDDDFEC4,
	CenterAxle_Update_mD07DE3AD001E22874E46BE16EFF7752B94434A96,
	CenterAxle__ctor_m41158E9FD04B238E60CDB36266B55466F1A666DB,
	GizmoObject_OnDrawGizmos_mBC844FEDCC85587B7B06FCB59EEE44FC864F1963,
	GizmoObject__ctor_m07BAEF9E583D5F0E577BF217DF81B0582EFD12F0,
	jiggleBone_Awake_m662C11C8DA10DEBF6F1C7C67B9FB0249AF7E33DD,
	jiggleBone_LateUpdate_mF887898486CDB2369AF9B299258107C46B477074,
	jiggleBone__ctor_m59C260C3E2CC17526EAAF6D712EBB7556486256D,
	PlayerAnimation_Awake_m1E43458A064235886005E0336844DC1685D7DC56,
	PlayerAnimation_Update_m24161E32CFBC1DB4A00E63C4B8AE2A2026189A03,
	PlayerAnimation_OnAnimatorIK_mCBB2EA7800B9E68238A9A9BA5DAF85A5BAEE984F,
	PlayerAnimation__ctor_m6712BFFFBAA031003247E1E6F34C1D9C197CF268,
	Spring_Update_mD01DE0062BB2AE428472EC2EC9A12603257DFC1F,
	Spring__ctor_m7A1FF518446BAECE38938EA7B4D164A1361A58BB,
	LookObjectsClass__ctor_mA091F52250F01D9C10AEF29031AD25D429691B69,
	VehicleCamera_CameraSwitch_m9ADE1B6BCCEDA23930E633A2C63FDCDDDA3EC4CB,
	VehicleCamera_CarAccelForward_m8665B42CDC415490A5DC774D284DE577CF85EED2,
	VehicleCamera_CarAccelBack_m894981900A1F5FA4C4DA306B87A43DEEA2D0D95A,
	VehicleCamera_CarSteer_m0F81EAE6C010C7C720CEDC069F4D1F1FA24A54D4,
	VehicleCamera_CarHandBrake_mC907AD072AECD8CADBE663D672AE6ACEEB1421E0,
	VehicleCamera_CarShift_m081ECED11B5E25F405D6BFD6AD46DE2D93B74B8E,
	VehicleCamera_RestCar_m47561C9E6E6E4D9359FC097207B507F3DB0AC12B,
	VehicleCamera_ShowCarUI_m4D3B944EDB92C232A96F73821C0AA22A7E05BB20,
	VehicleCamera_Start_m332BCBC9078F1C6CA381E4B6030918A0CD23D7D5,
	VehicleCamera_Update_m4627BECEAB50B368CDB946182B08AE37C19AF65B,
	VehicleCamera_AdjustLineOfSight_m2815276FF3579FF8C096CE68F3C6D0F82AC8F24E,
	VehicleCamera__ctor_m75F85D6FBEEBD517A9F2C35ABF77B4ABBE2E9BDE,
	CarUIClass__ctor_mCF84557822C2B5C36498EF1112214183054C095F,
	VehicleDamage_Start_m885F4656F023FEC9E0DB26162F9AEB75D34AFD1D,
	VehicleDamage_OnCollisionEnter_m453083B78AE2E273651A199CC9D85F8141664207,
	VehicleDamage_OnMeshForce_mC2FCBCBDEABC263FCA94CF61FF5C1E0A78A5EE5F,
	VehicleDamage_OnMeshForce_m445A59A2DBC0D45EEFE91F572A2D6CC5CFCD4602,
	VehicleDamage__ctor_m73BE37829922CB43402FCC56C05F3744C87EF188,
	Skidmarks_Start_m630AD543C6831297B563C7E0FCCF6E593B1E66E4,
	Skidmarks_Awake_mE824327E196F09A11614FF3BF8DF459D27101A52,
	Skidmarks_AddSkidMark_mA1475B4E1C10D429155C166434BF9D33B1B8C0A1,
	Skidmarks_LateUpdate_m243E9DE089D3D2FFEF63A8BDFD41CA21330B16FC,
	Skidmarks__ctor_mCF8400BC9314E7EAD88F56AC072EB921C0DAB2DA,
	markSection__ctor_m19F0CFB9502AC2E4060E3B4197173858845D9353,
	WheelSkidmarks_Start_mB965198AF4498114EFBEC136C7831205B2A81073,
	WheelSkidmarks_FixedUpdate_mCCF74A5BF34FFF9DC41AA2D95CB2929BEAADF065,
	WheelSkidmarks__ctor_m2E1705DF9124D9E9FDB49F2AB016F431A95FCDBB,
	AlphaButtonClickMask_Start_m188C2B5F6FE9CF85F7442D2E0DBADC5D8D302D1D,
	AlphaButtonClickMask_IsRaycastLocationValid_mE73AF8581DAC8324A8F30F357B4E6BDB2CC4AF7E,
	AlphaButtonClickMask__ctor_m5C7ADFC7BAC4BEB4564F0F89F107563C394969B7,
	EventSystemChecker_Awake_m8B954CCFC70EDE79CD9418D2D38DA27F91DEBAA1,
	EventSystemChecker__ctor_mE880C47D1714A5E4C2BF1F48ED7254DF1C758363,
	ForcedReset_Update_m976395EFFB7A0EC376736E1DBC6F80F50ECE4DD5,
	ForcedReset__ctor_mD8A0AEF74A204883C4A757AF8C2BA90B1AE737F9,
	MenuSceneLoader_Awake_m18B4E09AAFDC9578469D066C50A8AC0054AA34AC,
	MenuSceneLoader__ctor_m27FD45CA6C6C8B579D2FA4EEDABBA35F2C7EF6BC,
	PauseMenu_Awake_mB04BCBDA84AEC654D1976EC3DF61A0CF68D2C86C,
	PauseMenu_MenuOn_m8973DC956DD5235381EE4ACB4A182AA5BF0EF2EA,
	PauseMenu_MenuOff_mCD1AF41F916B0C02D2FD6F82377DBEAFF7C30720,
	PauseMenu_OnMenuStatusChange_mD1E8BD9B9CCB274A83FFF0FE2E06E6ABD0361B4A,
	PauseMenu__ctor_mA1A281F3359C234E5CF24FFEAC20C12C48D69018,
	SceneAndURLLoader_Awake_m8F276157A2A5FA943EF7918D6CCDB81273317E23,
	SceneAndURLLoader_SceneLoad_m2B09BD48F419F49A6BD461DBC7B2290EC8632B06,
	SceneAndURLLoader_LoadURL_m47E3E286E80F2D5B3E6A164C32F7E1B473532AE2,
	SceneAndURLLoader__ctor_m6DEE574FADF9E3E894594690CB2755F69D5D4BE5,
	CameraSwitch_OnEnable_mD422991EDD9D880928644EE1BC4E557EE644679C,
	CameraSwitch_NextCamera_m38AB4521C129032FA1DA5154E09D36D0FE2DB257,
	CameraSwitch__ctor_m550FCA9B0C24BBBC8BDBCAAAAA7BBF26312399FE,
	LevelReset_OnPointerClick_m9625C8235343CB1DE6B089AD6F2D5ACF738072A4,
	LevelReset__ctor_mFD2BB9AF1A9D7E795DDAAE2F33C1F0EB1FB31F07,
	ButtonOptions_PlayGame_m9E2117F45465D1044B34C49B84D9DE5074453F37,
	ButtonOptions_MainMenu_m1E7DC14B4795D0F1EA23C223DA9A0F9CB3DDC1D2,
	ButtonOptions_Credits_m570AA0A350E3D8A0F97436B472DAE6DB8344BA7D,
	ButtonOptions_Track01_mC6CA9D2DDF57C79D3119D31F21EB78EDFD12C0F1,
	ButtonOptions_Track02_m54284E0535331EF4C2396EC1E7360136604044A9,
	ButtonOptions_doExitGame_m0AE138AAF496350CBB82A76C556AD44E3F6768E9,
	ButtonOptions__ctor_m6A4E167BAF103B463212364819DB42488E1302CE,
	CameraChange_Update_m567F67F1C326A51C016C6BD49D152D6A4BEE67A1,
	CameraChange_ModeChange_m5D7CB5AA2649A4513C1C4C53F200DA833147DE68,
	CameraChange__ctor_m5A248C7658A54C780F720D1C983B98CEB7993672,
	U3CModeChangeU3Ed__5__ctor_m6E4DFE6D6853FF215E5C6950314239FBDE5A9065,
	U3CModeChangeU3Ed__5_System_IDisposable_Dispose_mAD92FCC379FAA0BF7F2708FCE020CBD7E3A6A8F6,
	U3CModeChangeU3Ed__5_MoveNext_m26FDA14921720021888B348B318D82231A0D3BB1,
	U3CModeChangeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m394AC68A54E9D1CCE5BA816174FDA7410AD52313,
	U3CModeChangeU3Ed__5_System_Collections_IEnumerator_Reset_m4ECE5228864B4992CC7AEFAA5CA56A89B6C1459B,
	U3CModeChangeU3Ed__5_System_Collections_IEnumerator_get_Current_m8B5E062560B10F8ED51785E84B3E2A6617216ACE,
	CarChoice_Start_m8035683201A655B956CC208F058EDFF4D7A0B761,
	CarChoice__ctor_mE3966E245F06271FC40C705608FC20509C448DE9,
	CarControlActive_Start_mF58667AC5DCD6308C17681F2446D1DC8ABBDA367,
	CarControlActive__ctor_mB1E1B81049371F652098602724C161B9BDE70DA0,
	Countdown_Start_m4EDEE1C587E3BA10C738C384E38BF5E85A4264E9,
	Countdown_CountStart_mA539D770497099A23409A280BD60ED80A0A0CFE9,
	Countdown__ctor_m046FB7F09BC45D1C33491F89DD01A93BBD31880E,
	U3CCountStartU3Ed__4__ctor_mBC03B9E6034DF120A0D2089986FD13B398B4D66B,
	U3CCountStartU3Ed__4_System_IDisposable_Dispose_mA70409A559F80987047BC73CF9553FFC296241D5,
	U3CCountStartU3Ed__4_MoveNext_m1E901961316164DB4C1470BA2116E670C0664442,
	U3CCountStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m421DF04ADD03CBDEE842CCE13AE941A4D4863394,
	U3CCountStartU3Ed__4_System_Collections_IEnumerator_Reset_m2ABF112DD1B76A21E8AE4F5F3AC281E80A7018C2,
	U3CCountStartU3Ed__4_System_Collections_IEnumerator_get_Current_m6DA6DC7604AE6FE8D637303A5EF00F99A0263E91,
	DreamCar01Track_Update_mBF4AD15CE71EC75EA57B02C6337F6DFBC94EE6D7,
	DreamCar01Track_OnTriggerEnter_mB246B8F2618C49FEC8DE1E13B956AB06F1877DCC,
	DreamCar01Track__ctor_m06581B38AC1C1A0B15E0C591CF2134E8FD10D4BD,
	U3COnTriggerEnterU3Ed__50__ctor_mE0FA7F7442227C1E446BB3EEF073CC9515FF105F,
	U3COnTriggerEnterU3Ed__50_System_IDisposable_Dispose_m6A998999BEA1A73AE267C12FEE2C3376DFCD216B,
	U3COnTriggerEnterU3Ed__50_MoveNext_mA0F38BEB2EAA6A1031CA7D81BE0B53D73DA01B73,
	U3COnTriggerEnterU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m113773DFC1D04FD4C23DB4ACBBDAB3E52D8C7F30,
	U3COnTriggerEnterU3Ed__50_System_Collections_IEnumerator_Reset_m0C5FC16BB46437E9E92FB39F12E3F3E6F947439E,
	U3COnTriggerEnterU3Ed__50_System_Collections_IEnumerator_get_Current_mD9B90C4BD35D717910E6CEF60B8ACBCF05B9FC36,
	DreamCar01Track1_Update_m7522D26B7D56DE06D1EB3CE4CA473C5FE815BCF4,
	DreamCar01Track1_OnTriggerEnter_m01F3423EB16DD02CED40F66C74B46758FEFED345,
	DreamCar01Track1__ctor_mCC690053D7EAB18218F875A21060ACE6BC940303,
	U3COnTriggerEnterU3Ed__23__ctor_m804AF5DFF01B58EA0EFB4514515F15749E5C0C5A,
	U3COnTriggerEnterU3Ed__23_System_IDisposable_Dispose_mAC2B1401586A46A40ADF2FE216D21512EFBBA0B4,
	U3COnTriggerEnterU3Ed__23_MoveNext_mC66CCC043F066FD71023B0E4D2D3775DFC702E62,
	U3COnTriggerEnterU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB147CCADDC2C1613A7428B27D8369D3D49EECCE7,
	U3COnTriggerEnterU3Ed__23_System_Collections_IEnumerator_Reset_m640AB20824CF86EFEA4D886AB2B65B89F7753015,
	U3COnTriggerEnterU3Ed__23_System_Collections_IEnumerator_get_Current_mD5B525E1142F6B2BC3C1C2F75F013EFBCE4707D5,
	FinishRotate_Update_m86F2BA51E6B60C5B088534869692C1297A5A8ED7,
	FinishRotate__ctor_m8A5CB130C0B9BDF0303DF35D781470C4C2D1952A,
	GlobalCar_RedCar_mF10F4D8931DC80180624BD3078CAB726B49F7994,
	GlobalCar_BlueCar_m0602B4A1D0BDC85DDCF45467893E3196FD9FC0C3,
	GlobalCar_BlackCar_mAF3E04BDFC60BFCFD5EDFE880F91F08F44FDC5ED,
	GlobalCar__ctor_m664EA38FE0E52A0004BFD547136C3A595D2F86EA,
	GlobalCash_Start_m223FE4563DF64084730E608E1DBF48FBCCD95A3A,
	GlobalCash_Update_mDFC1B00A38360D004EEAB8E7F8E5F89C586DCFA1,
	GlobalCash__ctor_mF756857254CA62235F81B0C8283E2D264DE9A1E1,
	HalfPointTrigger_OnTriggerEnter_mEE7997172F17F5BE5F3F2A33680159F40A5DADCA,
	HalfPointTrigger__ctor_m289628F9F3B74A159F89173BE51667FD831F82AD,
	LapComplete_Update_mD0BA5723842D689D1EA28E78B7A0EEC2BB287B4D,
	LapComplete_OnTriggerEnter_m1245488A53240B7DCB77721394ECEDAEAD202122,
	LapComplete__ctor_mA9DFE466EAE019368BCBB632AD40A3280AD728A5,
	LapTimeManager_Update_mADEBD26A4E32D6031F798159F50D046A088B9282,
	LapTimeManager__ctor_mA103382E9B5294A10871BA31021ADE11AAC4FBA6,
	MenuAppear_StartMenu_mBE11ED4AFF8BB87A6F2C37AAFBC15FD684C6F3C1,
	MenuAppear__ctor_m18BF204B3C2D0D457DEF532D34EACC572C51D45D,
	ModeScore_Start_m79E26F0F857997770A73F3B64923B22640FF78FD,
	ModeScore_Update_mBD0657CD1243F2707BA4E4528AAE8D12C16F6D36,
	ModeScore__ctor_m3CE433FC1A86A5FA7EC8FF01264938D61A65600E,
	ModeSelect_ScoreMode_m4A1DD9A33830BC7E1DD6CB5E44E63E790B018C61,
	ModeSelect_TheRaceMode_m5E74F2931306244FC6936AB67EAB688599591BF3,
	ModeSelect__ctor_m33C637987E5393112D599F9D614D1CB331B07359,
	PauseMenuActive_Update_m042E804B41D02BE7A73497660C170207CD3FB348,
	PauseMenuActive__ctor_m3E75C1597BCE42306C5C4A27C93DF4C126446CF9,
	PauseMenuActive__cctor_m7260119B8F740CB82BA903DB8EAA1764F0004CFC,
	PauseMenuActive_U3CUpdateU3Eg__ResumeU7C3_0_mB7B4AEEE0C5A1331E8EBE282CDEEE3432FE59A97,
	PauseMenuActive_U3CUpdateU3Eg__PauseU7C3_1_m61EC8A76C741685B429F07D32CA63AF865401E90,
	PosDown_OnTriggerExit_m56AA94835B65DA88BDF2105650F12C95B4CBCBF6,
	PosDown__ctor_mA58B701B2A150E4B0EF2E3E666298B2C9BC591A9,
	PosUp_OnTriggerExit_m8259C49429DE65DAE2793A0E571D58BBCE2DE0AA,
	PosUp__ctor_mFB0251D735C6E83F7AAED20E1C088A3DADADF3B8,
	RaceFinish_OnTriggerEnter_m4272AEFA0B264542D7B7D43743A7D4E19326C907,
	RaceFinish__ctor_m7071F04C622AB8342E870EEC0CA48DC15A1CC4C8,
	BlueScore_OnTriggerEnter_mB1082E4076C4ADA0E177D172FC6619D95BDBB39D,
	BlueScore__ctor_mF13E7C205668C56441018ACF3B4C677D13D0B532,
	RedScore_OnTriggerEnter_mFD2D4F52DB2195CA7FDC6D37716262D99A3DF423,
	RedScore__ctor_m73554B9DA1FA6ED37E35DF1BCB3AB3DCF42BBC90,
	YellowScore_OnTriggerEnter_m87DC1C27BBEFA577022F271A9ED753391BF41B36,
	YellowScore__ctor_m175883E3BAB70427A2DD0A6728582EE88DE62A42,
	Soundmanager_Start_m975C31F36795B907784A37DB9BCF92310274B474,
	Soundmanager_ChangeVolume_mAA3DFF1DC7601A9FBCB0795AD6F4E31F443B1F56,
	Soundmanager_load_m59449B9A84CEC2288275A035E610929DEFF14A1B,
	Soundmanager_save_m2700B41CBB75A7F5A748FC19FCA7C1126159A615,
	Soundmanager__ctor_m2D0E3F827FD370006285DC36B29EA59520B4097F,
	SplashToMenu_Start_m2D4A5CC4F2C41963AF787D9A90B244A1BE6250CF,
	SplashToMenu_ToMenu_m4340FFAE73ED422E679C79F9857297CB8BD805B0,
	SplashToMenu__ctor_mD224BF4E3A7789F4CD188BE40FD3FB692FE8E55D,
	U3CToMenuU3Ed__1__ctor_mF784F00797E38272788DB97F4C0DE2D85FDDB9C4,
	U3CToMenuU3Ed__1_System_IDisposable_Dispose_m36920930640368C665D1DB58C61D18DD460A6759,
	U3CToMenuU3Ed__1_MoveNext_m4D147FAE80B0DD2AED1D8E6831851B612FF3299F,
	U3CToMenuU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01474B7BBB17E4E9EB5E8F7AFCAF9AEC603E3B61,
	U3CToMenuU3Ed__1_System_Collections_IEnumerator_Reset_m4E63D4569018C6668C59A91F7521E4770D078437,
	U3CToMenuU3Ed__1_System_Collections_IEnumerator_get_Current_m9A0139268073C9EEF302E9E29AD8624051550F30,
	StableCam_Update_mDC52917C7B3E8FC43E52A361D18811B67262A85B,
	StableCam__ctor_m6F76A729FB16BF7C8F047C3B7F16AEC3FBA01E33,
	Unlockables_Update_m1C3696EB6FC0648B8EA409FEDAB7B98AB1779840,
	Unlockables_BlackUnlock_m39A0A801AC6058FB423701C9996B5C8725EB8D2B,
	Unlockables__ctor_m143B55A1F8CB79951EC2D7EBA10F9A99BB4DCC74,
	UnlockedObjects_Start_m7F1F585DB61381BEADACB5AAEA94C4CAC6DFC8B4,
	UnlockedObjects__ctor_mD74938E5D4CA5E3578C3232C27887301A2F7F228,
	ParticleSceneControls_Awake_m3268C5B5B81D4EE59C85C27AED5CA3956C284DA4,
	ParticleSceneControls_OnDisable_mEB5EAF7BC1928EBEFD83E651459327948255D811,
	ParticleSceneControls_Previous_mC2FE564898A6019238C5611E63B5EC3581CEAA63,
	ParticleSceneControls_Next_m4AB6C18CBE0148C4D354FDCAD5EE45D472577B8E,
	ParticleSceneControls_Update_m7B5894AE70D3733C901BFFF529195AC1467D632B,
	ParticleSceneControls_CheckForGuiCollision_mB8EDC5E34CE1B59986991852009A011D9514A8FB,
	ParticleSceneControls_Select_m123BA8121C7C5C454EF649A13C724F2622B4517A,
	ParticleSceneControls__ctor_mFE76B488C636F213B77193EDFF38F514A3186631,
	ParticleSceneControls__cctor_mBA868BA9BC2400AD1AB420B3D0AB42D863358338,
	DemoParticleSystem__ctor_mA046B73C934441755CA5D57991D7C453645AD8CE,
	DemoParticleSystemList__ctor_mAFBB16272EB2E695826211DD030553401DF6E83B,
	PlaceTargetWithMouse_Update_m28E68FE68FA0185C0D20A6001F4C262868348BE3,
	PlaceTargetWithMouse__ctor_m3582FA7DC3CF8198244F9EB014FE12AC97985FE7,
	SlowMoButton_Start_m8F334286C4BEE772EAA4842F431E01BB4929A04C,
	SlowMoButton_OnDestroy_mE8D51330ED641B12795E984B6F0ECCB00536DBDB,
	SlowMoButton_ChangeSpeed_m0C19576C039AA7BBBC478C87A6C964376BA58EAB,
	SlowMoButton__ctor_m993A85A43C7462445119B63636FD65A6F8C0F6BF,
	ActivateTrigger_DoActivateTrigger_m67A916D32496AEE42B99E067D4B53CD08D7F053F,
	ActivateTrigger_OnTriggerEnter_m1B6300E8F49B18FA7B558DC0E3406BD12F65E1D8,
	ActivateTrigger__ctor_m70F6E6A83BDB8D18767FB6E122A5019CE24ADC73,
	AutoMobileShaderSwitch_OnEnable_m64D0D68E657087D191F85C7FA99F3B879ECAC68B,
	AutoMobileShaderSwitch__ctor_m480014AF7A4225C84BE0C5BE3BC17ED6220C59F8,
	ReplacementDefinition__ctor_m4997D7740DF9303A83CAACA4BA3A12D7C333E3FC,
	ReplacementList__ctor_m8F28958E9FB9417946A0360B0AC3493DD0B79920,
	AutoMoveAndRotate_Start_m891D026688CE02C1C5FA72BD5AB9C656BB2A75A0,
	AutoMoveAndRotate_Update_m3D975BDCB0C3E8648C4BFF4BC1C051031732E607,
	AutoMoveAndRotate__ctor_m8F0CD8B9B52B5FCBDBABF694D75FBEFFBFEA5600,
	Vector3andSpace__ctor_m0BC6C4D6ADDEA323969A3F007106123F3A7B3962,
	CameraRefocus__ctor_m8A7B2D4B5D3EDD59E8BFC75BA42925A808F25027,
	CameraRefocus_ChangeCamera_m688C8AC7BF3512B6E0CC0A31E2824010C45E687D,
	CameraRefocus_ChangeParent_m60A783666226E66E99CBB8674D621CF0AEA05136,
	CameraRefocus_GetFocusPoint_m81CFE3F6A7D6B26E2AF0E37192953DC2EF6EA039,
	CameraRefocus_SetFocusPoint_mB02787B322CA5F26E2223C804457F9CC2798825F,
	CurveControlledBob_Setup_mBBE7165C9869012E40D574FE4CFC080B77E36E4E,
	CurveControlledBob_DoHeadBob_mB0983151149C57CFAEAF837EC93D6DE9BA07F09E,
	CurveControlledBob__ctor_m0EAE6C6C687D9144555AEFC5E5667CDCE1DAFE9C,
	DragRigidbody_Update_mF23D1DB84BC75A1F4BA7C1301F2E06AA6220CF71,
	DragRigidbody_DragObject_m8FE055A7BF316E6B6081ED6A000F4B33F6331383,
	DragRigidbody_FindCamera_mA0D3F9E16A4AF7F01F5C847118F95116FD1E3ABE,
	DragRigidbody__ctor_m2FF487F84D55FD4B112EDEC08495D85B707A2576,
	U3CDragObjectU3Ed__8__ctor_mE581CFDFA1541B920A9073BD407B17197AA22C4F,
	U3CDragObjectU3Ed__8_System_IDisposable_Dispose_mF26EE7E8993F6C8E42F3D472E25BA9AC3DDCB11A,
	U3CDragObjectU3Ed__8_MoveNext_mEF8CAA3CA274FA1284C3ED193F398E4C175A4AAA,
	U3CDragObjectU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1A8BEAEE65071C04D4D5109F01FC435E5E32598,
	U3CDragObjectU3Ed__8_System_Collections_IEnumerator_Reset_m3AD029462BAB8E669EDB80A7D1D156327A6034B7,
	U3CDragObjectU3Ed__8_System_Collections_IEnumerator_get_Current_m2FEB828E66BAB45FD0068CB1F443B43F10AE9457,
	DynamicShadowSettings_Start_mABF407E5176329CA9C819760364B55D8ECFFD54C,
	DynamicShadowSettings_Update_mF3128A86A6C42CF4B8AB6F2A3E7FCB2F81011A21,
	DynamicShadowSettings__ctor_mA1F95D2BC18371EA5A5A00BC6B0B2EEA44648D3B,
	FollowTarget_LateUpdate_m5A379F65CE462686067A208184C14618FBD47858,
	FollowTarget__ctor_m7695BB9D29256FF8E091780CD9FF2ACD5CD0BD4D,
	FOVKick_Setup_m81A58E005DA8B84808DD83EFFD50ADD4E7439DCB,
	FOVKick_CheckStatus_m79FB404B3FA31459D29B576F15C186AB0000DE70,
	FOVKick_ChangeCamera_m9BE2A7DCE3BEF7CBEB8CE5506C882331B38F256F,
	FOVKick_FOVKickUp_m35F515049C6AE4E4A29C5A83CD6DE9BCC822ACFF,
	FOVKick_FOVKickDown_mB964A3DD05D1E0337F3462DFF99864E2DD68B55D,
	FOVKick__ctor_m4F96E75BF9E5BBB1DD95122C4A0E1E6BAF4815C1,
	U3CFOVKickUpU3Ed__9__ctor_m1EF21E9B9DC9018D3B824D6DE00EB1E023012575,
	U3CFOVKickUpU3Ed__9_System_IDisposable_Dispose_m48ABBE7212815A0CCF476B5B04AF9CD539247549,
	U3CFOVKickUpU3Ed__9_MoveNext_mAA4C668C48D9643C4631119FCBA37FBEB6921AC9,
	U3CFOVKickUpU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99E4AABEA91319D0ED1D13D64B47B67105627012,
	U3CFOVKickUpU3Ed__9_System_Collections_IEnumerator_Reset_mE35D0195678B040DBCF2A0AFD6B4A37FFB327AE9,
	U3CFOVKickUpU3Ed__9_System_Collections_IEnumerator_get_Current_mFA0758DEBD49BA5EB77D7056D77BDA66E1194770,
	U3CFOVKickDownU3Ed__10__ctor_mD224B124C39C58E12A97C903679950D1DA896021,
	U3CFOVKickDownU3Ed__10_System_IDisposable_Dispose_m2E5B0C234FF88C0C4BAB260D0C220D0E4E416E8E,
	U3CFOVKickDownU3Ed__10_MoveNext_mBD3B663AFCDA2F1D51080784393CA95339E902AE,
	U3CFOVKickDownU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0AC4D4E3816A3A21DE1158B6CF3888DF6F8C605,
	U3CFOVKickDownU3Ed__10_System_Collections_IEnumerator_Reset_m585DC08D5BFA86A02F337DD9BA5352C16340E5CA,
	U3CFOVKickDownU3Ed__10_System_Collections_IEnumerator_get_Current_mFAC92D730D3E14E822DAFB863DDBEA52DB5F36C0,
	FPSCounter_Start_m6FD80CF9018A6D0D78201E830965399E32F694C5,
	FPSCounter_Update_m384CE1DC871CDE082E892B0FD1DEED2FBA73310A,
	FPSCounter__ctor_mA31248749E691ED7B04A6818FF2177EB00EBA9A5,
	LerpControlledBob_Offset_mB3F28F3F7F003505776686DF7416FF0283EB6A82,
	LerpControlledBob_DoBobCycle_mC05BB8C73BDBE83D173E5495474AA83D5696705E,
	LerpControlledBob__ctor_m5298F226DA9B6BD3747475A2B90FF8AC5057ACE6,
	U3CDoBobCycleU3Ed__4__ctor_m00E3AA853E0C70833839BE6CF4B684C4CD33ED1A,
	U3CDoBobCycleU3Ed__4_System_IDisposable_Dispose_m041C249DCAD39D852D5D4C53AE74743B147C56C0,
	U3CDoBobCycleU3Ed__4_MoveNext_m8A21DA48C9B7B222CE8571687E5555EFB7A95C25,
	U3CDoBobCycleU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D01238FD70F51A1194ECCDADB610C047D3A5792,
	U3CDoBobCycleU3Ed__4_System_Collections_IEnumerator_Reset_m113C61E01B625F04FCDCB54A3F408E3BBDA8010C,
	U3CDoBobCycleU3Ed__4_System_Collections_IEnumerator_get_Current_m16E4E9B6268494FD6D8AB1355AB783480603ACEC,
	ObjectResetter_Start_m11798912A3244DAB89010F14797215FF4C545FF2,
	ObjectResetter_DelayedReset_m5EC530F19321F0B1345039F13B2C2656E1C3D55F,
	ObjectResetter_ResetCoroutine_m69B6702ED74A205BE4C3BCD6CFFC103D95C21AB1,
	ObjectResetter__ctor_m3B23D97D1FB60A8B37A38639C6A91BB7B55AA66C,
	U3CResetCoroutineU3Ed__6__ctor_mEA1307100A79B77AF1A76D2FEB010AC0F398D1D8,
	U3CResetCoroutineU3Ed__6_System_IDisposable_Dispose_m3ED64FE685800880DBFC70D40D2E100D99300FFA,
	U3CResetCoroutineU3Ed__6_MoveNext_m5843875165FBE55EC671EB15BAD223C27EDE181C,
	U3CResetCoroutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5975A6D04132F784A7EA073D5C53A9718D70F21A,
	U3CResetCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_m149B3BE7E2CC36FE99172D68509239A467DC5D62,
	U3CResetCoroutineU3Ed__6_System_Collections_IEnumerator_get_Current_mDC979ABAC9337C102E679D1336D55DCE1CF725F6,
	ParticleSystemDestroyer_Start_m22D9C83B5A722EA2E1C253E2FF099438138BF6FD,
	ParticleSystemDestroyer_Stop_m4C91BE25E4FE2956C252BA8F5B403A35283BF657,
	ParticleSystemDestroyer__ctor_m93DD6572873B5FA0275846AEFD644028F02164CB,
	U3CStartU3Ed__4__ctor_m75C35393971810AEBA21CC290AAA4CC49495E255,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m30657DE1BBA6F01F5956D1E956F93FC6FB95E903,
	U3CStartU3Ed__4_MoveNext_m8F48483D8702D87182DDF114056160A982938E6F,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m518B488D36CA8433355D9D9E5C7FF56414206FF3,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m809AC76CDDA11F1B03907A7BF03939F0645577C0,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m710D2045FB130B25C4B8911F951D2FB0BA9050D8,
	PlatformSpecificContent_OnEnable_m3F1EDBFC1F3DBA76DEA47E4F8020D01BB7E81A87,
	PlatformSpecificContent_CheckEnableContent_mD9F7B70D48E634DC2C18C23E263F6BADDF5FD5EA,
	PlatformSpecificContent_EnableContent_m0803E25C4DF725D74A58755F9A1AA2F1BC7A9F0A,
	PlatformSpecificContent__ctor_m0655BFAD4DD4D971BE30511AFCCA29589DBE90E8,
	SimpleMouseRotator_Start_mC61B3E02E806C9E966F37029A33553D7643E5B15,
	SimpleMouseRotator_Update_m524B5CCCCFCBFCCB88FFA000C12D3B8DB1FA352A,
	SimpleMouseRotator__ctor_m3CAF12200E3884E468A94ECFAAFBD517191D9516,
	SmoothFollow_Start_m914CFA8E252AE8832270B584F77EEB18B5DF2612,
	SmoothFollow_LateUpdate_m3C60EB2C41AAE96878EE5E5DD0FE31BF5BCFB031,
	SmoothFollow__ctor_mEE4FE3D909F116297EEAC100215CD8A514E043CA,
	TimedObjectActivator_Awake_m35EEF5BA247D13FC5B1AB2C31681BB504EAF5B64,
	TimedObjectActivator_Activate_m8D54B8B84EACEBCB2C71E10C7214212B741A04B3,
	TimedObjectActivator_Deactivate_mC156085CA25A5A7488B9C96A85346FB8B7947AC7,
	TimedObjectActivator_ReloadLevel_mFAA2BADC84D8E9B7D931D67BDDB0960595BA8BB4,
	TimedObjectActivator__ctor_m5B33EE9A4CD720344F8DDBFD2B937E2A0E28CA3E,
	Entry__ctor_mBF1A0554BE0EE56D56A9C9EF90EF8DE8E0FB5BE4,
	Entries__ctor_m6AC502F787DE068AECB0337B82F3FFAF2862F93B,
	U3CActivateU3Ed__5__ctor_mFB8778165F139004330CC6A3743D6820FF6ED47B,
	U3CActivateU3Ed__5_System_IDisposable_Dispose_m2FA37B3EB3ED2C9DAC56B61BB56AC25F77D35AB8,
	U3CActivateU3Ed__5_MoveNext_mA4DD23258631024824E15FD18F742BA4D342C4EF,
	U3CActivateU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDA75E7324FFF19894C628190BD88A5C1009BCD5,
	U3CActivateU3Ed__5_System_Collections_IEnumerator_Reset_m38243A882F10F8A9411F0AB165CDD2551C38DDFB,
	U3CActivateU3Ed__5_System_Collections_IEnumerator_get_Current_mF2227106EC05A7DD3EA085EE1E8A2FDBEF16B754,
	U3CDeactivateU3Ed__6__ctor_m899EA0F4E6FE2879D91EEF688EC6985A160E2562,
	U3CDeactivateU3Ed__6_System_IDisposable_Dispose_m8DF5874B9F84C5378AC80474B09FA2AC9DD7858F,
	U3CDeactivateU3Ed__6_MoveNext_m53744A123319ED281728B3A1B0E745FAA9B13551,
	U3CDeactivateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9E5C199626DA1CD692DC06E3948C6111EEA3E3D8,
	U3CDeactivateU3Ed__6_System_Collections_IEnumerator_Reset_m34A6D814484AD9621AA15A3DB3AB7D304DE7A5F6,
	U3CDeactivateU3Ed__6_System_Collections_IEnumerator_get_Current_m92519078AE1085CE8B5B47F87484748720BB292D,
	U3CReloadLevelU3Ed__7__ctor_mB0C3DD1EBE01A4174A41DF73CA7959A978D3C584,
	U3CReloadLevelU3Ed__7_System_IDisposable_Dispose_mDE6472331A37952D2CDF6C319D01EC56938A8F2D,
	U3CReloadLevelU3Ed__7_MoveNext_m0313BB28993322AE9B1E48037D492586E2B909CB,
	U3CReloadLevelU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9A935D9379F9EB43E8A67E59C53388164E7F254C,
	U3CReloadLevelU3Ed__7_System_Collections_IEnumerator_Reset_m5694D3EF695046C6A1579159E27A8F1A35136558,
	U3CReloadLevelU3Ed__7_System_Collections_IEnumerator_get_Current_mD0E398142BA22111EC8BC841A4A5BB59A71569E8,
	TimedObjectDestructor_Awake_m2F595C9C46994AA3AA219F7C9583EF23F9F4539A,
	TimedObjectDestructor_DestroyNow_mDF4DEF5006FEE0D6A216A8AF1625B1005A842D4A,
	TimedObjectDestructor__ctor_m3B0E2CAF3EF3FC9E18F9DE8644F5A6375034DC25,
	WaypointCircuit_get_Length_m1236B0A843218603E40B10B60B52B2E7422FA3B7,
	WaypointCircuit_set_Length_m863FDE229863388113B92A833DB17384ACAA8E6A,
	WaypointCircuit_get_Waypoints_m1548CA404E245A6D76B2EEEA7C537E2438DD89DD,
	WaypointCircuit_Awake_mEE8F9D45FB160ED347F77E7B42158CD7E9D0DBE2,
	WaypointCircuit_GetRoutePoint_m3DE0695DAD41C9FE4AC263C618A7FD8B2C7EED81,
	WaypointCircuit_GetRoutePosition_m99C1CAF1FF0D344A0504D9E7B47EDB26D7C7D144,
	WaypointCircuit_CatmullRom_mCDE15554815F3FF68DDDC642A2B12C7464309379,
	WaypointCircuit_CachePositionsAndDistances_mC15C637B064C3DE5D72CB8AE9AD43AFC253EB421,
	WaypointCircuit_OnDrawGizmos_mA0E135CAA8FAF17A6AE5D528DFCEA8E4A2D40999,
	WaypointCircuit_OnDrawGizmosSelected_m238FB04A7650E4D0F423D8C806E783128E5FF824,
	WaypointCircuit_DrawGizmos_mE5F6AB8578D3A1DD584996DC3F705CE4D1223CFD,
	WaypointCircuit__ctor_m7C43D705AC2DAE19854F50E7B6AC70327077DD4A,
	WaypointList__ctor_m0E6BE7CDB93082199992A6248FF7C9A94CAB919B,
	RoutePoint__ctor_mCCDE64040449EDB283B910FC23935BBB32D1DFD5,
	WaypointProgressTracker_get_targetPoint_m4F823E334B64B9FCFD563CA474289AF2B14AFD32,
	WaypointProgressTracker_set_targetPoint_m2BED62ABBE7E2C8ECA459D32CBB3C4B541E51854,
	WaypointProgressTracker_get_speedPoint_mC52CC0812547C8644009766FB8A0CE33715ED974,
	WaypointProgressTracker_set_speedPoint_mC6891F0B93B772C22E068C5C31E155236D29F5DE,
	WaypointProgressTracker_get_progressPoint_mA2914CC30840A7EAAD5724DF6F4C067152CA966B,
	WaypointProgressTracker_set_progressPoint_m76B01B28B26FE69B242E058E4A2836D95575841D,
	WaypointProgressTracker_Start_m444232E34CBA3AF6548795B03D417A0BAFF56751,
	WaypointProgressTracker_Reset_m6A346E495442D196FF1666381F3351DF913B4883,
	WaypointProgressTracker_Update_m44D15F27C065AC7FFBC7EF6C7BBB1151DB1B6A2D,
	WaypointProgressTracker_OnDrawGizmos_mA178D2CE99E76CEC2DD2B165FD5A7185C2B1FBC5,
	WaypointProgressTracker__ctor_mF3FEE82CF0B834E9B5F602D74B3CD3E402100583,
	AfterburnerPhysicsForce_OnEnable_m84FEA393BE5D8FA30EE0331F65FF49E4DEA4FF3F,
	AfterburnerPhysicsForce_FixedUpdate_m7ABE56FC167BCCD86437BF62800511E982AB4571,
	AfterburnerPhysicsForce_OnDrawGizmosSelected_m2E46B2B0E13D90EB9AF516D01A6B10A5BC6FB650,
	AfterburnerPhysicsForce__ctor_mA80D179FF58F655FE895E1F8E43DE4F39835425A,
	ExplosionFireAndDebris_Start_mFE91656E5941C07D3F2A8E930B6EF3ACFA2767D9,
	ExplosionFireAndDebris_AddFire_m425D0F537ADF0CA2858A3B8D9E177626AF849E84,
	ExplosionFireAndDebris__ctor_m31B9795702F6DDB37F137A1600C60CF424CB9FB2,
	U3CStartU3Ed__4__ctor_m654866B338818A02D8A859C29170FA103BDF75D6,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mBE2A3DEE3789134E5B7D728FF7DAB247973A2EEA,
	U3CStartU3Ed__4_MoveNext_mFB613C537051E474BD2F6D1C4B2FB4740E676045,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73095AFAE33F9FDB419352040AAF4625E9B93C8C,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m1A46BFEF351A0D8D4460DEA6051CFDFA78200490,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m39E21679BAA2E949AA925B01357FE7D766728DFF,
	ExplosionPhysicsForce_Start_m370D01FE6D7CE5974143C20D8DB1D830AE688ACF,
	ExplosionPhysicsForce__ctor_mB037B7FBCEE28759C26A0E33D4A958E7A7D69804,
	U3CStartU3Ed__1__ctor_m460DD3BF0681F7F816234BA6422A293452FDA130,
	U3CStartU3Ed__1_System_IDisposable_Dispose_m01622861FE6176F42905E610F84E19204C2C4E6C,
	U3CStartU3Ed__1_MoveNext_m1082097E399D1772248C2916614291B1FFA52292,
	U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEB8947792CF4695EDBEB87F90688BB4ED6B95506,
	U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m887F7AE884FB48C61B7A5AED5FA58B2CAA37FC71,
	U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_m44F967B0D8CBD51E7B9EDF332B3BFFF256DACEDC,
	Explosive_Start_m27F89690A4785E39E59CE43CF1EBA7AAD72CC171,
	Explosive_OnCollisionEnter_m172BB7CE5D3A54BA968449E84FF2E18367942732,
	Explosive_Reset_m83612ECA692A68CECA4FBBFCEBECDA9DC13CF735,
	Explosive__ctor_m5AEF01810918AC8F11E30D8D0CC2D702EF2979D3,
	U3COnCollisionEnterU3Ed__8__ctor_mCB962F88A6E714F58F10A2E5DCB6CE3062FEE0DD,
	U3COnCollisionEnterU3Ed__8_System_IDisposable_Dispose_m5CF5AFFB8CBAC1F29C0FAAEA1330E851BE0B161F,
	U3COnCollisionEnterU3Ed__8_MoveNext_m8AFE7BA66A5C3A481494AE72C63C4BAADCEBC938,
	U3COnCollisionEnterU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m53A8C41319889D0F3CEE1954D51680A84CFDC174,
	U3COnCollisionEnterU3Ed__8_System_Collections_IEnumerator_Reset_m6B3902A22CF546D2EDC226E2D9A4D2DB0FDED820,
	U3COnCollisionEnterU3Ed__8_System_Collections_IEnumerator_get_Current_m21F500127088FB6C437C59CA912F2FD2FA6EF3AE,
	ExtinguishableParticleSystem_Start_mC29A8B945025785DE601895F3121153C17052DB1,
	ExtinguishableParticleSystem_Extinguish_m084314FFC2FED71A10A79CCD795639C357D33052,
	ExtinguishableParticleSystem__ctor_mFD1FACA848B804BF787F80645D11B30ED6651789,
	FireLight_Start_m0119386C1B5952F44A8BADE374CB05E636456570,
	FireLight_Update_mE20D336BAB5800F4E7EE05A3BA1E74F3C070C53C,
	FireLight_Extinguish_mB3B9BE9E97F4DDA2287B39283777FEA2AC25AEBC,
	FireLight__ctor_m4B61AB72ED30781C0121834171C63104F01223CE,
	Hose_Update_mFFFD4FC702953CB99599C87814E2604EDE737D32,
	Hose__ctor_m42EB7EDF65C0B24C0DD96D2F96A7D33C867574BA,
	ParticleSystemMultiplier_Start_m4655AADBCC9ACEACD4CEF85B17E905216FFBEF4A,
	ParticleSystemMultiplier__ctor_m4ACC644100AC4F6AA46DC1F33020A4449D869386,
	SmokeParticles_Start_mB77C48DABD154ED76C53334948115EC87066E1F4,
	SmokeParticles__ctor_m42152342972AE5EE9DFFE1E52C99450F470806A4,
	WaterHoseParticles_Start_m69FD918E507D29D4B392DC5E2E3928F0E46231CD,
	WaterHoseParticles_OnParticleCollision_m1BE62264E81B0C60C3229D53BD8EA4D950578D79,
	WaterHoseParticles__ctor_m632D0D0466141E640795FD0BC0E7B21BF6A324EF,
	WaterBasic_Update_m79AE15CDF47AA396823D10220EBA44C34CD6F569,
	WaterBasic__ctor_mF258EB45327E0929D2CB72B5266080C3F2E9CF4A,
	Displace_Awake_mE99DEB129DED3AF1991C408D98A52E342B4643A7,
	Displace_OnEnable_m952A8DDD7BF19B262520923CACCAE21927044BD8,
	Displace_OnDisable_m344E8D36AD672D92E465CD50F28E512A817B8B08,
	Displace__ctor_mA627066AA526C79EF3AA8D4FCDF70261CEA5CFA8,
	GerstnerDisplace__ctor_m718281CC56902E6BF82FAC236AB3043BFA0E9BFF,
	MeshContainer__ctor_mF1E5D7C018B2F9F756CECB8C1A0497406F833E3B,
	MeshContainer_Update_mA307DB7261AAD0254EBC0013D2DB7E30C12B94CA,
	PlanarReflection_Start_mF4B62F45289EDE1798379F46BB22090CFE446874,
	PlanarReflection_CreateReflectionCameraFor_m536231E7168798D1B53C3C69B4C3CE24F96C1734,
	PlanarReflection_SetStandardCameraParameter_m18039B2FBF79EDCC215634AA15719B070D665DF9,
	PlanarReflection_CreateTextureFor_m5CC4B46CAFD4C2B62F6B2E90A3DDC566F82B73AF,
	PlanarReflection_RenderHelpCameras_m8EC47193F6B3B38703D2FC5FC26A69115FB93241,
	PlanarReflection_LateUpdate_m67497232C28C05149AAA4EC85A3CCAB96D4F259C,
	PlanarReflection_WaterTileBeingRendered_mAD0310016829E5BB8BAE9F0313A4654C4C7039C1,
	PlanarReflection_OnEnable_m08B218FDAEA9D8E040927BEF9E011202783CC281,
	PlanarReflection_OnDisable_mE5DCEA557577A4B4CD3790829AF59BE81E4CAF36,
	PlanarReflection_RenderReflectionFor_m743FA341870F000B1626F691C9DED6E3714D5987,
	PlanarReflection_SaneCameraSettings_m73A5C561F9B670BC714B3A4B5B6722BF8AD30C1E,
	PlanarReflection_CalculateObliqueMatrix_mFA8D1838E4D40288C73A944485471F15A9EE13AB,
	PlanarReflection_CalculateReflectionMatrix_mC0F530AB64BBB6ADCAF28DD8E014D9388CEF9271,
	PlanarReflection_Sgn_m2C181CAD6E9DADF5B8F618D2D01649A826F8939A,
	PlanarReflection_CameraSpacePlane_mBBA681992D91FE0286F9694E11D44BFE640A698B,
	PlanarReflection__ctor_m8188E385A4C4CE7AB26873A83DB4377EA390A66F,
	SpecularLighting_Start_mFE455D641AAD002581EA31536A30C2F01B650CEE,
	SpecularLighting_Update_mC376C1CE2ECA4F4121AFE286F1F863EAF07720B1,
	SpecularLighting__ctor_mA0D46E411DF992BD42BCA24875516A8A5A2AAB3E,
	Water_OnWillRenderObject_m030DC7B6995FEC2690883CAA454A4A72C0F5BD54,
	Water_OnDisable_mBC5924D74718A4A22BA7AFA98296F9E0247F6FFD,
	Water_Update_m67CF13DF8C862994509BF70D888E84C4CEB8BD84,
	Water_UpdateCameraModes_m5A9B5A17F3C6714DC1E67305196CA9C477947011,
	Water_CreateWaterObjects_m759D0CAE5A10475C04C250DF826FC862BDF6A207,
	Water_GetWaterMode_mFF2310EBA5942F0B15B55B884F1923AF390F258D,
	Water_FindHardwareWaterSupport_mCC07F7400748514F5A89E92835812E047D763789,
	Water_CameraSpacePlane_mDE8B72357B143694C254E3411D4A36DB272F1CD2,
	Water_CalculateReflectionMatrix_mD27C10A49F7964539E94CBAC003DA65EA42003C0,
	Water__ctor_mF11768BB5C88F59E159BDDA9D4713A95BCDC7EDC,
	WaterBase_UpdateShader_mF024410B93ACDEEB86F2784367D4371789A8ECC6,
	WaterBase_WaterTileBeingRendered_m5AE3A512DBA0F49C5480DC7AC0832C69DA2007D6,
	WaterBase_Update_m97AB1D3EC79F89868388DEE04B9C12AC7C4F516C,
	WaterBase__ctor_mEF380720D189D0DF1DE0F40ACB847C00D12362CC,
	WaterTile_Start_m59B733629B793EA675F0934D0283148809B2D944,
	WaterTile_AcquireComponents_mFD2764B5DDB1CC33CA9AF55BB0D1D570EC92AD0B,
	WaterTile_OnWillRenderObject_m003657E9F9D7BBC9A618E204B3769E6F849C31E9,
	WaterTile__ctor_m5B94DD8133797CA9BF11522024360A0A1049F5E5,
	AxisTouchButton_OnEnable_mD8C2E1D724758527A2846C09CC7937830BA33AA9,
	AxisTouchButton_FindPairedButton_m639BD2D1A09BE33E8C82071921C4C14C57A68B39,
	AxisTouchButton_OnDisable_m5694ED6BFFCD06F121BBEA0FE552FF6B52D3078F,
	AxisTouchButton_OnPointerDown_m9AFBEB27DC7F3EA9E1EAE3BBF652EBC59BC66D4F,
	AxisTouchButton_OnPointerUp_mC38DDA9F687A46DE07977EBCFC2A9EF9242C248A,
	AxisTouchButton__ctor_mD1833F5DAAC806A70E989E4AC6A1C06BE04F2649,
	ButtonHandler_OnEnable_mD1D2CC3990A4EEDF5F393F0F5117E6634B2A5E9E,
	ButtonHandler_SetDownState_m7AB2F0D5E7AD859EBBBB3EDF459EF7279DCFA65E,
	ButtonHandler_SetUpState_m2BE684CD4BD7766903C83E3A2A78DDD38F941952,
	ButtonHandler_SetAxisPositiveState_mA25C8B1267D683CF9CBF988958190D84EBE9EC2A,
	ButtonHandler_SetAxisNeutralState_mBFD4413300A657D797F1035A3D772E0CD080C990,
	ButtonHandler_SetAxisNegativeState_mFD8E0D0F76D32DDEB582DA2FEA1F71A2B75C8B2A,
	ButtonHandler_Update_m9949BD0A3FBB84B766C797804FC347F5CBD52699,
	ButtonHandler__ctor_mD247ECA143AE9FA0DAA5D1D2E41ED6CB8D46BB08,
	CrossPlatformInputManager__cctor_m3DA11007BA9960F289314B3995540642637E6B96,
	CrossPlatformInputManager_SwitchActiveInputMethod_m5509783E9C56F86EBCEC9A51323FEAC33F456485,
	CrossPlatformInputManager_AxisExists_m4766470D2A3BCE44CFFE44BCAC0FDA0623B23372,
	CrossPlatformInputManager_ButtonExists_m046F62C8CA5B80CB8B5EC9830C430C12155FBB4F,
	CrossPlatformInputManager_RegisterVirtualAxis_mDA9D3A2181CD74F9096BF1C7807802DE353E4E1A,
	CrossPlatformInputManager_RegisterVirtualButton_m5E3A328FC6A6EC011EB337125A67468BDD86AAF4,
	CrossPlatformInputManager_UnRegisterVirtualAxis_m64342E501FD520FE5FE28A1AB5CF1B56334168F7,
	CrossPlatformInputManager_UnRegisterVirtualButton_m6EFB512B492D6FD6475DE35B931F27CE5B107741,
	CrossPlatformInputManager_VirtualAxisReference_m181E96561480060A9559B3749982548E347D634A,
	CrossPlatformInputManager_GetAxis_mB7AB6DA9693D497643353CF2B97A48C75F95007E,
	CrossPlatformInputManager_GetAxisRaw_m2B6A9EE32A0AE8ABD902BB5F6B74D0BB814C42E8,
	CrossPlatformInputManager_GetAxis_m9F2D914919AF9FC38087D229A6B6C1845045B5EE,
	CrossPlatformInputManager_GetButton_mF4FDF5B4FE1B224C4271E9B832CD8BA08AE73EFF,
	CrossPlatformInputManager_GetButtonDown_mCA7A96BB65979ADD959C6BFD8B6A3A1A7B1CDC26,
	CrossPlatformInputManager_GetButtonUp_mFB33294740857F8DE0BF7ACF110938B80C4DBDA5,
	CrossPlatformInputManager_SetButtonDown_mF5AB872C58C8C9A01435623E0C2679F68B768595,
	CrossPlatformInputManager_SetButtonUp_m6676EAE387ED7F03018429E9198113891378087B,
	CrossPlatformInputManager_SetAxisPositive_m4ACAB49EC1004299FDC5A2F547E9E5EC66F2F1DB,
	CrossPlatformInputManager_SetAxisNegative_m5A5DE4693430AB47774341EFB6B368EBD45A4CC1,
	CrossPlatformInputManager_SetAxisZero_m670B5FE17F3239D65FDC176C613309D87D86B1CC,
	CrossPlatformInputManager_SetAxis_mDB627063A19E94CF4C7BDD682EB7205D46F9619A,
	CrossPlatformInputManager_get_mousePosition_mE656A61832658E0E3DB01CC62D3998454087CAA8,
	CrossPlatformInputManager_SetVirtualMousePositionX_m963A27C3CD1CA33ED0054E029C046E6A492E4EF0,
	CrossPlatformInputManager_SetVirtualMousePositionY_m7EC2205F945F0DF0BAC67DC9907C8EDEFE97169E,
	CrossPlatformInputManager_SetVirtualMousePositionZ_mAA1C2749743422A7326CDC4C40BEABCD1C6C1DDF,
	VirtualAxis_get_name_mBBB8C139AABF771FC91A61B8444F835908F25A39,
	VirtualAxis_set_name_mE474E7B124D3E784ADF9D3532BC0A75F2684A2A2,
	VirtualAxis_get_matchWithInputManager_m531F00E4DD93A41FB0244FB71343FC9FB07DC4DF,
	VirtualAxis_set_matchWithInputManager_m61D9709975B67900CAD15101BF9C3AD57D2D88D0,
	VirtualAxis__ctor_m88EDC66F4BAB51D1BA2BDB502EBE995F08F42E64,
	VirtualAxis__ctor_m486C4129232F0F15151DA882C1C9F1DFDFE5D047,
	VirtualAxis_Remove_mCC5EF7DB8EC863AC7030AC9AB92F46A723BB7748,
	VirtualAxis_Update_m2A06394E13EA748D09D1506235BAB669636D9CBB,
	VirtualAxis_get_GetValue_mAC358FAC6484FAA00EB187E0583ECD4576794C44,
	VirtualAxis_get_GetValueRaw_mDCDC78FB43C16D8F65B9BE8799F0053DBB64007E,
	VirtualButton_get_name_m0B8D3FE4453224CE39D4316089F38D80399B449C,
	VirtualButton_set_name_mEF2365424C0A01C48B1D95066D01E5DC0B5B6DFA,
	VirtualButton_get_matchWithInputManager_m614D75F3386EB15F081A2F5D548B5743589BE939,
	VirtualButton_set_matchWithInputManager_m371812D456658C76DBC0128EC168A3091BBDF5C0,
	VirtualButton__ctor_m71595BAF216317FBF79F564F306D3A87F430EDE4,
	VirtualButton__ctor_m50F9D1236BD4CFA9C3136E0D9321DF9604D5C021,
	VirtualButton_Pressed_mAAC725DA03D80EC7275B0F0B82528E3C21670ADE,
	VirtualButton_Released_mD4D0FD8E203575FE98152A62BF6B16071E383F5C,
	VirtualButton_Remove_m040109DCD13EF3704399353ED4BC4AAB35539DF6,
	VirtualButton_get_GetButton_mD1EBB3A0B0A88B5CC0589120B42106447F9ED065,
	VirtualButton_get_GetButtonDown_m6C10A64F6C990B87627E8DDE6C1FFCFEBCD8FDB7,
	VirtualButton_get_GetButtonUp_mE7D1541E27B10531F1542C55781ED62EED0DC37F,
	InputAxisScrollbar_Update_m2E6071A5B2BA9BBE9F0D5AA4B7285053BD83FDCB,
	InputAxisScrollbar_HandleInput_m4E53774409CC762C0D34D83B186D18266E21A280,
	InputAxisScrollbar__ctor_mBDEC7527CCE962AC682F51B9179CC6E139CDCE98,
	Joystick_OnEnable_m24A5EA0EDD73F1486837F1F08DF4B80721EF754C,
	Joystick_Start_m8CE36CA457A4A410C33F008316130997880DB58F,
	Joystick_UpdateVirtualAxes_m7D6CB9CF992E3F6A52D21E37B0293E2EF11EDDE0,
	Joystick_CreateVirtualAxes_m10235FD4D0F3393790EBF5A58391886D78549173,
	Joystick_OnDrag_mA04BB282F1D24DA48FCFCBD7889D4964C15BC7F0,
	Joystick_OnPointerUp_m8E4F8B50A7E5665DA2226F16EF6CDE5BAE5326A5,
	Joystick_OnPointerDown_m6AF1B2BF22F35B5E3498907EC51C8C0561729FAD,
	Joystick_OnDisable_m656AD25023372C1AB0A455F73218ABFB0145B8FF,
	Joystick__ctor_mE448490D7C35B109542036993D0D4C1A94998D4E,
	MobileControlRig_OnEnable_mD1C1F78BDA9EA618AB12221F826AE0DDB93BB1E6,
	MobileControlRig_Start_m5DE87E0E24CB89A6D9DB4755EC7443592630ADA1,
	MobileControlRig_CheckEnableControlRig_m1708EC5391D82F8050E2F3E2939FE19A02B01DED,
	MobileControlRig_EnableControlRig_m8F79B98DE64549179F06D849E9F97E139B388221,
	MobileControlRig__ctor_m127520DB08F2DFE95FA839E1C5043E603D2C61DA,
	TiltInput_OnEnable_m9419C0AFC8EABA0CB8F02488E6653CB6C0ED262B,
	TiltInput_Update_mC35CD38493ECF6EB17DB66B5219C8C817F88F034,
	TiltInput_OnDisable_m2125CE9F5BD06B2C184F6642BCE6585218A6E223,
	TiltInput__ctor_m20D49463D098180E19FA5980DCE712A520358551,
	AxisMapping__ctor_mC15745B60FFEA4482E885B02C8910FC2A0972981,
	TouchPad_OnEnable_m8046F501C2F56DD64ADF83DCD0923DD745E4806F,
	TouchPad_Start_mCD5535ABCB35D5C1660120B56C74EFC6FD7D8D93,
	TouchPad_CreateVirtualAxes_m09ACDDC26B1A01D3904821C3547FE8CFC67CD6F0,
	TouchPad_UpdateVirtualAxes_m1E34D78B61E20D535CF499A7D7A0690D8082EBDA,
	TouchPad_OnPointerDown_mDE5A780D09754F413B01C3D71717A82E101F492B,
	TouchPad_Update_m89E6817453070E87C77A803271689E69AC83111C,
	TouchPad_OnPointerUp_m88F91FCE7A72803EF92EF4F2518C1082AA221C68,
	TouchPad_OnDisable_mB3CFD5F3AF6F4BBB0DB43DFDFFBC6ABCAE9B25F2,
	TouchPad__ctor_m222A3522FA022C53F488FBB4F482F913ED34C009,
	VirtualInput_get_virtualMousePosition_m9DF87F8DAE8FA5CF9BC85284922026025AABB1FF,
	VirtualInput_set_virtualMousePosition_m3D48CAC3DC8D5A673C81F6986C8FE48DCD19CB59,
	VirtualInput_AxisExists_m01EC1FD139D4DC78B03BD91601E694BB0FD99FCD,
	VirtualInput_ButtonExists_m504FCFBACFAF025BD335F73B9E6365D0877F21FC,
	VirtualInput_RegisterVirtualAxis_m4D5DAB8CD547D5200513D860FDA6DC3930150BAC,
	VirtualInput_RegisterVirtualButton_m7AEF52824F354DA313380E110D03D2800F1A9B21,
	VirtualInput_UnRegisterVirtualAxis_mDC436E4797B5E7091462A67AC29559F8DE1FD688,
	VirtualInput_UnRegisterVirtualButton_mFD11E0016A1865D5A0C3E05ED7DC6C3F15DAF2EB,
	VirtualInput_VirtualAxisReference_mB9BFA6AD246B52D158CE8309581FE468DF756914,
	VirtualInput_SetVirtualMousePositionX_mDEE15F467D72B1B64C99473FFB2E7C3D65175B70,
	VirtualInput_SetVirtualMousePositionY_m4A9FB6CF5DBD17EABABABFC241BBCBD2720B3EA9,
	VirtualInput_SetVirtualMousePositionZ_m5EF12D8C9F3A8F7FCC443E682DA1F456AAD5B1DD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	VirtualInput__ctor_m39FA60B6A3FB2A793B3825E6EE488A0262224DCB,
	MobileInput_AddButton_mCE54D249830BB224C0E7868831F5DE9872721ECA,
	MobileInput_AddAxes_m9E1EF0D37885B89830E25966ACA9241FE6A67B05,
	MobileInput_GetAxis_mF7E4D1D5BB15C8D6DBBFA1D0ACB34C07BC52C3AB,
	MobileInput_SetButtonDown_mC9D7C110515111912F656057FEFBEF8E5880D70C,
	MobileInput_SetButtonUp_mAC2717F036575064C64280479659887F0B448410,
	MobileInput_SetAxisPositive_mDB01EA55F7F75B0C1FA2D79DC42D742F4B5FC45D,
	MobileInput_SetAxisNegative_mD1181DCDCC49DF0D476E9B525AF8274D2E2386FF,
	MobileInput_SetAxisZero_m93E3632453634975E55A9D4181FED4D5DBE15694,
	MobileInput_SetAxis_m57CFF9414DCE627C9AA83AECA9E3F61013EB94FB,
	MobileInput_GetButtonDown_m4AA7D6AB5B05ABCCA3C1908FA877E26998FBB71B,
	MobileInput_GetButtonUp_m473F8F5AA2D5CDCB5505837AA1CE17077C0D2C0A,
	MobileInput_GetButton_mAAF8F610076711577A20B084469AC344454E6632,
	MobileInput_MousePosition_mDE6DFB3175A5C3A75D210339227B70DA6E0CEA35,
	MobileInput__ctor_m50B1FDC06838FEABE2C9E43F8AE4476426413240,
	StandaloneInput_GetAxis_m879778236E844FFB175FC5EFED4D50C31FE914A0,
	StandaloneInput_GetButton_m6082577C808C1ED0E6D41434A917CAA90C6054DC,
	StandaloneInput_GetButtonDown_m1170A0D0F15EFAA2C69F865A8FBED63EED6F55C2,
	StandaloneInput_GetButtonUp_mD1A9A9DA63DEF0BB1F5743E1E92A5A65DA89B28B,
	StandaloneInput_SetButtonDown_mC273D780F0692CD7233597FF543B1A83A947F34A,
	StandaloneInput_SetButtonUp_mFF786A10495F3FFAB08DA57378B11BE278C9670D,
	StandaloneInput_SetAxisPositive_m48E3A614364EA257E208F312D690B4156545E314,
	StandaloneInput_SetAxisNegative_mFC6573E09AAD9371BDF288183584126E3C45BB02,
	StandaloneInput_SetAxisZero_m76EE6A91ADD9C7A867B279C16239FCC197FCBA0B,
	StandaloneInput_SetAxis_m38950004F9B6DD18B1ADFE2061DEF3356816E31F,
	StandaloneInput_MousePosition_mFBD800C92551160E40AC7EB8549E760240334BFE,
	StandaloneInput__ctor_mD89211F517B446A581B7D7AA1079D8FEBFEC389D,
	BrakeLight_Start_m79E75E76BA79E6E5244DB4A9CCB9B98F494F763B,
	BrakeLight_Update_m6FC731716E0455D3D605DE4C70BD708FC1102FAA,
	BrakeLight__ctor_m9AF99D86B450EE4739C6AD76EB98B83EE8B7E56D,
	CarAIControl_Awake_mD2089BE1D633E8625AE3465FB4A97EDD7228BADB,
	CarAIControl_FixedUpdate_m08EAE849DA5C637C28BF4A76A820940995B79CFB,
	CarAIControl_OnCollisionStay_m355A6C6957FF4368F30C2BA2C1437CFA4C7BEA8D,
	CarAIControl_SetTarget_mA7FD3633368824E4AF643F5D8745D290122F7F57,
	CarAIControl__ctor_mC112AA6249F00CA8A4DA12A2A2DB4ACA65E30A25,
	CarAudio_StartSound_mDC58A4590A6CCE8F65EDF936F6A1F8335E20ECD6,
	CarAudio_StopSound_m4D15199B8BD77034BF556D616839D9D0DC7C6989,
	CarAudio_Update_mE4A3EA645511C5948CD8DDAF5A8F4CCA8C596F86,
	CarAudio_SetUpEngineAudioSource_m27A0CB49228CF678DDB00E47EF1B997C095D2867,
	CarAudio_ULerp_m224D3E2D54A1BAEC55443940996F25A262766726,
	CarAudio__ctor_m2A1E401BA72ED226F84CD57335C8A3E1EC4FF569,
	CarController_get_Skidding_m30527ED8ED64198664ABD9F52683D5175E01E53D,
	CarController_set_Skidding_m0E17DA6D5A3CF8210BF8A032896B4889146EC619,
	CarController_get_BrakeInput_m38CBD2496997600DCE24280D4A6C8BD45D764EB1,
	CarController_set_BrakeInput_m2D9D58EE619793C1694E8425B5E753B73A088BA8,
	CarController_get_CurrentSteerAngle_mA67C5DFB1A0EA0C52D8DD24A99E9E403F556B026,
	CarController_get_CurrentSpeed_mAAF3026B379B0C2B7DA31EAB953B5FF27EE8AA17,
	CarController_get_MaxSpeed_m36AF0F96E3D85904C03313EE0BBC1C97B29247FC,
	CarController_get_Revs_mF62F270CF0A3EE218FC8706431206D8B4A97D756,
	CarController_set_Revs_m4CFD510A5533F9EF3CFA8E2C80B2A3188087FCAF,
	CarController_get_AccelInput_m6508BF98BCC6E7EFC016ADE800B6C2FFDF2A2327,
	CarController_set_AccelInput_m7398B7A62BA5D9C65E4D1A8E6C86E2ACA082E65C,
	CarController_Start_m56C0595BD2EF0197B75057F46CFF0D28C93F19B2,
	CarController_GearChanging_m44D35FE8CF69D24143C83F445D0FA9988AB6D5F0,
	CarController_CurveFactor_m063F5F48DE03D5901B393E327164E1E10F49CE52,
	CarController_ULerp_m53C2DB4BA45DC0464846C839B8D27AAB395BFFEA,
	CarController_CalculateGearFactor_mB7A33A4892FBC6768D35A6D0E1C7462C645EEAD8,
	CarController_CalculateRevs_mB96F83186C7F570B0B379FC9360EF723EC96AEED,
	CarController_Move_m5F35A2C2386A7CBAED5561846C742F80647FCB0A,
	CarController_CapSpeed_mBCACA1CB04D7A83D933A1C9DE99F56C0965A9CC1,
	CarController_ApplyDrive_mE52790A94B93C3BD86B9CBEA3259A7E2CF2A5D00,
	CarController_SteerHelper_m7493A75F868F43301DB0C074EE823CDF8CBC1A26,
	CarController_AddDownForce_m31689FF75806FE6129C6E9605B4FB79A7BEDBC98,
	CarController_CheckForWheelSpin_m58909D01006D82C65D0963E105B8BEEE1EE50D5C,
	CarController_TractionControl_mFD6A5BD1002A9A02A2FF8F507F9D87CA9596F2D6,
	CarController_AdjustTorque_mC1E7ABD0BBFA5D26ABD82AAFDD02CFB27452CD75,
	CarController_AnySkidSoundPlaying_m1EDCA6E8100899C1C54E1EC10F8CCD37D111BAD4,
	CarController__ctor_m83D4A43409B7D2E4859598C533392550CDC46F57,
	CarController__cctor_m1D39E9B2ACE5BC3D53CC3FB2C47C9416877B8E65,
	CarSelfRighting_Start_m4996445BA84866C4C0C59F5409604510B164E308,
	CarSelfRighting_Update_m8ED1D5510F5794AD2521B39D35A5332DF52B2407,
	CarSelfRighting_RightCar_mFD7EBDC74A6F7690147946962C95D6598049C0C4,
	CarSelfRighting__ctor_m7A3309295BF03808C023FBFFA718F423308BA38E,
	CarUserControl_Awake_m18597F775E26B5CB33F33FA399D407672596A5B8,
	CarUserControl_FixedUpdate_m392895DDE302A2119D120574923A3509A47EE843,
	CarUserControl__ctor_mDE50C7EECDF3B283FE46CF2C9AA4B180BC23C2CA,
	Mudguard_Start_mB6997804B6CF001D668D0EA6178F9E129BC176F0,
	Mudguard_Update_m227D04AF7B9078A8E3F3486B75F8ADCCB1914367,
	Mudguard__ctor_mAF35227ABDB228B503C74F20EF3369488E4F730D,
	SkidTrail_Start_m2695ED24C08B06196C72289F1966031DEF0EA411,
	SkidTrail__ctor_m91FCFC15149FA5B42C14DC21BAAC3FBF508ED90B,
	U3CStartU3Ed__1__ctor_m7AFCB32BFE8C866374C483F7A8B4C8406CC6B5CD,
	U3CStartU3Ed__1_System_IDisposable_Dispose_m50EEBFFF90F957EAA72D3FC5A5B0AC712C64C563,
	U3CStartU3Ed__1_MoveNext_m2A889D196AC731C52CBFAD3F7DE71681D7BBD25E,
	U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0BFD033FFC2B2E1DD5B1026D5BAF6BE7C97481C,
	U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m9513A2F5ED5939A57CE31950DC6594B71D4FE876,
	U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mA7EABF66D3685C35C314600934F17B6C28BB62AD,
	Suspension_Start_mF2921EA9BEDC0FA76051D860607EB470CEF91D78,
	Suspension_Update_m8195490C7663062F131370651C6D2291ABBDE3F5,
	Suspension__ctor_m8AA95141EE8417156AEEB201139DF6DA4EBD2F30,
	WheelEffects_get_skidding_m1EBCDCFEDB16686E9B2D4B73FEE0482B9337EE97,
	WheelEffects_set_skidding_mBA4FE70870D08789EDF7C7EF7060E53BBA7C8C49,
	WheelEffects_get_PlayingAudio_mA04B2C917A3F14A87066DC812D7FBA268F02AF36,
	WheelEffects_set_PlayingAudio_m2EEE94A5D8D9BF9517AD6DA76FA26E9D238A23AC,
	WheelEffects_Start_mF995D30D628E703C7CC835FBBFBDEDBFBDE5DA92,
	WheelEffects_EmitTyreSmoke_m58A2563671E5A909B9544E4754101B8F2EC91CDF,
	WheelEffects_PlayAudio_mD4BDB2BFA5BB4FB8BE4024FCF2382CEC30CCA071,
	WheelEffects_StopAudio_mF919DD64488467022850E3323492E07CE88497DD,
	WheelEffects_StartSkidTrail_mAA0A3A8AE8E3C79A9C80898154942444FEC10323,
	WheelEffects_EndSkidTrail_mDABC126E04064635FA218790E33C19E171030975,
	WheelEffects__ctor_m39F4618C7E58523783B781C74CDB25D115E4E4BE,
	U3CStartSkidTrailU3Ed__18__ctor_mC72F95742347617153FEBDD8C8675023C457A0E4,
	U3CStartSkidTrailU3Ed__18_System_IDisposable_Dispose_mD3F45C2E53A7527B65E4DF777B4D97296E42FB37,
	U3CStartSkidTrailU3Ed__18_MoveNext_m1091256D4BD86BD61651285CD5D8CD518563C5D0,
	U3CStartSkidTrailU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA8759211E822567D82E01677B3C49B92F5A63395,
	U3CStartSkidTrailU3Ed__18_System_Collections_IEnumerator_Reset_m981418E0BAC5979456EF45FCD3D5917BA5B4F352,
	U3CStartSkidTrailU3Ed__18_System_Collections_IEnumerator_get_Current_mF42C32E83A0BAD7E47FD5B5646B55E355EC8DC4A,
	AeroplaneAiControl_Awake_mBB1F332AC1DD045C2A966BA70D5953DF13A28856,
	AeroplaneAiControl_Reset_m8AE5A7318E2DABB2F9FC199D1A63CEFF31DF624C,
	AeroplaneAiControl_FixedUpdate_mA4FB1E3A82B6BD908B6442AF1EC25BA912FD10B8,
	AeroplaneAiControl_SetTarget_m1D2251D711AF77634C31CD681767DDA4D69A2ABF,
	AeroplaneAiControl__ctor_m0D633CFE318B1C993BFDB21B58B9063EEF083123,
	AeroplaneAudio_Awake_mD1C3764DA8C85EE774B0498DA5E9780A661DD104,
	AeroplaneAudio_Update_mE0F1409B7B370A2C794F3B025C5693FDE22F8B76,
	AeroplaneAudio__ctor_mA7BF192E0267AE2CCC0E12B940961C322706158D,
	AdvancedSetttings__ctor_mFAB9E670E2A2A4F1C0A437146AA5295D52282D78,
	AeroplaneController_get_Altitude_m4E875DCD8BCB99EE7F07DA2E8762AFD46BE541FE,
	AeroplaneController_set_Altitude_mAE9A6518154CFD78E7CFF142CE90D962CD3227CB,
	AeroplaneController_get_Throttle_m4AD61D14FF2EB832F32EF2201EEEB88D8C00112F,
	AeroplaneController_set_Throttle_m92D547C38B62C8BC0627EA481B518D9814471C51,
	AeroplaneController_get_AirBrakes_mA6A92442654786A3A81FC10F115E19388289B321,
	AeroplaneController_set_AirBrakes_m219D70D6F0149377B82BFD9C5EBC04F704F69E5B,
	AeroplaneController_get_ForwardSpeed_m24FBA8E719F9947C59C04A99A82F023BB880AA56,
	AeroplaneController_set_ForwardSpeed_mA83C4E387E8F7BE0EB0513469189C1EAA7B15488,
	AeroplaneController_get_EnginePower_m6820B0A8A0667DF3F875DD6B946CAFA90D49886F,
	AeroplaneController_set_EnginePower_mC367A1178518B9BD614703C790ED9C3E905C6C8B,
	AeroplaneController_get_MaxEnginePower_m7C993DEBFB6C0BDC12585CC7EEFEF54482841A04,
	AeroplaneController_get_RollAngle_m0EE0E1FF7C938837030E55880D5C1C00672952FC,
	AeroplaneController_set_RollAngle_m194392FEB7B4232ECC7668DC939835A964FA66F9,
	AeroplaneController_get_PitchAngle_m256401C8A133DF9FECD7F0ED7039C402C54CF7DA,
	AeroplaneController_set_PitchAngle_m6B3800033AF9875C9C281698484D2E231FCBB22F,
	AeroplaneController_get_RollInput_m0C5664B7B308AA31B69423E3D36DAB1A3281BAD7,
	AeroplaneController_set_RollInput_m4BB6E33F4D4A5E9625B7DCE2630A83A3A41B654F,
	AeroplaneController_get_PitchInput_mC727744982F5F76244E92A6D6E47FCCC08426112,
	AeroplaneController_set_PitchInput_mF42D986AC4A0106FB23CC615FBB0FC23373E3338,
	AeroplaneController_get_YawInput_m592983EDFA03B03DDBBA4EE77907AA42E32054FE,
	AeroplaneController_set_YawInput_m69C88A4428AFB957949221A11B65A31FEC45651E,
	AeroplaneController_get_ThrottleInput_m59851ED24E7C475C0C70D4864F6FF459316F0D07,
	AeroplaneController_set_ThrottleInput_m2743123CE0B04B4916503B63E69910CE7744095F,
	AeroplaneController_Start_m7FBCD8FBF0BCC1462B1A8BC4E2B5FF8D6AE5EE4B,
	AeroplaneController_Move_m50739E5FDF5DC4DC9CFA0F433DBA454D00895F1D,
	AeroplaneController_ClampInputs_m11BFF054FCFFD465F2092EEEC1569089D32ED574,
	AeroplaneController_CalculateRollAndPitchAngles_m256DFBCD062BE41329387558298C11E5120E1CEA,
	AeroplaneController_AutoLevel_m769E68621FF95E4F36EF22B21C1D5534B0B64647,
	AeroplaneController_CalculateForwardSpeed_mEDE489FF22663E14B1D0EBD4A8C711AA82430ABC,
	AeroplaneController_ControlThrottle_m23FD0CBDDCE899A8C33DD36D20E530F29ABF5692,
	AeroplaneController_CalculateDrag_m5F4B70479BD915D297D6F251AD8B0B08B9516914,
	AeroplaneController_CaluclateAerodynamicEffect_m06EB3AD0D5A5A7B3D5C160DA498B0813561E4BB8,
	AeroplaneController_CalculateLinearForces_m16697D299B525FE4771CA9EA1D8E768D5BF2EB7D,
	AeroplaneController_CalculateTorque_m83F88301C503C2A4EFE346086E8580C4231D1490,
	AeroplaneController_CalculateAltitude_m4E655414BAA1B4E1B556DF0D0ED88C85A945E90D,
	AeroplaneController_Immobilize_m2E23F22E0724E48E28928285DBB419318A5B4B91,
	AeroplaneController_Reset_m6AC87A785943BEA306E592215BD489DE80E4510A,
	AeroplaneController__ctor_m1FC615E68A962649FEC23298121027B4E1CE3349,
	AeroplaneControlSurfaceAnimator_Start_mF59FE734FA30A77F3252848150CFEF108128207E,
	AeroplaneControlSurfaceAnimator_Update_m226B44E84AB643FD36508C6DD9B9C6C9CB0B0B10,
	AeroplaneControlSurfaceAnimator_RotateSurface_m1C533431497586B6C6B9604556B350E8117C1D8D,
	AeroplaneControlSurfaceAnimator__ctor_m31502B7EB27FE87B5ED3CF500BCDDECF6EB52131,
	ControlSurface__ctor_m8432698A6B9941EC562E2F3C7D5D8067CFA79076,
	AeroplanePropellerAnimator_Awake_mE329F441D5138ACC3CC9B5AB838851F4C5C2FCC4,
	AeroplanePropellerAnimator_Update_m0ED917491FE41855D1B4205B49E73BC66D6E9B96,
	AeroplanePropellerAnimator__ctor_mEE35C8DC327F6D127671F6236B3470980764E3CB,
	AeroplaneUserControl2Axis_Awake_mE6284378E8163C26FDB122A2721ED98CF2EE5921,
	AeroplaneUserControl2Axis_FixedUpdate_m85869B57F1E6125C4279263942F09785F43B5888,
	AeroplaneUserControl2Axis_AdjustInputForMobileControls_mF8743B3626967E1836696860024F755B0439234F,
	AeroplaneUserControl2Axis__ctor_mEF5248CD690ABADA04E8DD4FE8498AF34927C8F1,
	AeroplaneUserControl4Axis_Awake_m2E197C2C29E1F56E6C25FB657AF0313D2C16ED31,
	AeroplaneUserControl4Axis_FixedUpdate_m59F01670B6E8FF68815980D7F5B376C3DC3CF85A,
	AeroplaneUserControl4Axis_AdjustInputForMobileControls_m7A8B54330AC2872BEC5CA02F6714429CF557653A,
	AeroplaneUserControl4Axis__ctor_mC752A563940E50FF9E13976C5688F05291584B7F,
	JetParticleEffect_Start_m2FB30A37B89344109B7E39F2FF2C3F35D71AF265,
	JetParticleEffect_Update_m149824E423722F077F054BE895B88FF8FAF93AD9,
	JetParticleEffect_FindAeroplaneParent_m7910F189D8D43BE33589A795685FCFEBF6832F16,
	JetParticleEffect__ctor_mF8E9E08946B553295EAF785029EE39A4B4A47676,
	LandingGear_Start_m70C789B2041BD240C035E877C24B286DCFDA09E8,
	LandingGear_Update_m73400D86CB6743CD87E9D40E42F0B875DB870AB6,
	LandingGear__ctor_m8DFED9790FE9F6BF42F1391CCA7AE61FCEF10349,
	Ball_Start_m5D0B8C28F1754209F6D16469D336F4B1512B7A5C,
	Ball_Move_m8F0B2AEF76FB1CE02759A6821D547694DC6C206A,
	Ball__ctor_mFF6055A2CCEA332012FA5096FD74AFCC3AA9DE83,
	BallUserControl_Awake_m2E99883139DA507FD403B93D914012CD0E5A7469,
	BallUserControl_Update_m9914D43D57FCE7CF3D2005007BD40D494B402029,
	BallUserControl_FixedUpdate_mF8BAAC8DEE572570B46E3427A0A439EBD763CC59,
	BallUserControl__ctor_m464B644240493E89135688A53AA2ABE78ED388D8,
	AICharacterControl_get_agent_mE9D2D96A9F9C131E51806B002B2EAF007C1D6F95,
	AICharacterControl_set_agent_m578A2A67480A8EA83153AC7634411B77FA9CCC46,
	AICharacterControl_get_character_m428E6810E7895CA422D1F78CCC0D55062CD3EB32,
	AICharacterControl_set_character_m7500A5CBD3DBC30E9EC8E77F02094901A56F08DE,
	AICharacterControl_Start_m16F993FE523F1D911D1A7810F5D2E57F8CAFEEC2,
	AICharacterControl_Update_m94A6E10CB8196AA80D5AA78048464D589DBC726F,
	AICharacterControl_SetTarget_m8BC1A01E10C8ED6CDB8CFA5B06F8A90D2923FF79,
	AICharacterControl__ctor_m5329061559B50669BBD6DF1A0B509EEBB2E6F53A,
	ThirdPersonCharacter_Start_m5AA32FD6C771EE3BB7AC492F35DEB17164389EEE,
	ThirdPersonCharacter_Move_m14BD868DA889B216D21EBD4B65822D1301A8D817,
	ThirdPersonCharacter_ScaleCapsuleForCrouching_m9ACB83ACE0F987E8F3D0793ADA140C4744798505,
	ThirdPersonCharacter_PreventStandingInLowHeadroom_m77123AD6DFC0726F46E3CAFF44DC7A918233BECF,
	ThirdPersonCharacter_UpdateAnimator_m5D81E79A8CC7B4F7AB9CEBA64A6BFD1A100847D8,
	ThirdPersonCharacter_HandleAirborneMovement_m058EEAB7BFBFCC5C3B80227B8F3FE5A8797BEF46,
	ThirdPersonCharacter_HandleGroundedMovement_m1DCF7A4ED5FFC241FCCF8DC3174D7C31B110522C,
	ThirdPersonCharacter_ApplyExtraTurnRotation_m17BF71EAC445A20DB271B94C5B024C17F858677F,
	ThirdPersonCharacter_OnAnimatorMove_mD0430AE51DCE26AB275DEFFB377A4C76ACB14CEB,
	ThirdPersonCharacter_CheckGroundStatus_mE303C4E82C923B49B57CB218228945068A8A10C4,
	ThirdPersonCharacter__ctor_m5C6F59AE87039781AC92B9C9743D3518D50987A3,
	ThirdPersonUserControl_Start_m0A4BA00B12E8F11166BF653A6E9D5BE02CFBC3ED,
	ThirdPersonUserControl_Update_m3635F6E70F540A56F063280BFC1FE283E5CE5643,
	ThirdPersonUserControl_FixedUpdate_m5410D2090502C79F94D9B5C93B2C988F9861310B,
	ThirdPersonUserControl__ctor_m88F867E11FE8AA21EB19765F4CBDA14E68DB751E,
	FirstPersonController_Start_mAA4A49EB87FBB7902C8EB60B2DBCB98560A86539,
	FirstPersonController_Update_m7A06B61C940B9C029DEA5CAA52E071FEF1E4101F,
	FirstPersonController_PlayLandingSound_m1163F5994A1ECC6354A71D3B14463AA52BD6AD0C,
	FirstPersonController_FixedUpdate_m885A83AC79736376ADA18D55A166A552B364EE39,
	FirstPersonController_PlayJumpSound_m724953DFC405CE754883F265282A759DF54F0CD9,
	FirstPersonController_ProgressStepCycle_m4A0C50AA385E80A025CD791C8551A0A59428DFD1,
	FirstPersonController_PlayFootStepAudio_mEAFE8B9F81AC4CE7A91E3B2D7ADA146FBC2C38DD,
	FirstPersonController_UpdateCameraPosition_m45F7DDF70CB43963FB5A36BA11EF8ADF67CD127E,
	FirstPersonController_GetInput_mF4AB933D87EDE6957A437045F1BFEA59C7E6A07A,
	FirstPersonController_RotateView_mD729D6CB1C9C70DAFC522878C2655D6E22989319,
	FirstPersonController_OnControllerColliderHit_m61BE461A3EFCAFDA23EBD42CF46427753A1D5975,
	FirstPersonController__ctor_mC29C4274392FB92FA99B4CBD7CAE7EAD76E71D60,
	HeadBob_Start_m0461CAD7A2559ABA05871DC2637C917EE831637E,
	HeadBob_Update_m39FFE350BAB4E1680603665E4F569E56E6BD72E6,
	HeadBob__ctor_m5660486EDE7ECED49229156E2E97C9FFE5F7991C,
	MouseLook_Init_m65D7184A71FB03E7D8A91B5568B29B95A2966F81,
	MouseLook_LookRotation_mA488F8CAAF8FF957AC2CDF5C6BC13C6AECE13FE4,
	MouseLook_SetCursorLock_m232686DFCD6A9ACD9A7A00A097985F7B219D5C09,
	MouseLook_UpdateCursorLock_m2A544D023BF6BC47AD20026BC6BD2ECCAA3DFABB,
	MouseLook_InternalLockUpdate_mBF77944AF4DB12E517B3BF223C471750E7B7AA10,
	MouseLook_ClampRotationAroundXAxis_m300D0DC7B8F3C07914205392FC3E4953EF949049,
	MouseLook__ctor_m3A4849EBD9493C9A93CF072D4D8F89579B75CDCA,
	RigidbodyFirstPersonController_get_Velocity_mB70B043E4FF2263A33E379FF712BACB02633C339,
	RigidbodyFirstPersonController_get_Grounded_m77BF29EDF29AC361B1DED301A83CC1A12110CA61,
	RigidbodyFirstPersonController_get_Jumping_mCEEF2A49DDD908B8331D63CDC79271BF18505F86,
	RigidbodyFirstPersonController_get_Running_mB5D2C7EE08B1425F737F83EB7BB1CEB1A4AA3DF9,
	RigidbodyFirstPersonController_Start_m007AC5C8C9C6A023F268E43B2547911DED6F41DE,
	RigidbodyFirstPersonController_Update_m9AE00B033CD92A95DF1D72AA04777C7243DCBF53,
	RigidbodyFirstPersonController_FixedUpdate_m2627ED832700FEED40B40D4199DA0EF301207D2D,
	RigidbodyFirstPersonController_SlopeMultiplier_mC48D2F329905D984EA3080CF0A8C6290B50327C9,
	RigidbodyFirstPersonController_StickToGroundHelper_mE9E633A49F5FE50DFCB6D2244FCBA5AB3D518048,
	RigidbodyFirstPersonController_GetInput_m679BF16A9B5F17B9258B11CE29222CCCD72F352F,
	RigidbodyFirstPersonController_RotateView_mD297E393DA2494AD94EB0F5DAAB150448EE0CEFF,
	RigidbodyFirstPersonController_GroundCheck_mB8795C247C54E60D56AAF6B923BA9A368273C147,
	RigidbodyFirstPersonController__ctor_m67B9AA5754CEF1E8145B0CB196DAD988126AFC0C,
	MovementSettings_UpdateDesiredTargetSpeed_m6D418A9690124CFC9FA7DEA81FB2E521CFC73D12,
	MovementSettings__ctor_m349160EB18090C7916E6210DC54F023AC6410527,
	AdvancedSettings__ctor_mC6C95C0E431E720B8F952FD872E22A801CEE4D46,
	AbstractTargetFollower_Start_m7F61268C7065276D04619317D1B609C06C37C398,
	AbstractTargetFollower_FixedUpdate_m9BBDE9FD938E5F3C379CC827B2D14B2BF1E197B3,
	AbstractTargetFollower_LateUpdate_m828E15AD2C38E4F0947474CB179D054E962FF6ED,
	AbstractTargetFollower_ManualUpdate_m001A4048A98BFF454F9E03BFB17478A02C4E80EA,
	NULL,
	AbstractTargetFollower_FindAndTargetPlayer_m692A40676591951502AB0C1B7904AE247E14B3C3,
	AbstractTargetFollower_SetTarget_mCB80D61FD43B809825CF5685694F069163835A80,
	AbstractTargetFollower_get_Target_m9D95FB6B7D394313A438474A9F3F2A2E69CFEF1B,
	AbstractTargetFollower__ctor_m9383202CC56D1418B9B62CDA9A85A00D4F7EB181,
	AutoCam_FollowTarget_mAEE3C3DBFAF42EFBB5A903C2BD63D070B884BFF2,
	AutoCam__ctor_m9DD923DB2FE3DF7B76B1CB48D5D3FC86256C1103,
	FreeLookCam_Awake_m14CCC793AE86B51FB3B5939DFF90E402BCA5BA6E,
	FreeLookCam_Update_m52A2CF02E928E5FEB6CB7F080873258A0A0E510B,
	FreeLookCam_OnDisable_mCDF2848FDCEAE527591F1B4187145D6C89243C8C,
	FreeLookCam_FollowTarget_m6FBDD37A7C75C9E0B2CAB6081A20365A0DBCA8A3,
	FreeLookCam_HandleRotationMovement_mB03D65D9D7D28863ADB3AC892B7F03882ABFD9CC,
	FreeLookCam__ctor_mC068E3574DCE19C486D767F4B8E5D0DA17A74901,
	HandHeldCam_FollowTarget_m8FA00A4BF32224C5A9EF0C7FBEA378ADD2F60A81,
	HandHeldCam__ctor_mFEBA00B6A4892C79C8C780E7FC290AB4BC3E28E1,
	LookatTarget_Start_m6C0EA22CAA8DD3279AC54D149D8BE5751DC90820,
	LookatTarget_FollowTarget_m8E28D73BA8EFF01C3CF34147B8B30E483075BD34,
	LookatTarget__ctor_m46B5A2FF61935C88A06F7D1CE8B734EFA77623C1,
	PivotBasedCameraRig_Awake_m983D94AEF67006D5DB60ECF7E37A4A2E80C06627,
	PivotBasedCameraRig__ctor_m2E64852C2F38E2BFA5525713519BBEC14DF15C01,
	ProtectCameraFromWallClip_get_protecting_m0D765A8440B7565261BD788EB73C1EE4EE3DAD65,
	ProtectCameraFromWallClip_set_protecting_m48C0228FFDBCF906769B116CD846C9EE0F7EDDE3,
	ProtectCameraFromWallClip_Start_m7B988A51AF1E1399BF7CF2F34436289BD2271260,
	ProtectCameraFromWallClip_LateUpdate_mDEA1282E559819EA00553332A82BEC9BF7393655,
	ProtectCameraFromWallClip__ctor_m475D40249900346AB58EA88AC94CAC77D21D3B0D,
	RayHitComparer_Compare_m58E06CAB53C50F64CD9BC9C64050BAF4E5E9354D,
	RayHitComparer__ctor_mE0506654EA6FCFED553243206FF268F188737B00,
	TargetFieldOfView_Start_mFCD6A3AC71B2828B939F54C2700B2267CA674FD7,
	TargetFieldOfView_FollowTarget_mE76F2BD563C4BE107F02AA643D1B44F2182AF839,
	TargetFieldOfView_SetTarget_m0A958EA19FD6A8DBE394C8C95A7B2857EE9C3F64,
	TargetFieldOfView_MaxBoundsExtent_mD5B03F520F5DDED1529DC43817C7159CEBA6A955,
	TargetFieldOfView__ctor_mE55C2793450450A863BE51AB81A1670184D9EE19,
	Camera2DFollow_Start_mBBCB0756B72E6A35767B21551B149655C99B4710,
	Camera2DFollow_Update_mFCEB81CAF13114E1AC1DAC417CC6F9207FF59523,
	Camera2DFollow__ctor_m5D8B513FD9D7A833F4C42EE1F4C30452BF3C42C6,
	CameraFollow_Awake_mBBAF3F7EC4EB6661BC8175A30FCEBB56FD593874,
	CameraFollow_CheckXMargin_mBA18B08E43C19B547573C57A58196FFF05F88B0B,
	CameraFollow_CheckYMargin_m1036E178E98FBF08B43DAA2CFD4D17E8A594B1C7,
	CameraFollow_Update_m95F27CA16E2239544092698F286B099E82944FD2,
	CameraFollow_TrackPlayer_m4E783430A9FAF82ED6F5B14BD1A18D1681538097,
	CameraFollow__ctor_m2AE326C2998FB64EF10070E8F968D10F8F93945F,
	Platformer2DUserControl_Awake_mDB0746580C433A28419249C9A4B479F5047CB504,
	Platformer2DUserControl_Update_mD6F1860515F415BE3049299A04F6733D57EFBEF7,
	Platformer2DUserControl_FixedUpdate_m0E625C864E17EDFDC887737570D1323FF42AE914,
	Platformer2DUserControl__ctor_m867784A332298E7C83BC03F98D44497049D5223B,
	PlatformerCharacter2D_Awake_m51C97A56A28460A2AD20D4935A9CC2C3E4F8CE44,
	PlatformerCharacter2D_FixedUpdate_mB4BCA3EAD27575C9465D6F33AE248676B6D3B1C7,
	PlatformerCharacter2D_Move_m5C9E0B1A3FA19184A28BBC94FFD0DA09B3B66382,
	PlatformerCharacter2D_Flip_m3BADEAB3C7723ED0447C289F89A14228226B6611,
	PlatformerCharacter2D__ctor_mEA1EE535C6C514E3F455E216E456BF1B74D01077,
	Restarter_OnTriggerEnter2D_m088B16EF1BC6AF41BBE0ED226F107B807851567D,
	Restarter__ctor_mD6B984BB34344BFD9D5B41980F8AA8636F350CBF,
};
extern void RoutePoint__ctor_mCCDE64040449EDB283B910FC23935BBB32D1DFD5_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x0600015A, RoutePoint__ctor_mCCDE64040449EDB283B910FC23935BBB32D1DFD5_AdjustorThunk },
};
static const int32_t s_InvokerIndices[866] = 
{
	143,
	1706,
	1706,
	1706,
	1408,
	1408,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1396,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1432,
	1432,
	1432,
	1428,
	1428,
	1706,
	1706,
	1706,
	1706,
	675,
	1706,
	1706,
	1706,
	1408,
	1451,
	854,
	1706,
	1706,
	1706,
	239,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	656,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1408,
	1408,
	1706,
	1706,
	1706,
	1706,
	1408,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1659,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1706,
	1706,
	1706,
	1706,
	1706,
	1659,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1706,
	1089,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1706,
	1089,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	2760,
	1706,
	1706,
	1408,
	1706,
	1408,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1659,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1682,
	1396,
	1706,
	2760,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1408,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	493,
	1408,
	1408,
	1706,
	1706,
	828,
	1330,
	1706,
	1706,
	1091,
	1659,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1706,
	1706,
	1706,
	1706,
	1706,
	1408,
	1408,
	1408,
	1659,
	1659,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1706,
	1706,
	1706,
	1686,
	1659,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1706,
	1432,
	1091,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1659,
	1706,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1706,
	1706,
	1428,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1089,
	1089,
	1089,
	1706,
	1706,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1706,
	1706,
	1706,
	1686,
	1432,
	1659,
	1706,
	1511,
	1330,
	150,
	1706,
	1706,
	1706,
	1428,
	1706,
	1706,
	855,
	1738,
	1481,
	1738,
	1481,
	1738,
	1481,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1659,
	501,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1659,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1706,
	1089,
	1706,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1408,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1408,
	1706,
	1706,
	1089,
	822,
	1089,
	1408,
	1706,
	823,
	1706,
	1706,
	823,
	1408,
	2352,
	2352,
	2690,
	279,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	823,
	473,
	1646,
	1646,
	279,
	2499,
	1706,
	1706,
	823,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1408,
	1408,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	2760,
	2712,
	2670,
	2670,
	2716,
	2716,
	2716,
	2716,
	2641,
	2688,
	2688,
	2458,
	2670,
	2670,
	2670,
	2716,
	2716,
	2716,
	2716,
	2716,
	2522,
	2758,
	2720,
	2720,
	2720,
	1659,
	1408,
	1682,
	1428,
	1408,
	826,
	1706,
	1432,
	1686,
	1686,
	1659,
	1408,
	1682,
	1428,
	1408,
	826,
	1706,
	1706,
	1706,
	1682,
	1682,
	1682,
	1706,
	1432,
	1706,
	1706,
	1706,
	1449,
	1706,
	1408,
	1408,
	1408,
	1706,
	1706,
	1706,
	1706,
	1706,
	1428,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1449,
	1408,
	1706,
	1408,
	1706,
	1706,
	1702,
	1449,
	1216,
	1216,
	1408,
	1408,
	1408,
	1408,
	1089,
	1432,
	1432,
	1432,
	672,
	1216,
	1216,
	1216,
	1408,
	1408,
	1408,
	1408,
	1408,
	828,
	1702,
	1706,
	1408,
	1408,
	672,
	1408,
	1408,
	1408,
	1408,
	1408,
	828,
	1216,
	1216,
	1216,
	1702,
	1706,
	672,
	1216,
	1216,
	1216,
	1408,
	1408,
	1408,
	1408,
	1408,
	828,
	1702,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1408,
	1408,
	1706,
	1706,
	1706,
	1706,
	1089,
	2237,
	1706,
	1682,
	1428,
	1686,
	1432,
	1686,
	1686,
	1686,
	1686,
	1432,
	1686,
	1432,
	1706,
	1706,
	2690,
	2237,
	1706,
	1706,
	348,
	1706,
	846,
	1706,
	1706,
	1706,
	1706,
	1432,
	1682,
	1706,
	2760,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1659,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1706,
	1706,
	1706,
	1682,
	1428,
	1682,
	1428,
	1706,
	1706,
	1706,
	1706,
	1659,
	1706,
	1706,
	1396,
	1706,
	1682,
	1659,
	1706,
	1659,
	1706,
	1706,
	1706,
	1408,
	1706,
	1706,
	1706,
	1706,
	1706,
	1686,
	1432,
	1686,
	1432,
	1682,
	1428,
	1686,
	1432,
	1686,
	1432,
	1686,
	1686,
	1432,
	1686,
	1432,
	1686,
	1432,
	1686,
	1432,
	1686,
	1432,
	1686,
	1432,
	1706,
	187,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	824,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	442,
	1706,
	1706,
	1706,
	442,
	1706,
	1706,
	1706,
	1659,
	1706,
	1706,
	1706,
	1706,
	1706,
	853,
	1706,
	1706,
	1706,
	1706,
	1706,
	1659,
	1408,
	1659,
	1408,
	1706,
	1706,
	1408,
	1706,
	1706,
	516,
	1428,
	1706,
	1449,
	1706,
	840,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1432,
	1706,
	1432,
	1366,
	1706,
	1408,
	1706,
	1706,
	1706,
	1706,
	823,
	823,
	1428,
	1706,
	1706,
	1106,
	1706,
	1702,
	1682,
	1682,
	1682,
	1706,
	1706,
	1706,
	1686,
	1706,
	1700,
	1706,
	1706,
	1706,
	1447,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1432,
	1706,
	1408,
	1659,
	1706,
	1432,
	1706,
	1706,
	1706,
	1706,
	1432,
	1706,
	1706,
	1432,
	1706,
	1706,
	1432,
	1706,
	1706,
	1706,
	1682,
	1428,
	1706,
	1706,
	1706,
	542,
	1706,
	1706,
	1432,
	1408,
	2458,
	1706,
	1706,
	1706,
	1706,
	1706,
	1682,
	1682,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	1706,
	512,
	1706,
	1706,
	1408,
	1706,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	866,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
